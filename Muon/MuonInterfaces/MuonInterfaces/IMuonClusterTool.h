#ifndef MUONTOOLS_IMUONCLUSTERTOOL_H
#define MUONTOOLS_IMUONCLUSTERTOOL_H 1

// Include files
// from STL
#include <string>

// from Gaudi
#include "GaudiKernel/IAlgTool.h"

/** @class IMuonClusterTool IMuonClusterTool.h MuonTools/IMuonClusterTool.h
 *
 *
 *  @author Alessia Satta
 *  @date   2010-01-15
 */
struct IMuonClusterTool : extend_interfaces<IAlgTool> {
  DeclareInterfaceID( IMuonClusterTool, 2, 0 );
  virtual StatusCode doCluster(const std::string& Input,
                               const std::string& Output) =0;

};
#endif // MUONTOOLS_IMUONCLUSTERTOOL_H
