#ifndef MUONID_IMUONCLUSTERREC2_H
#define MUONID_IMUONCLUSTERREC2_H 1

// Include files
//from STL
#include <vector>
#include <memory>
// from Gaudi
#include "GaudiKernel/IAlgTool.h"
#include "Kernel/STLExtensions.h"
#include "MuonInterfaces/IMuonPadRec.h"


struct ClusterVars {
  std::array<int,4> ncl {};
  std::array<float,4> avg_cs {};
};

/** @class IMuonClusterRec2 IMuonClusterRec2.h MuonInterfaces/IMuonClusterRec2.h
 *
 *  Interface to clustering algorithm
 *  @author Marco Santimaria
 *  @date   2017-06-23
 */
struct IMuonClusterRec2 : extend_interfaces<IAlgTool> {

  DeclareInterfaceID( IMuonClusterRec2, 1, 0);
  virtual ClusterVars clusters(LHCb::span<const MuonLogPad> pads) const = 0 ;


};
#endif // MUONID_IMUONCLUSTERREC2_H
