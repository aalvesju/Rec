#ifndef MAKEMUONMEASUREMENTS_H 
#define MAKEMUONMEASUREMENTS_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"
#include "TrackInterfaces/IMeasurementProvider.h"
#include "MuonDet/DeMuonDetector.h"

/** @class MakeMuonMeasurements MakeMuonMeasurements.h
 *  
 *
 *  @author Jose Angel Hernando Morata
 *  @author Xabier Cid Vidal
 *  @date   2008-07-16
 */
class MakeMuonMeasurements : public GaudiAlgorithm {
public: 
  /// Standard constructor
  using GaudiAlgorithm::GaudiAlgorithm;

  StatusCode initialize() override;    ///< Algorithm initialization
  StatusCode execute   () override;    ///< Algorithm execution

private:

  IMeasurementProvider*  m_measProvider = nullptr;
  DeMuonDetector*  m_mudet = nullptr;

  Gaudi::Property<bool> m_use_uncrossed 
    {this, "UseUncrossed", true,
    "Use or not uncrossed logical channels in the non-pad detector areas"};

};
#endif // MAKEMUONMEASUREMENTS_H
