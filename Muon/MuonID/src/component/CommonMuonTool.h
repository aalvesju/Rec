#ifndef COMMONMUONTOOL_H_
#define COMMONMUONTOOL_H_

#include <array>
#include <string>
#include <utility>
#include <vector>

#include "Event/MuonPID.h"
#include "Event/Track.h"
#include "GaudiAlg/GaudiTool.h"
#include "MuonDet/DeMuonDetector.h"
#include "MuonID/ICommonMuonTool.h"

/** @class CommonMuonTool CommonMuonTool.h
 * A tool that provides functionality for all steps in muon identification that
 *are the same in HLT and offline.
 *
 * It is designed to have no state associated with the event. The only members
 *are related to conditions (such as detector information or FoI).
 *
 * @author Kevin Dungs
 * @date 2015-01-06
 */
class CommonMuonTool final : public extends<GaudiTool, ICommonMuonTool> {
 public:
  CommonMuonTool(const std::string& type, const std::string& name,
                 const IInterface* parent);
  virtual ~CommonMuonTool() override = default;

  virtual auto initialize() -> StatusCode override;
  virtual auto extrapolateTrack(const LHCb::Track&) const
      -> MuonTrackExtrapolation override;
  virtual auto preSelection(const LHCb::Track&) const noexcept -> bool override;
  virtual auto inAcceptance(const MuonTrackExtrapolation&) const noexcept
      -> bool override;
  virtual auto hitsAndOccupancies(const LHCb::Track&,
                                  const MuonTrackExtrapolation&,
                                  const MuonHitHandler& hitHandler) const
      -> std::tuple<CommonConstMuonHits, MuonTrackOccupancies> override;
  virtual auto extractCrossed(const CommonConstMuonHits&) const noexcept
      -> std::tuple<CommonConstMuonHits, MuonTrackOccupancies> override;
  virtual auto isMuon(const MuonTrackOccupancies&, const float) 
      const noexcept -> bool override;
  virtual auto isMuonLoose(const MuonTrackOccupancies&, float) const noexcept
      -> bool override;
  virtual auto foi(unsigned int, unsigned int, float) const noexcept
      -> std::pair<float, float> override;

  virtual unsigned int getFirstUsedStation() const noexcept override;

 private:
  // Helpers
  auto i_foi(unsigned int, unsigned int, float) const noexcept -> std::pair<float, float>;
  // Members
  unsigned int m_firstUsedStation;
  DeMuonDetector* m_muonDet = nullptr;               // non-owning
  double m_foiFactor = 1.;
  double m_preSelMomentum = 0.;
  std::vector<float> m_stationZ;
  MuonTrackExtrapolation m_regionInner, m_regionOuter;
  std::array<std::vector<double>, 3> m_foiParamX, m_foiParamY;
  std::vector<double> m_momentumCuts;
  size_t m_stationsCount;
  size_t m_regionsCount;
  Gaudi::Property<unsigned int> num_active_stations {
      this, "NumActiveStations", 4,
          "Number of active MUON stations. In Run II there are "
          "5 stations, 4 used in MuonID."
          " In Run III there will be 4 stations, all used. So in"
          " foreseeable future you want this = 4."
          " Also something might break if the number changes..."};
};

#endif  // COMMONMUONTOOL_H_
