/** Implementation of DLLMuonTool
 *
 * 2015-01-12: Ricardo Vazquez Gomez
 */
#include <boost/numeric/conversion/cast.hpp>

#include "DLLMuonTool.h"

// Let's define constants we do not need to calculate
// ATanh(0.5)
static const auto ATANH05 = std::atanh(0.5);

DECLARE_COMPONENT( DLLMuonTool )

/** C'tor
 * Also declares an interface to that the tool can be obtained in Gaudi via
 * tool<...>(...)
 */
DLLMuonTool::DLLMuonTool(const std::string& type, const std::string& name,
                         const IInterface* parent)
    : GaudiTool(type, name, parent) {
  declareInterface<DLLMuonTool>(this);
}

/** Initialises the tool.
 * Loads helpers and extracts detector information and variables from the
 * CondDB.
 */
StatusCode DLLMuonTool::initialize() {
  auto sc = GaudiTool::initialize();
  if (sc.isFailure()) {
    return sc;
  }
  m_muonTool = tool<ICommonMuonTool>("CommonMuonTool");
  m_muonDet = getDet<DeMuonDetector>(DeMuonLocation::Default);
  m_stationsCount = boost::numeric_cast<size_t>(m_muonDet->stations());
  m_regionsCount = m_muonDet->regions() / m_stationsCount;
  iM2 = m_muonTool->getFirstUsedStation();
  iM3 = iM2 + 1;
  iM4 = iM3 + 1;
  iM5 = iM4 + 1;

  if (m_OverrideDB) {
    if (msgLevel(MSG::INFO))
      info() << "Initialise: OverrideDB=true. Data from Conditions database "
                "will be ignored." << endmsg;
    if (msgLevel(MSG::INFO))
      info() << "Initialise: Using values read from options file" << endmsg;
  } else {  // Read values from database
    // Prepare to load information from conditions data base
    Condition* condFoiFactor = nullptr;
    Condition* condPreSelMomentum = nullptr;
    Condition* condMomentumCuts = nullptr;
    Condition* condNumberMupBins = nullptr;
    Condition* condMupBins = nullptr;

    Condition* condNonMuLandauParameterR1 = nullptr;
    Condition* condNonMuLandauParameterR2 = nullptr;
    Condition* condNonMuLandauParameterR3 = nullptr;
    Condition* condNonMuLandauParameterR4 = nullptr;
    // For DLL flag 4
    Condition* condtanhScaleFactors = nullptr;
    Condition* condtanhCumulHistoMuonR1 = nullptr;
    Condition* condtanhCumulHistoMuonR2 = nullptr;
    Condition* condtanhCumulHistoMuonR3 = nullptr;
    Condition* condtanhCumulHistoMuonR4 = nullptr;

    registerCondition<DLLMuonTool>("Conditions/ParticleID/Muon/FOIfactor", condFoiFactor);
    registerCondition<DLLMuonTool>("Conditions/ParticleID/Muon/PreSelMomentum", condPreSelMomentum);
    registerCondition<DLLMuonTool>("Conditions/ParticleID/Muon/MomentumCuts", condMomentumCuts);
    registerCondition<DLLMuonTool>("Conditions/ParticleID/Muon/nMupBins", condNumberMupBins);
    registerCondition<DLLMuonTool>("Conditions/ParticleID/Muon/MupBins", condMupBins);

    registerCondition<DLLMuonTool>(
        "Conditions/ParticleID/Muon/NonMuLandauParameterR1",
        condNonMuLandauParameterR1);
    registerCondition<DLLMuonTool>(
        "Conditions/ParticleID/Muon/NonMuLandauParameterR2",
        condNonMuLandauParameterR2);
    registerCondition<DLLMuonTool>(
        "Conditions/ParticleID/Muon/NonMuLandauParameterR3",
        condNonMuLandauParameterR3);
    registerCondition<DLLMuonTool>(
        "Conditions/ParticleID/Muon/NonMuLandauParameterR4",
        condNonMuLandauParameterR4);

    // for DLL flag 4
    registerCondition<DLLMuonTool>(
        "Conditions/ParticleID/Muon/tanhScaleFactors",
        condtanhScaleFactors);

    registerCondition<DLLMuonTool>(
        "Conditions/ParticleID/Muon/tanhCumulHistoMuonR1",
        condtanhCumulHistoMuonR1);
    registerCondition<DLLMuonTool>(
        "Conditions/ParticleID/Muon/tanhCumulHistoMuonR2",
        condtanhCumulHistoMuonR2);
    registerCondition<DLLMuonTool>(
        "Conditions/ParticleID/Muon/tanhCumulHistoMuonR3",
        condtanhCumulHistoMuonR3);
    registerCondition<DLLMuonTool>(
        "Conditions/ParticleID/Muon/tanhCumulHistoMuonR4",
        condtanhCumulHistoMuonR4);

    // Update conditions
    sc = runUpdate();
    if (sc.isFailure()) {
      Error("Could not update conditions from the CondDB.");
      return sc;
    }
    // Extract parameters
    foiFactor_ = condFoiFactor->param<double>("FOIfactor");
    preSelMomentum_ = condPreSelMomentum->param<double>("PreSelMomentum");
    momentumCuts_ = condMomentumCuts->paramVect<double>("MomentumCuts");
    m_MupBinsR1 = condMupBins->paramVect<double>("MupBinsR1");
    m_MupBinsR2 = condMupBins->paramVect<double>("MupBinsR2");
    m_MupBinsR3 = condMupBins->paramVect<double>("MupBinsR3");
    m_MupBinsR4 = condMupBins->paramVect<double>("MupBinsR4");

    // Non-Muons - Region 1-2-3-4: // flag 4
    m_NonMuLanParR1 =
        condNonMuLandauParameterR1->paramVect<double>("NonMuLandauParameterR1");
    m_NonMuLanParR2 =
        condNonMuLandauParameterR2->paramVect<double>("NonMuLandauParameterR2");
    m_NonMuLanParR3 =
        condNonMuLandauParameterR3->paramVect<double>("NonMuLandauParameterR3");
    m_NonMuLanParR4 =
        condNonMuLandauParameterR4->paramVect<double>("NonMuLandauParameterR4");

    // For DLL flag = 4. The name of the parameter changes for flag = 5
    m_tanhScaleFactorsMuonR1 =
        condtanhScaleFactors->paramVect<double>("tanhScaleFactorsR1");
    m_tanhScaleFactorsMuonR2 =
        condtanhScaleFactors->paramVect<double>("tanhScaleFactorsR2");
    m_tanhScaleFactorsMuonR3 =
        condtanhScaleFactors->paramVect<double>("tanhScaleFactorsR3");
    m_tanhScaleFactorsMuonR4 =
        condtanhScaleFactors->paramVect<double>("tanhScaleFactorsR4");

    m_tanhCumulHistoMuonR1_1 =
        condtanhCumulHistoMuonR1->paramVect<double>("tanhCumulHistoMuonR1_1");
    m_tanhCumulHistoMuonR1_2 =
        condtanhCumulHistoMuonR1->paramVect<double>("tanhCumulHistoMuonR1_2");
    m_tanhCumulHistoMuonR1_3 =
        condtanhCumulHistoMuonR1->paramVect<double>("tanhCumulHistoMuonR1_3");
    m_tanhCumulHistoMuonR1_4 =
        condtanhCumulHistoMuonR1->paramVect<double>("tanhCumulHistoMuonR1_4");
    m_tanhCumulHistoMuonR1_5 =
        condtanhCumulHistoMuonR1->paramVect<double>("tanhCumulHistoMuonR1_5");
    m_tanhCumulHistoMuonR1_6 =
        condtanhCumulHistoMuonR1->paramVect<double>("tanhCumulHistoMuonR1_6");
    m_tanhCumulHistoMuonR1_7 =
        condtanhCumulHistoMuonR1->paramVect<double>("tanhCumulHistoMuonR1_7");

    m_tanhCumulHistoMuonR2_1 =
        condtanhCumulHistoMuonR2->paramVect<double>("tanhCumulHistoMuonR2_1");
    m_tanhCumulHistoMuonR2_2 =
        condtanhCumulHistoMuonR2->paramVect<double>("tanhCumulHistoMuonR2_2");
    m_tanhCumulHistoMuonR2_3 =
        condtanhCumulHistoMuonR2->paramVect<double>("tanhCumulHistoMuonR2_3");
    m_tanhCumulHistoMuonR2_4 =
        condtanhCumulHistoMuonR2->paramVect<double>("tanhCumulHistoMuonR2_4");
    m_tanhCumulHistoMuonR2_5 =
        condtanhCumulHistoMuonR2->paramVect<double>("tanhCumulHistoMuonR2_5");

    m_tanhCumulHistoMuonR3_1 =
        condtanhCumulHistoMuonR3->paramVect<double>("tanhCumulHistoMuonR3_1");
    m_tanhCumulHistoMuonR3_2 =
        condtanhCumulHistoMuonR3->paramVect<double>("tanhCumulHistoMuonR3_2");
    m_tanhCumulHistoMuonR3_3 =
        condtanhCumulHistoMuonR3->paramVect<double>("tanhCumulHistoMuonR3_3");
    m_tanhCumulHistoMuonR3_4 =
        condtanhCumulHistoMuonR3->paramVect<double>("tanhCumulHistoMuonR3_4");
    m_tanhCumulHistoMuonR3_5 =
        condtanhCumulHistoMuonR3->paramVect<double>("tanhCumulHistoMuonR3_5");

    m_tanhCumulHistoMuonR4_1 =
        condtanhCumulHistoMuonR4->paramVect<double>("tanhCumulHistoMuonR4_1");
    m_tanhCumulHistoMuonR4_2 =
        condtanhCumulHistoMuonR4->paramVect<double>("tanhCumulHistoMuonR4_2");
    m_tanhCumulHistoMuonR4_3 =
        condtanhCumulHistoMuonR4->paramVect<double>("tanhCumulHistoMuonR4_3");
    m_tanhCumulHistoMuonR4_4 =
        condtanhCumulHistoMuonR4->paramVect<double>("tanhCumulHistoMuonR4_4");
    m_tanhCumulHistoMuonR4_5 =
        condtanhCumulHistoMuonR4->paramVect<double>("tanhCumulHistoMuonR4_5");

  }
  m_tanhScaleFactorsMuon.push_back(&m_tanhScaleFactorsMuonR1.value());
  m_tanhScaleFactorsMuon.push_back(&m_tanhScaleFactorsMuonR2.value());
  m_tanhScaleFactorsMuon.push_back(&m_tanhScaleFactorsMuonR3.value());
  m_tanhScaleFactorsMuon.push_back(&m_tanhScaleFactorsMuonR4.value());

  m_tanhScaleFactorsNonMuon.push_back(&m_tanhScaleFactorsNonMuonR1.value());
  m_tanhScaleFactorsNonMuon.push_back(&m_tanhScaleFactorsNonMuonR2.value());
  m_tanhScaleFactorsNonMuon.push_back(&m_tanhScaleFactorsNonMuonR3.value());
  m_tanhScaleFactorsNonMuon.push_back(&m_tanhScaleFactorsNonMuonR4.value());

  // DLLflag = 3 and 4 for muons
  m_tanhCumulHistoMuonR1.push_back(&m_tanhCumulHistoMuonR1_1.value());
  m_tanhCumulHistoMuonR1.push_back(&m_tanhCumulHistoMuonR1_2.value());
  m_tanhCumulHistoMuonR1.push_back(&m_tanhCumulHistoMuonR1_3.value());
  m_tanhCumulHistoMuonR1.push_back(&m_tanhCumulHistoMuonR1_4.value());
  m_tanhCumulHistoMuonR1.push_back(&m_tanhCumulHistoMuonR1_5.value());
  m_tanhCumulHistoMuonR1.push_back(&m_tanhCumulHistoMuonR1_6.value());
  m_tanhCumulHistoMuonR1.push_back(&m_tanhCumulHistoMuonR1_7.value());

  m_tanhCumulHistoMuonR2.push_back(&m_tanhCumulHistoMuonR2_1.value());
  m_tanhCumulHistoMuonR2.push_back(&m_tanhCumulHistoMuonR2_2.value());
  m_tanhCumulHistoMuonR2.push_back(&m_tanhCumulHistoMuonR2_3.value());
  m_tanhCumulHistoMuonR2.push_back(&m_tanhCumulHistoMuonR2_4.value());
  m_tanhCumulHistoMuonR2.push_back(&m_tanhCumulHistoMuonR2_5.value());

  m_tanhCumulHistoMuonR3.push_back(&m_tanhCumulHistoMuonR3_1.value());
  m_tanhCumulHistoMuonR3.push_back(&m_tanhCumulHistoMuonR3_2.value());
  m_tanhCumulHistoMuonR3.push_back(&m_tanhCumulHistoMuonR3_3.value());
  m_tanhCumulHistoMuonR3.push_back(&m_tanhCumulHistoMuonR3_4.value());
  m_tanhCumulHistoMuonR3.push_back(&m_tanhCumulHistoMuonR3_5.value());

  m_tanhCumulHistoMuonR4.push_back(&m_tanhCumulHistoMuonR4_1.value());
  m_tanhCumulHistoMuonR4.push_back(&m_tanhCumulHistoMuonR4_2.value());
  m_tanhCumulHistoMuonR4.push_back(&m_tanhCumulHistoMuonR4_3.value());
  m_tanhCumulHistoMuonR4.push_back(&m_tanhCumulHistoMuonR4_4.value());
  m_tanhCumulHistoMuonR4.push_back(&m_tanhCumulHistoMuonR4_5.value());

  m_tanhCumulHistoMuon.push_back(&m_tanhCumulHistoMuonR1);
  m_tanhCumulHistoMuon.push_back(&m_tanhCumulHistoMuonR2);
  m_tanhCumulHistoMuon.push_back(&m_tanhCumulHistoMuonR3);
  m_tanhCumulHistoMuon.push_back(&m_tanhCumulHistoMuonR4);

  // DLLFlag = 5 for non-muons
  m_tanhCumulHistoNonMuonR1.push_back(&m_tanhCumulHistoNonMuonR1_1.value());
  m_tanhCumulHistoNonMuonR1.push_back(&m_tanhCumulHistoNonMuonR1_2.value());
  m_tanhCumulHistoNonMuonR1.push_back(&m_tanhCumulHistoNonMuonR1_3.value());
  m_tanhCumulHistoNonMuonR1.push_back(&m_tanhCumulHistoNonMuonR1_4.value());
  m_tanhCumulHistoNonMuonR1.push_back(&m_tanhCumulHistoNonMuonR1_5.value());
  m_tanhCumulHistoNonMuonR1.push_back(&m_tanhCumulHistoNonMuonR1_6.value());
  m_tanhCumulHistoNonMuonR1.push_back(&m_tanhCumulHistoNonMuonR1_7.value());

  m_tanhCumulHistoNonMuonR2.push_back(&m_tanhCumulHistoNonMuonR2_1.value());
  m_tanhCumulHistoNonMuonR2.push_back(&m_tanhCumulHistoNonMuonR2_2.value());
  m_tanhCumulHistoNonMuonR2.push_back(&m_tanhCumulHistoNonMuonR2_3.value());
  m_tanhCumulHistoNonMuonR2.push_back(&m_tanhCumulHistoNonMuonR2_4.value());
  m_tanhCumulHistoNonMuonR2.push_back(&m_tanhCumulHistoNonMuonR2_5.value());

  m_tanhCumulHistoNonMuonR3.push_back(&m_tanhCumulHistoNonMuonR3_1.value());
  m_tanhCumulHistoNonMuonR3.push_back(&m_tanhCumulHistoNonMuonR3_2.value());
  m_tanhCumulHistoNonMuonR3.push_back(&m_tanhCumulHistoNonMuonR3_3.value());
  m_tanhCumulHistoNonMuonR3.push_back(&m_tanhCumulHistoNonMuonR3_4.value());
  m_tanhCumulHistoNonMuonR3.push_back(&m_tanhCumulHistoNonMuonR3_5.value());

  m_tanhCumulHistoNonMuonR4.push_back(&m_tanhCumulHistoNonMuonR4_1.value());
  m_tanhCumulHistoNonMuonR4.push_back(&m_tanhCumulHistoNonMuonR4_2.value());
  m_tanhCumulHistoNonMuonR4.push_back(&m_tanhCumulHistoNonMuonR4_3.value());

  m_tanhCumulHistoNonMuon.push_back(&m_tanhCumulHistoNonMuonR1);
  m_tanhCumulHistoNonMuon.push_back(&m_tanhCumulHistoNonMuonR2);
  m_tanhCumulHistoNonMuon.push_back(&m_tanhCumulHistoNonMuonR3);
  m_tanhCumulHistoNonMuon.push_back(&m_tanhCumulHistoNonMuonR4);

  StatusCode scNorm = calcLandauNorm();
  if (scNorm.isFailure()) {
    return Error(" Normalizations of Landaus not properly set ", scNorm);
  }
  return sc;
}

/**Method to calculate the distance from the hit to the extrapolated position
*/
float DLLMuonTool::calcDist(
    const ICommonMuonTool::MuonTrackExtrapolation& extrapolation,
    const CommonConstMuonHits& hits) const {
  float m_dist = 0.;
  float mCoordX[s_usedStations] = {0., 0., 0., 0.};
  float mCoordY[s_usedStations] = {0., 0., 0., 0.};
  float mCoordDX[s_usedStations] = {0., 0., 0., 0.};
  float mCoordDY[s_usedStations] = {0., 0., 0., 0.};
  const unsigned int firstUsedStation = m_muonTool->getFirstUsedStation();
  // TODO(kazeevn) Why Lambda and for_each and not just a loop?
  // We anyway do it for the side effects.
  for_each(begin(hits), end(hits), [&](const CommonMuonHit* hit) {
      const unsigned int s = hit->station();
      if (s < firstUsedStation) {
	return;
      }
      const unsigned int index_from_first_used = s - firstUsedStation;
      if (mCoordX[index_from_first_used] == 0.) {  // first coord found.
        mCoordX[index_from_first_used] = hit->x();
        mCoordY[index_from_first_used] = hit->y();
        // difference between x,y and the extrapolated x,y
        mCoordDX[index_from_first_used] = (hit->x() - extrapolation[s].first) / hit->dx();
        mCoordDY[index_from_first_used] = (hit->y() - extrapolation[s].second) / hit->dy();
        if (msgLevel(MSG::DEBUG)) {
          debug() << "calcDist : In station " << s << endmsg;
          debug() << "calcDist : x = " << hit->x()
                  << ", m_trackX[station] = " << extrapolation[s].first
                  << ", dx = " << hit->dx() << endmsg;
          debug() << "calcDist : y = " << hit->y()
                  << ", m_trackY[station] = " << extrapolation[s].second
                  << ", dy = " << hit->dy() << endmsg;
	}
      } else {
        // get best match in X and Y
        if (msgLevel(MSG::DEBUG)) {
          debug() << "(x - m_trackX[station])*(x - m_trackX[station]) = "
                  << (hit->x() - extrapolation[s].first) *
              (hit->x() - extrapolation[s].first) << endmsg;
          debug() << "(y - m_trackY[station])*(y - m_trackY[station]) = "
                  << (hit->y() - extrapolation[s].second) *
              (hit->y() - extrapolation[s].second) << endmsg;
          debug() << "pow((mCoordX[station] - m_trackX[station]),2) = "
                  << (mCoordX[index_from_first_used] - extrapolation[s].first)*(mCoordX[index_from_first_used] - extrapolation[s].first)
                  << endmsg;
          debug() << "pow((mCoordY[station] - m_trackY[station]),2) = "
                  << (mCoordY[index_from_first_used] - extrapolation[s].second)*(mCoordY[index_from_first_used] - extrapolation[s].second)
                  << endmsg;
	}
        if ((hit->x() - extrapolation[s].first)*(hit->x() - extrapolation[s].first) +
            (hit->y() - extrapolation[s].second)*(hit->y() - extrapolation[s].second) <
            (mCoordX[index_from_first_used] - extrapolation[s].first)*(mCoordX[index_from_first_used] - extrapolation[s].first) +
            (mCoordY[index_from_first_used] - extrapolation[s].second)*(mCoordY[index_from_first_used] - extrapolation[s].second)) {
          // this Coord is a better match
          mCoordX[index_from_first_used] = hit->x();
          mCoordY[index_from_first_used] = hit->y();
          mCoordDX[index_from_first_used] = (hit->x() - extrapolation[s].first) / hit->dx();
          mCoordDY[index_from_first_used] = (hit->y() - extrapolation[s].second) / hit->dy();
          if (msgLevel(MSG::DEBUG)) {
            debug() << "calcDist : In station " << s << endmsg;
            debug() << "calcDist : x = " << hit->x()
                    << ", m_trackX[station] = " << extrapolation[s].first
                    << ", dx = " << hit->dx() << endmsg;
            debug() << "calcDist : y = " << hit->y()
                    << ", m_trackY[station] = " << extrapolation[s].second
                    << ", dy = " << hit->dy() << endmsg;
          }
        }
      }
    }
  );

  // calculate the square of the distances
  unsigned nstn = 0;
  for (unsigned stn = 0; stn != s_usedStations; ++stn) {
    if (msgLevel(MSG::DEBUG)) {
      debug() << "calcDist: station: " << stn << " mCoordDX = " << mCoordDX[stn]
              << " mCoordDY = " << mCoordDY[stn] << endmsg;
    }
    if (mCoordDX[stn] != 0. && mCoordDY[stn] != 0.) {
      if (msgLevel(MSG::DEBUG)) {
        debug() << "nstn = " << nstn << endmsg;
        debug() << "mCoordDX[stn]*mCoordDX[stn] = "
                << mCoordDX[stn] * mCoordDX[stn] << endmsg;
        debug() << "mCoordDY[stn]*mCoordDY[stn] = "
                << mCoordDY[stn] * mCoordDY[stn] << endmsg;
        debug() << "m_dist = " << m_dist << endmsg;
      }
      ++nstn;
      m_dist += mCoordDX[stn]*mCoordDX[stn] + mCoordDY[stn]*mCoordDY[stn];
    }
  }
  if (msgLevel(MSG::DEBUG)) debug() << "m_dist Total = " << m_dist << endmsg;
  m_dist = m_dist / nstn;  // divide by the number of station

  return m_dist;
}

/** Calculate the number of tracks that share hits
 */
bool DLLMuonTool::calcNShared(
    LHCb::MuonPID* muonid, LHCb::MuonPIDs* pMuids,
    const CommonConstMuonHits& hits,
    const ICommonMuonTool::MuonTrackExtrapolation& extr,
    std::map<LHCb::MuonPID*, float>* m_muonDistMap,
    std::map<LHCb::MuonPID*, CommonConstMuonHits>* m_muonMap) const {

  unsigned nNSH;
  float dsquare1;
  // Calculate dist only if it is not store already in the distance map
  // TODO: the following should be made a function
  if (m_muonDistMap->find(muonid) == m_muonDistMap->end()) {
    // not found
    dsquare1 = calcDist(extr, hits);
    if (msgLevel(MSG::DEBUG))
      debug() << "     For ORIGINAL track with momentum p = "
              << (muonid->idTrack())->p() << ". Dist 1 = " << dsquare1
              << ". The track container has " << pMuids->size() << "tracks."
              << endmsg;
    if (dsquare1 < 0.) {
      muonid->setNShared(100);
      return Warning("calcDist 1 failure", StatusCode::FAILURE).isSuccess();
    }
    // Store distance in the muon map
    (*m_muonDistMap)[muonid] = dsquare1;
  } else {
    // Get distance from the muon map
    dsquare1 = (*m_muonDistMap)[muonid];
  }

  CommonConstMuonHits hits2;

  for (LHCb::MuonPID* pMuon: *pMuids) {
    // skip if this muonPID is the same as the input or
    // if IsMuonLoose is false
    if (!pMuon->IsMuonLoose() || pMuon == muonid) continue;
    if (msgLevel(MSG::DEBUG))
      debug() <<
          "---------> Start the computation of Dist 2 for track with p = "
          << pMuon->idTrack()->p() << " <----------" << endmsg;
    // see if there are shared hits between the muonIDs
    if (msgLevel(MSG::DEBUG))
      debug() << "Comparing primary with p = " << (muonid->idTrack())->p()
              << " with secondary of p = " << pMuon->idTrack()->p()
              << endmsg;
    // Get hits from muon hits map
    hits2 = (*m_muonMap)[pMuon];
    if (compareHits(hits, hits2)) {
      // get dist for this muonID

      float dsquare2;
      // Calculate dist only if it is not store already in the distance map
      if (m_muonDistMap->find(pMuon) == m_muonDistMap->end()) {
        // not found
        const LHCb::Track* track2 =
          pMuon->idTrack();  // get the track of the muonID
        const auto extr2 = m_muonTool->extrapolateTrack(*track2);

        dsquare2 = calcDist(extr2, hits2);

        if (msgLevel(MSG::DEBUG))
          debug() << "     For ORIGINAL track with momentum p = "
                  << (muonid->idTrack())->p() << ". Dist 1 = " << dsquare1
                  << ". The track container has " << pMuids->size() <<
                  "tracks." << endmsg;
        if (dsquare2 < 0.) {
          muonid->setNShared(100);
          return Warning("calcDist 2 failure", StatusCode::FAILURE).isSuccess();
        }
        if (msgLevel(MSG::DEBUG))
          debug() << "            For SECONDARY track with momentum = "
                  << track2->p() << ". Dist 2 = " << dsquare2 << endmsg;
        // Store distance in the muon map
        (*m_muonDistMap)[pMuon] = dsquare2;
      } else {
        // Get distance from the muon map
        dsquare2 = (*m_muonDistMap)[pMuon];
      }

      // the muonID which gets the number of shared hits is the one
      // which has the biggest dist
      if (dsquare2 < dsquare1) {
        nNSH = muonid->nShared();
        if (msgLevel(MSG::DEBUG))
          debug() <<
                  "Setting the nShared to the original track. NShared =  "
                  << nNSH << endmsg;
        muonid->setNShared(nNSH + 1);
      } else {
        nNSH = pMuon->nShared();
        if (msgLevel(MSG::DEBUG))
          debug() << "Setting the nShared to the secondary track. NShared =  "
                  << nNSH << endmsg;
        pMuon->setNShared(nNSH + 1);
      }
    }
  }
  if (msgLevel(MSG::DEBUG))
    debug() << "nShared = " << muonid->nShared() << endmsg;

  // Store hits in the muon map for this track
  (*m_muonMap)[muonid] = hits;
  return true;
}

// Get the bin for the given P
unsigned DLLMuonTool::GetPbin(const float p, const unsigned region) const {
  const std::vector<double>* pBins = [&]() -> const std::vector<double>* {
    switch (region + 1) {
      case 1: {
        return &m_MupBinsR1.value();
        break;
      }
      case 2: {
        return &m_MupBinsR2.value();
        break;
      }
      case 3: {
        return &m_MupBinsR3.value();
        break;
      }
      case 4: {
        return &m_MupBinsR4.value();
        break;
      }
      default: return nullptr;
    }}();

  if (msgLevel(MSG::VERBOSE))
    verbose() << "GetPbin: region+1 " << region + 1 << " p " << p
              << " pBins vector address: " << pBins << endmsg;
  if (!pBins) {
    error() << "GetPbin: No match to a pBins vector. "
        "Null pBins pointer. Return default value 0." << endmsg;
    return 0;
  }


  unsigned end(pBins->size());
  for (unsigned iBin = 0; iBin != end; ++iBin) {
    if (msgLevel(MSG::VERBOSE))
      verbose() << "GetPbin:\tiBin " << iBin << " pBins[iBin] "
                << (*pBins)[iBin] << endmsg;
    if (p < (*pBins)[iBin]) return iBin;
  }
  return pBins->size();  // last bin. npBins goes from 0 to (Gaias npBin)+1
}

std::vector<double> DLLMuonTool::loadNonMuLandauParam(
    const ICommonMuonTool::MuonTrackExtrapolation& extrapolation) {
  std::vector<double> parNonMu;

  // Get region to use to calculate the LL
  int region = findTrackRegion(extrapolation);

  if (region == 0) {                                            // Region 1
    for (unsigned i = 0; i != 3; ++i) {
      parNonMu[i] = m_NonMuLanParR1[i];
    }
  } else if (region == 1) {  // Region 2
    for (unsigned i = 0; i != 3; ++i) {
      parNonMu[i] = m_NonMuLanParR2[i];
    }
  } else if (region == 2) {  // Region 3
    for (unsigned i = 0; i != 3; ++i) {
      parNonMu[i] = m_NonMuLanParR3[i];
    }
  } else if (region == 3) {  // Region 4
    for (unsigned i = 0; i != 3; ++i) {
      parNonMu[i] = m_NonMuLanParR4[i];
    }
  } else
  {
    error() << "loadNonMuLandauParam: Not valid region! This shoud not happen. Using R1.  " << endmsg;
    for (unsigned i = 0; i != 3; ++i) {
      parNonMu[i] = m_NonMuLanParR1[i];
    }
  }

  return parNonMu;
}

unsigned int DLLMuonTool::getRegionFromPosition(
    const ICommonMuonTool::MuonTrackExtrapolation& extrapolation) const {
  // Returns a Region from the extrapolation in M2 in case all else does not work
  // Yes, this function is hardcoded:
  // it is used only for very strange tracks whose extrapolation is in the gaps in various stations

  //  Following numbers from TDR, page 3.
  //   R1  x(300, 600)    y(250,500)
  //   R2  x(600, 1200)   y(500, 1000)
  //   R3  x(1200, 2400)  y(1000, 2001)
  //   R4  x(2400,4804)   y(2001, 40003)
  float x = extrapolation[iM2].first;
  float y = extrapolation[iM2].second;
  if(msgLevel(MSG::DEBUG))
    debug() << "getRegionFromPosition: getting region.  iM2 = "<< iM2
            << ". Extrapolation in x = " << extrapolation[iM2].first
            << ", y = " << extrapolation[iM2].second << endmsg;


  if(x > 2400 || y > 2001) return 3; // R4 or larger
  if(x > 1200 || y > 1000) return 2; // R3
  if(x > 600  || y > 500)  return 1; // R2
  else                     return 0; // R1
  // Return R1 also in beam-pipe case
}

int DLLMuonTool::findTrackRegion(
    const ICommonMuonTool::MuonTrackExtrapolation& extrapolation) const {
  // Returns a region for a given track extrapolation
  // It always returns a valid region (0,... ,3) = (R1,...R4)

  const std::vector<int> regions = findTrackRegions(extrapolation);
  int region = regions[iM2];  // M2
  // Find a non zero region
  if (region < 0){
    // TODO(kazeevn) investigate the hardcoded 3
    for (unsigned int i = m_stationsCount-3; i != m_stationsCount; ++i){ //M3, M4, M5
      if(msgLevel(MSG::DEBUG))
        debug() << format("No valid region in M2 looking in i=%i",i)   << endmsg;

      region = regions[i];
      if(region >= 0)break;
    }

    // If all else fails get region from x,y position in M2
    if(region<0){
      if(msgLevel(MSG::DEBUG))
        debug() << "No valid region found, going to call getRegionFromPosition"  << endmsg;

      region = getRegionFromPosition(extrapolation);
    }

  }

  return region;
}



std::vector<int> DLLMuonTool::findTrackRegions(
    const ICommonMuonTool::MuonTrackExtrapolation& extrapolation) const {
  //=====================================================================
  // comment: Returns the muon detector region of the extrapolated track;
  //=====================================================================

  std::vector<int> trackRegion(m_stationsCount, -1);
  // Region of the track extrapolated:

  for (unsigned sta = 0; sta != m_stationsCount; ++sta) {
    int chnum = -1;
    int regnum = -1;
    m_muonDet->Pos2StChamberNumber(extrapolation[sta].first,
                              extrapolation[sta].second, sta, chnum,
                              regnum).ignore();
    trackRegion[sta] = regnum;
    if (trackRegion[sta] < 0 && msgLevel(MSG::DEBUG)) {
      debug() << format(
                     " Track extrapolation in station %d gives not-existent "
                     "region ",
                     sta) << endmsg;
    }
  }

  if (msgLevel(MSG::DEBUG)) debug() << "TrackRegions" << trackRegion << endmsg;
  return trackRegion;
}

//=====================================================================
float DLLMuonTool::calc_closestDist(
    const LHCb::Track& track,
    const ICommonMuonTool::MuonTrackExtrapolation& extrapolation,
    const CommonConstMuonHits& hits,
    const std::vector<unsigned>& occupancies) const {
  //=====================================================================
  // comment: Returns the squared distance calculated with closest hit
  //=====================================================================


  // TODO(kazeevn) port all 4/5 code to this stack implementation
  std::array<float, 5> closest_x_data = {-1, -1, -1, -1, -1};
  boost::iterator_range<float*> closest_x(closest_x_data.data(),
					   closest_x_data.data() + m_stationsCount);
  std::vector<float> closest_y(m_stationsCount, -1);
  std::vector<float> Fdist(m_stationsCount, 0);
  // TODO(kazeevn) what is the harddcoded value?
  std::vector<float> small_dist(m_stationsCount, 10000000.);
  std::vector<float> closest_region(m_stationsCount, -1);
  float closest_dist = 0;

  // Get closest hits
  float foiXDim, foiYDim;

  float p = track.p();
  for_each(
      std::begin(hits), std::end(hits),
      [&](const CommonMuonHit* hit) {  // hits in FOI
        unsigned s = hit->tile().station();
        unsigned r = hit->tile().region();
        foiXDim = hit->dx() * foiFactor_ * m_muonTool->foi(s, r, p).first;
        foiYDim = hit->dy() * foiFactor_ * m_muonTool->foi(s, r, p).second;
        if (msgLevel(MSG::DEBUG))
          debug() << "The p = " << p << ", with foiFactor = " << foiFactor_
                  << ", foiX = " << m_muonTool->foi(s, r, p).first
                  << ", and foiY = " << m_muonTool->foi(s, r, p).second
                  << endmsg;
        if (msgLevel(MSG::DEBUG))
          debug() << "For hit in x = " << hit->x() << ", and y= " << hit->y()
                  << ". Extrapolation in x = " << extrapolation[s].first
                  << ", y = " << extrapolation[s].second << endmsg;
        if (msgLevel(MSG::DEBUG))
          debug() << "The FOI dimension is, foiXDim  = " << foiXDim
                  << ", foiYDim = " << foiYDim << endmsg;
        if (s >= m_muonTool->getFirstUsedStation()) {
          if ((fabs(hit->x() - extrapolation[s].first) < foiXDim) &&
              (fabs(hit->y() - extrapolation[s].second) < foiYDim)) {
            if (msgLevel(MSG::DEBUG))
              debug() << "residX = " << fabs(hit->x() - extrapolation[s].first)
                      << ", and residY = "
                      << fabs(hit->y() - extrapolation[s].second) << endmsg;

            Fdist[s] = pow((hit->x() - extrapolation[s].first) / hit->dx(), 2) +
                       pow((hit->y() - extrapolation[s].second) / hit->dy(), 2);
            if (msgLevel(MSG::DEBUG))
              debug() << "FDist in station " << s << " = " << Fdist[s]
                      << endmsg;
            if (Fdist[s] < small_dist[s]) {
              small_dist[s] = Fdist[s];
              closest_x[s] = (hit->x() - extrapolation[s].first) / hit->dx();
              closest_y[s] = (hit->y() - extrapolation[s].second) / hit->dy();
              closest_region[s] = r;
            }  // Fdist
          }    // foi
        }      // station
      });      // hit

  if (msgLevel(MSG::DEBUG)) {
    debug()
        << "calc_closestDist: station closest_region \t closest_x \t closest_y"
        << endmsg;
    for (unsigned ist = 0; ist != m_stationsCount; ++ist) {
      debug() << ist << " " << closest_region[ist] << "\t" << closest_x[ist]
              << "\t" << closest_y[ist] << endmsg;
    }
  }


  if (p > preSelMomentum_ &&
      p < momentumCuts_[0]) {  // JHLJHL: Check station indices whith no M1
                               // configuration 30/08/2013
    // 3 or 2 stations
    if (occupancies[iM2] > 0 && occupancies[iM3] > 0 &&
        occupancies[iM4] > 0) {  // M2 &&M3 && M4
      closest_dist = closest_x[iM2]*closest_x[iM2] + closest_y[iM2]*closest_y[iM2] +
                     closest_x[iM3]*closest_x[iM3] + closest_y[iM3]*closest_y[iM3] +
                     closest_x[iM4]*closest_x[iM4] + closest_y[iM4]*closest_y[iM4];
      closest_dist = closest_dist / 3.;
      return closest_dist;
    }
    if (occupancies[iM2] > 0 && occupancies[iM3] > 0) {  // M2 &&M3
      closest_dist = closest_x[iM2]*closest_x[iM2] + closest_y[iM2]*closest_y[iM2] +
                     closest_x[iM3]*closest_x[iM3] + closest_y[iM3]*closest_y[iM3];
      closest_dist = closest_dist / 2.;
      return closest_dist;
    }
    if (occupancies[iM2] > 0 && occupancies[iM4] > 0) {  // M2 &&M4
      closest_dist = closest_x[iM2]*closest_x[iM2] + closest_y[iM2]*closest_y[iM2] +
                     closest_x[iM4]*closest_x[iM4] + closest_y[iM4]*closest_y[iM4];
      closest_dist = closest_dist / 2.;
      return closest_dist;
    }
    if (occupancies[iM3] > 0 && occupancies[iM4] > 0) {  // M3 &&M4
      closest_dist = closest_x[iM3]*closest_x[iM3] + closest_y[iM3]*closest_y[iM3] +
                     closest_x[iM4]*closest_x[iM4] + closest_y[iM4]*closest_y[iM4];
      closest_dist = closest_dist / 2.;
      return closest_dist;
    }
  }  // 3-6

  if (p > momentumCuts_[0]) {
    if (occupancies[iM2] > 0 && occupancies[iM3] > 0 && occupancies[iM4] > 0 &&
        occupancies[iM5] > 0) {
      // M2 &&M3&&M4&&M5
      closest_dist = closest_x[iM2]*closest_x[iM2] + closest_y[iM2]*closest_y[iM2] +
                     closest_x[iM3]*closest_x[iM3] + closest_y[iM3]*closest_y[iM3] +
                     closest_x[iM4]*closest_x[iM4] + closest_y[iM4]*closest_y[iM4] +
                     closest_x[iM5]*closest_x[iM5] + closest_y[iM5]*closest_y[iM5];
      closest_dist = closest_dist / 4.;
      return closest_dist;
    }
    if (occupancies[iM2] > 0 && occupancies[iM3] > 0 &&
        occupancies[iM4]) {  // M2 && M3 && M4
      closest_dist = closest_x[iM2]*closest_x[iM2] + closest_y[iM2]*closest_y[iM2] +
                     closest_x[iM3]*closest_x[iM3] + closest_y[iM3]*closest_y[iM3] +
                     closest_x[iM4]*closest_x[iM4] + closest_y[iM4]*closest_y[iM4];
      closest_dist = closest_dist / 3.;
      return closest_dist;
    }
    if (occupancies[iM2] > 0 && occupancies[iM3] > 0 &&
        occupancies[iM5]) {  // M2 && M3 && M5
      closest_dist = closest_x[iM2]*closest_x[iM2] + closest_y[iM2]*closest_y[iM2] +
                     closest_x[iM3]*closest_x[iM3] + closest_y[iM3]*closest_y[iM3] +
                     closest_x[iM5]*closest_x[iM5] + closest_y[iM5]*closest_y[iM5];
      closest_dist = closest_dist / 3.;
      return closest_dist;
    }
    if (occupancies[iM3] > 0 && occupancies[iM4] > 0 &&
        occupancies[iM5] > 0) {  // M3 &&M4 && M5
      closest_dist = closest_x[iM3]*closest_x[iM3] + closest_y[iM3]*closest_y[iM3] +
                     closest_x[iM4]*closest_x[iM4] + closest_y[iM4]*closest_y[iM4] +
                     closest_x[iM5]*closest_x[iM5] + closest_y[iM5]*closest_y[iM5];
      closest_dist = closest_dist / 3.;
      return closest_dist;
    }
    if (occupancies[iM2] > 0 && occupancies[iM4] > 0 &&
        occupancies[iM5] > 0) {  // M2 &&M4 && M5
      closest_dist = closest_x[iM2]*closest_x[iM2] + closest_y[iM2]*closest_y[iM2] +
                     closest_x[iM4]*closest_x[iM4] + closest_y[iM4]*closest_y[iM4] +
                     closest_x[iM5]*closest_x[iM5] + closest_y[iM5]*closest_y[iM5];
      closest_dist = closest_dist / 3.;
      return closest_dist;
    }
  }
  if (!closest_dist) return 0;
  return closest_dist;
}

//=====================================================================
float DLLMuonTool::calc_ProbNonMu_tanh(float tanhdist0, unsigned pBin,
                                        unsigned region) const {
  unsigned nDistBins = (*(*m_tanhCumulHistoNonMuon[region])[pBin]).size();
  unsigned itanhdist = unsigned(tanhdist0 * nDistBins);

  if (itanhdist >= nDistBins) itanhdist = nDistBins - 1;

  if (msgLevel(MSG::DEBUG))
    debug() << "calc_ProbNonMu_tanh: region " << region << " pBin " << pBin
            << " tanh(dist) " << tanhdist0 << " itanhdist " << itanhdist
            << " ProbNonMu "
            << (*((*(m_tanhCumulHistoNonMuon[region]))[pBin]))[itanhdist]
            << endmsg;
  return (*((*(m_tanhCumulHistoNonMuon[region]))[pBin]))[itanhdist];
}

//=====================================================================
float DLLMuonTool::calc_ProbMu_tanh(float tanhdist0, unsigned pBin,
                                     unsigned region) const {
  unsigned nDistBins = (*(*m_tanhCumulHistoMuon[region])[pBin]).size();
  unsigned itanhdist = unsigned(tanhdist0 * nDistBins);

  if (itanhdist >= nDistBins) itanhdist = nDistBins - 1;

  if (msgLevel(MSG::DEBUG))
    debug() << "calc_ProbMu_tanh: region " << region << " pBin " << pBin
            << " tanh(dist) " << tanhdist0 << " itanhdist " << itanhdist
            << " ProbMu "
            << (*((*(m_tanhCumulHistoMuon[region]))[pBin]))[itanhdist]
            << endmsg;
  return (*((*(m_tanhCumulHistoMuon[region]))[pBin]))[itanhdist];
}

float DLLMuonTool::calc_ProbNonMu(
    float dist0, const std::vector<float>& parNonMu) const {
  //=====================================================================
  // comment: Returns for a given track of dist0 the probability to be a
  // non-muon;
  //=====================================================================

  float Prob = ROOT::Math::landau_cdf(dist0, parNonMu[1], parNonMu[0]) -
               ROOT::Math::landau_cdf(0, parNonMu[1], parNonMu[0]);
  if (msgLevel(MSG::DEBUG))
    debug() << "ProbNonMu, parNonMu[0], parNonMu[2] = " << Prob << ", "
            << parNonMu[0] << ", " << parNonMu[2] << endmsg;
  if (parNonMu[2] > 0) {
    if (msgLevel(MSG::DEBUG))
      debug() << "probnmu, parNonMu[2] : " << Prob << "," << parNonMu[2]
              << endmsg;
    return Prob = Prob / parNonMu[2];
  } else {
    Warning("ProbNonMu: normalization out of control ", StatusCode::FAILURE)
        .ignore();
    return -1.;
  }
}

StatusCode DLLMuonTool::calcLandauNorm() {
  //=====================================================================
  // comment: Normalizations for Landaus for Muons and Non-Muons
  //=====================================================================

  double parnmu[3];
  for (unsigned i = 1; i != 3; ++i) parnmu[i] = 0;
  float Norm = -1;

  //=============
  //  Non-Muons
  //=============

  if (m_NonMuLanParR1.size() < 3) {
    return Error("NonMuLandauParameterR1 not filled properly");
  }
  for (unsigned i = 0; i != 3; ++i) parnmu[i] = m_NonMuLanParR1[i];
  Norm = calcNorm_nmu(parnmu);
  if (Norm < 0 || Norm == 0)
    return Error("normalization of Non-Muon R1 out of control");
  m_NonMuLanParR1[2] = Norm;

  if (m_NonMuLanParR2.size() < 3) {
    return Error("NonMuLandauParameterR2 not filled properly");
  }
  for (unsigned i = 0; i != 3; ++i) parnmu[i] = m_NonMuLanParR2[i];
  Norm = calcNorm_nmu(parnmu);
  if (Norm < 0 || Norm == 0)
    return Error("normalization of Non-Muon R2 out of control");
  m_NonMuLanParR2[2] = Norm;

  if (m_NonMuLanParR3.size() < 3) {
    return Error("NonMuLandauParameterR3 not filled properly");
  }
  for (unsigned i = 0; i != 3; ++i) parnmu[i] = m_NonMuLanParR3[i];
  Norm = calcNorm_nmu(parnmu);
  if (Norm < 0 || Norm == 0)
    return Error("normalization of Non-Muon R3 out of control");
  m_NonMuLanParR3[2] = Norm;

  if (m_NonMuLanParR4.size() < 3) {
    return Error("NonMuLandauParameterR4 not filled properly");
  }
  for (unsigned i = 0; i != 3; ++i) parnmu[i] = m_NonMuLanParR4[i];
  Norm = calcNorm_nmu(parnmu);
  if (Norm < 0 || Norm == 0)
    return Error("normalization of Non-Muon R4 out of control");
  m_NonMuLanParR4[2] = Norm;

  return StatusCode::SUCCESS;
}

float DLLMuonTool::calcNorm_nmu(double* par) {
  //=====================================================================
  // comment: Calculate Normalizations for non-muons
  //=====================================================================

  //double m_x = 0.2;
  //double m_nMax = 4000;
  float m_x = 800.;
  //float Norm = ROOT::Math::landau_cdf(m_x * m_nMax, par[1], par[0]) -
  float Norm = ROOT::Math::landau_cdf(m_x, par[1], par[0]) - 
               ROOT::Math::landau_cdf(0., par[1], par[0]);

  return Norm;
}

std::tuple<float, float, float> DLLMuonTool::calcMuonLL_tanhdist(
    const LHCb::Track& track,
    const ICommonMuonTool::MuonTrackExtrapolation& extrapolation,
    CommonConstMuonHits& hits,
    std::vector<unsigned>& occupancies) const {
  //=============================================================================
  // comment: Calculate the muon DLL using cumulative histos of the hyperbolic
  //          tangent of the closest distance tuned on runI data for muon and
  //          non-muon
  //          hypothesis per region and momentum bins:
  //=============================================================================

  float p = track.p();

  // Initialize some variables:
  float myDist = -1.;
  float ProbMu = -1.;
  float ProbNonMu = -1.;

  // Determine the region to use for the calculation
  int region = findTrackRegion(extrapolation);

  // Calculate Distance using the closest hit:
  myDist = calc_closestDist(track, extrapolation, hits, occupancies);
  if (msgLevel(MSG::DEBUG))
    debug() << "The value of myDist is = " << myDist << endmsg;

  // Determine the momentum bin for this region
  unsigned pBin = GetPbin(p, region);
  float tanhdistMu, tanhdistNonMu;
  // Calculate tanh(dist). The effetive scale factor is after dividing by arc
  // tanh(0.5)
  if (msgLevel(MSG::DEBUG))
    debug() << "m_tanhScaleFactorsMuon[region] = "
            << (*(m_tanhScaleFactorsMuon[region]))[pBin] << endmsg;
  tanhdistMu =
      tanh(myDist / (*(m_tanhScaleFactorsMuon[region]))[pBin] * ATANH05);
  if (msgLevel(MSG::DEBUG))
    debug() << "The value of Tanhdist Muon is = " << tanhdistMu << endmsg;
  tanhdistNonMu =
      tanh(myDist / (*(m_tanhScaleFactorsNonMuon[region]))[pBin] * ATANH05);
  if (msgLevel(MSG::DEBUG))
    debug() << "The value of Tanhdist NonMuon is = " << tanhdistNonMu << endmsg;

  // Calculate Prob(mu)  for a given track using tanh(dist);
  ProbMu = calc_ProbMu_tanh(tanhdistMu, pBin, region);
  if (msgLevel(MSG::DEBUG))
    debug() << "The value of ProbMu is = " << ProbMu << endmsg;

  // Calculate ProbNonMu using Landau fits
  ProbNonMu = calc_ProbNonMu_tanh(tanhdistNonMu, pBin, region);
  if (msgLevel(MSG::DEBUG))
    debug() << "The value of ProbNonMu is = " << ProbNonMu << endmsg;

  // Using histograms it's not unexpected that some bins are empty. Use very
  // small prob as a protection for the log
  if (ProbMu < 1.e-30) {
    if (msgLevel(MSG::DEBUG))
      debug() << "calcMuonLL_tanhdist: Found null ProbMu: " << ProbMu << endmsg;
    ProbMu = 1.e-30;
  }
  if (ProbNonMu < 1.e-30) {
    if (msgLevel(MSG::DEBUG))
      debug() << "calcMuonLL_tanhdist: Found null ProbNonMu: " << ProbNonMu
              << endmsg;
    ProbNonMu = 1.e-30;
  }

  if (msgLevel(MSG::DEBUG)) {
    debug() << "calcMuonLL_tanhdist: region: " << region << " momentum: " << p
            << " pBin: " << pBin << " dist " << myDist
            << " tanh(dist2) for Mu: " << tanhdistMu
            << " tanh(dist2) for Non Mu: " << tanhdistNonMu
            << " ProbMu: " << ProbMu << " ProbNonMu: " << ProbNonMu
            << " DLL: " << log(ProbMu / ProbNonMu) << endmsg;
  }

  return std::make_tuple(ProbMu, ProbNonMu, myDist);
}

std::tuple<float, float, float> DLLMuonTool::calcMuonLL_tanhdist_landau(
    const LHCb::Track& track,
    const ICommonMuonTool::MuonTrackExtrapolation& extrapolation,
    const CommonConstMuonHits& hits,
    const std::vector<unsigned>& occupancies) const {
  //=============================================================================
  // comment: Calculate the muon DLL using cumulative histos of the hyperbolic
  //          tangent of the closest distance tuned on 2010 data for muon
  //          hypothesis
  //          and previous landau fittings to 2009 MC for non-muon hypothesis,
  //          per region and momentum bins:
  //=============================================================================

  float p = track.p();

  // Initialize some variables:
  float myDist = -1.;
  float ProbMu = -1.;
  float ProbNonMu = -1.;

  // Find the region to use to calculate the MuonLL
  int region = findTrackRegion(extrapolation);

  // Calculate Distance using the closest hit:
  myDist = calc_closestDist(track, extrapolation, hits, occupancies);
  if (msgLevel(MSG::DEBUG))
    debug() << "The value of myDist is = " << myDist << endmsg;

  // Find Landau's parameters for a given track:
  std::vector<float> parNonMu(3, 0);

  // parNonMu = loadNonMuLandauParam(extrapolation);
  if (region == 0)
    parNonMu = {36.5, 23.8, 0.919259};  //# region 1
  else if (region == 1)
    parNonMu = {40.5, 33.3, 0.867921};  //# region 2
  else if (region == 2)
    parNonMu = {22.0, 30.0, 0.800642};  //# region 3
  else if (region == 3)
    parNonMu = {10.0, 17.0, 0.795441};  //# region 4

  // Determine the momentum bin for this region
  unsigned pBin = GetPbin(p, region);
  float tanhdist;
  // Calculate tanh(dist). The effetive scale factor is after dividing by arc
  // tanh(0.5)
  tanhdist = tanh(myDist / (*(m_tanhScaleFactorsMuon[region]))[pBin] * ATANH05);
  if (msgLevel(MSG::DEBUG))
    debug() << "The value of Tanhdist is = " << tanhdist << endmsg;

  // Calculate Prob(mu)  for a given track using tanh(dist);
  ProbMu = calc_ProbMu_tanh(tanhdist, pBin, region);
  if (msgLevel(MSG::DEBUG))
    debug() << "The value of ProbMu is = " << ProbMu << endmsg;

  // Calculate ProbNonMu using Landau fits
  ProbNonMu = calc_ProbNonMu(myDist, parNonMu);
  if (msgLevel(MSG::DEBUG))
    debug() << "The value of ProbNonMu is = " << ProbNonMu << endmsg;

  // Using histograms it's not unexpected that some bins are empty. Use very
  // small prob as a protection for the log
  if (ProbMu < 1.e-30) {
    if (msgLevel(MSG::DEBUG))
      debug() << "calcMuonLL_tanhdist_landau: Found null ProbMu: " << ProbMu
              << endmsg;
    ProbMu = 1.e-30;
  }
  if (ProbNonMu < 1.e-30) {
    if (msgLevel(MSG::DEBUG))
      debug() << "calcMuonLL_tanhdist_landau: Found null ProbNonMu: "
              << ProbNonMu << endmsg;
    ProbNonMu = 1.e-30;
  }

  if (msgLevel(MSG::DEBUG)) {
    debug() << "calcMuonLL_tanhdist_landau: region: " << region
            << " momentum: " << p << " pBin: " << pBin << " dist " << myDist
            << " tanh(dist2): " << tanhdist << " ProbMu: " << ProbMu
            << " ProbNonMu: " << ProbNonMu
            << " DLL: " << log(ProbMu / ProbNonMu) << endmsg;
  }

  return std::make_tuple(ProbMu, ProbNonMu, myDist);
}

/**  Method to compare the Coords of the muonIDs
* modified from the original MuonIDAlg.cpp
*/
bool DLLMuonTool::compareHits(const CommonConstMuonHits& hits1,
                              const CommonConstMuonHits& hits2) const {
  bool theSame = false;
  for_each(
      std::begin(hits1), std::end(hits1),
      [&](const CommonMuonHit* hit1) {  // first hits
        for_each(
            std::begin(hits2), std::end(hits2),
            [&](const CommonMuonHit* hit2) {  // second hits
              if (msgLevel(MSG::DEBUG)) {
                debug() << "compareHits:   The hit1 is = " <<
                    hit1->tile().key() << ". The hit2 is = " <<
                    hit2->tile().key() << endmsg;
              }
              if (hit1->tile().key() == hit2->tile().key()) {
                if (msgLevel(MSG::DEBUG))
                  debug() << "For hits that are the same, the station is = "
                          << hit1->tile().station() << endmsg;
                if (hit1->tile().station() >= iM2) theSame = true;
              }
          });
      });
  return theSame;
}
