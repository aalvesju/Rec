// Include files
#include "InCaloAcceptanceAlg.h"

// ============================================================================
/** @class InBremAcceptanceAlg InBremAcceptanceAlg.cpp
 *  the preconfigured instance of InCaloAcceptanceAlg
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date   2006-06-17
 */
// ============================================================================

struct InBremAcceptanceAlg final : InCaloAcceptanceAlg {
  /// Standard constructor
  InBremAcceptanceAlg(const std::string& name, ISvcLocator* pSvc)
      : InCaloAcceptanceAlg(name, pSvc) {
    using LHCb::CaloAlgUtils::CaloIdLocation;

    Gaudi::Functional::updateHandleLocation(
        *this, "Output", CaloIdLocation("InBrem", context()));

    _setProperty("Tool", "InBremAcceptance/InBrem");
    // track types:
    _setProperty("AcceptedType", Gaudi::Utils::toString<int>(
                                     LHCb::Track::Types::Velo, LHCb::Track::Types::Long,
                                     LHCb::Track::Types::Upstream));
  }
};

// ============================================================================

DECLARE_COMPONENT( InBremAcceptanceAlg )

// ============================================================================
