// Include files
#include <type_traits>
#include "CaloTrackMatchAlg.h"
#include "Event/CaloHypo.h"
#include "Relations/IRelation.h"
#include "Relations/RelationWeighted2D.h"

// =============================================================================
/** @class BremMatchAlg
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date 2006-06-16
 */
// =============================================================================

class BremMatchAlg final : public CaloTrackMatchAlg {
 public:
  StatusCode execute() override;

  BremMatchAlg(const std::string& name, ISvcLocator* pSvc)
      : CaloTrackMatchAlg(name, pSvc) {
    using namespace LHCb::CaloAlgUtils;
    Inputs inputs = Inputs(1, CaloHypoLocation("Photons", context()));
    _setProperty("Calos", Gaudi::Utils::toString(inputs));
    _setProperty("Output", CaloIdLocation("BremMatch", context()));
    _setProperty("Filter", CaloIdLocation("InBrem", context()));
    _setProperty("Tool", "CaloBremMatch/BremMatch");
    setProperty("Threshold", 10000).ignore();
    // track types:
    _setProperty("AcceptedType", Gaudi::Utils::toString<int>(
                                     LHCb::Track::Types::Velo, LHCb::Track::Types::Long,
                                     LHCb::Track::Types::Upstream));
  }
};

// =============================================================================

DECLARE_COMPONENT( BremMatchAlg )

// =============================================================================
// Standard execution of the algorithm
// =============================================================================

StatusCode BremMatchAlg::execute() {
  Assert(!m_tracks.empty(), "No Input tracks");
  Assert(!m_calos.empty(), "No Input Clusters");

  using Table = LHCb::RelationWeighted2D<LHCb::Track, LHCb::CaloHypo, float>;
  static_assert(std::is_base_of<LHCb::Calo2Track::ITrHypoTable2D, Table>::value,
                "Table must inherit from ITrHypoTable2D");

  // create the relation table and register it in TES
  Table* table = new Table(10 * 100);
  put(table, m_output);

  // perform the actual jobs
  StatusCode sc =  doTheJob<LHCb::CaloHypo,Table>( table ) ;
  if(counterStat->isQuiet())counter (Gaudi::Utils::toString( m_tracks.value() )+ "->"
                                  +  Gaudi::Utils::toString( m_calos.value() ) + "=>" + m_output ) += table->i_relations().size();
  return sc;
}
