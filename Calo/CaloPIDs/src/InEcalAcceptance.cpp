// Include files
#include "InCaloAcceptance.h"

// ============================================================================
/** @class InEcalAcceptance
 *  The precofigured instance of InCaloAcceptance Tool
 *  @author Victor EGORYCHEV Victor.Egorychev@cern.ch
 *  @author Vanya  BELYAEV    ibelyaev@phsycis.syr.edu
 *  @date   2006-05-28
 */
// ============================================================================

struct InEcalAcceptance final : InCaloAcceptance {
  /// standard constructor
  InEcalAcceptance(const std::string& type, const std::string& name,
                   const IInterface* parent)
      : InCaloAcceptance(type, name, parent) {
    _setProperty("Calorimeter", DeCalorimeterLocation::Ecal);
    _setProperty("UseFiducial", "true");
    _setProperty("Tolerance", "5");  /// 5 * Gaudi::Units::mm
  };

  /// C++11 non-copyable idiom
  InEcalAcceptance() = delete;
  InEcalAcceptance(const InEcalAcceptance&) = delete;
  InEcalAcceptance& operator=(const InEcalAcceptance&) = delete;
};

// ============================================================================

DECLARE_COMPONENT( InEcalAcceptance )

// ============================================================================
