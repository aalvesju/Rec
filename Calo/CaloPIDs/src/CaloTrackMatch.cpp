// Include files
#include "CaloTrackMatch.h"
#include "GaudiKernel/IIncidentListener.h"
#include "GaudiKernel/IIncidentSvc.h"

// ============================================================================
/** @file
 *  Implementation file for class CaloTrackMatch
 *
 *  Properties of the class:
 *  BadValue - bad value of the chi2,
 *  ConditionName - string with the path to the file containing e- and e+ X-correction condition parameters,
 *  AlphaPOut - track-cluster matching X-correction parameters for Outer zone of the ECAL in case (q*polarity) > 0
 *  AlphaNOut - track-cluster matching X-correction parameters for Outer zone of the ECAL in case (q*polarity) < 0
 *  AlphaPMid - track-cluster matching X-correction parameters for Middle zone of the ECAL in case (q*polarity) > 0
 *  AlphaNMid - track-cluster matching X-correction parameters for Middle zone of the ECAL in case (q*polarity) < 0
 *  AlphaPInn - track-cluster matching X-correction parameters for Inner zone of the ECAL in case (q*polarity) > 0
 *  AlphaNInn - track-cluster matching X-correction parameters for Inner zone of the ECAL in case (q*polarity) < 0
 *
 *  @author Oleg STENYAKIN  oleg.stenyakin@cern.ch
 *  @date   2014-03-03
 *
 */

// ============================================================================
/// standard constructor
// ============================================================================

CaloTrackMatch::CaloTrackMatch(const std::string& type, const std::string& name,
                               const IInterface* parent)
    : Calo::CaloTrackTool(type, name, parent) {
  declareInterface<IIncidentListener>(this);
}

// ============================================================================

StatusCode CaloTrackMatch::i_updateAlpha() {
  // allow a user to disable the CondDB and reset the x-corrections with
  // setProperty on the fly (if ever needed)
  if ( m_conditionName.empty() ){
    if( msgLevel(MSG::DEBUG) )
      debug() << "attempt to update X-correction parameters by UpdMgrSvc while CondDB access disabled" << endmsg;
    return StatusCode::SUCCESS;
  }

  // safety protection against SIGSEGV
  if ( !m_cond ){ fatal() << "CaloTrackMatch::i_updateAlpha m_cond == 0" << endmsg; return StatusCode::FAILURE; }
  try {
    m_alphaPOut.value() = m_cond->paramAsDoubleVect( "alphaPOut" );
    m_alphaNOut.value() = m_cond->paramAsDoubleVect( "alphaNOut" );
    m_alphaPMid.value() = m_cond->paramAsDoubleVect( "alphaPMid" );
    m_alphaNMid.value() = m_cond->paramAsDoubleVect( "alphaNMid" );
    m_alphaPInn.value() = m_cond->paramAsDoubleVect( "alphaPInn" );
    m_alphaNInn.value() = m_cond->paramAsDoubleVect( "alphaNInn" );
  }
  catch ( GaudiException &exc ){
    fatal() << "X-correction update failed! msg ='" << exc << "'" << endmsg;
    return StatusCode::FAILURE;
  }
  if ( msgLevel(MSG::DEBUG) )
    debug() << "CondDB update of CaloTrackMatch X-correction parameters with '" << m_conditionName << "':"
            <<"\nalphaPOut: "<< Gaudi::Utils::toString ( m_alphaPOut.value() )
            <<" alphaNOut: " << Gaudi::Utils::toString ( m_alphaNOut.value() )
            <<"\nalphaPMid: "<< Gaudi::Utils::toString ( m_alphaPMid.value() )
            <<" alphaNMid: " << Gaudi::Utils::toString ( m_alphaNMid.value() )
            <<"\nalphaPInn: "<< Gaudi::Utils::toString ( m_alphaPInn.value() )
            <<" alphaNInn: " << Gaudi::Utils::toString ( m_alphaNInn.value() ) << endmsg;

  return StatusCode::SUCCESS;
}

// =============================================================================

StatusCode CaloTrackMatch::initialize(){
  StatusCode sc = Calo::CaloTrackTool::initialize();
  if ( sc.isFailure() ) { return sc ; }
  IIncidentSvc* isvc = svc<IIncidentSvc>( "IncidentSvc" , true ) ;
  isvc->addListener ( this , IncidentType::BeginEvent ) ;

  if (! existDet<DataObject>(detSvc(), m_conditionName) ){
    if( msgLevel(MSG::DEBUG) ) debug() << "Condition '" << m_conditionName << "' not found -- switch off the use of the CondDB for CaloTrackMatch!" << endmsg;
    m_conditionName = "";
  }

  if ( ! m_conditionName.empty() ){
    registerCondition(m_conditionName, m_cond, &CaloTrackMatch::i_updateAlpha);
    sc = runUpdate();  // ask UpdateManagerSvc to load the condition w/o waiting for the next BeginEvent incident
  }
  else
    if( msgLevel(MSG::DEBUG) ) debug() << "ConditionName empty -- reading of the CaloTrackMatch X-correction parameters from the CondDB has been disabled!" << endmsg;

  m_magFieldSvc = svc<ILHCbMagnetSvc>( "MagneticFieldSvc", true ) ;

  return StatusCode::SUCCESS;
}

// ============================================================================
