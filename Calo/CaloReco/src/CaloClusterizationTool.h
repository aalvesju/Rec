#ifndef CALOCLUSTERIZATIONTOOL_H
#define CALOCLUSRERIZATIONTOOL_H 1
// ============================================================================
#include <string>
#include <iostream>
#include <string>
#include <iostream>
#include "GaudiAlg/GaudiTool.h"
#include "CaloKernel/CaloVector.h"
#include "CaloInterfaces/ICaloClusterization.h"
#include "GaudiKernel/Counters.h"
#include "CaloDet/DeCalorimeter.h"
#include "CelAutoTaggedCell.h"
#include "CaloUtils/CellSelector.h"

/** @class CaloClusterizationTool CaloClusterizationTool.h
 *
 *
 *  @author Victor Egorychev
 *  @date   2008-04-03
 */
class CaloClusterizationTool : public extends<GaudiTool,ICaloClusterization> {

public:
  /// container to tagged  cells with sequential access
  typedef std::vector<CelAutoTaggedCell*> SeqVector;
  /// container to tagged  cells with direct (by CaloCellID key)  access
  typedef CaloVector<CelAutoTaggedCell*>  DirVector ;

  /// Extend ICaloClusterization
  using extends<GaudiTool,ICaloClusterization>::extends;

  unsigned int clusterize
  ( std::vector<LHCb::CaloCluster*>&      clusters   ,
    const LHCb::CaloDigits*               hits       ,
    const DeCalorimeter*                  detector   ,
    const std::vector<LHCb::CaloCellID>&  seeds      ,
    const unsigned int                    level      ) const override
  {
    return _clusterize ( clusters , *hits , detector , seeds , level ) ;
  }

  // ==========================================================================

  unsigned int clusterize
  ( std::vector<LHCb::CaloCluster*>&      clusters   ,
    const CaloVector<LHCb::CaloDigit*>&   hits       ,
    const DeCalorimeter*                  detector   ,
    const std::vector<LHCb::CaloCellID>&  seeds      ,
    const unsigned int                    level      ) const override
  {
    return _clusterize ( clusters , hits , detector , seeds , level ) ;
  }

  // ==========================================================================
  unsigned int clusterize
  ( std::vector<LHCb::CaloCluster*>&      clusters   ,
    const LHCb::CaloDigits*               hits       ,
    const DeCalorimeter*                  detector   ,
    const LHCb::CaloCellID&               seed       ,
    const unsigned int                    level      ) const override
  {
    return clusterize ( clusters , hits , detector ,
                        std::vector<LHCb::CaloCellID>(1,seed) , level ) ;
  };
  // ==========================================================================
  unsigned int clusterize
  ( std::vector<LHCb::CaloCluster*>&      clusters   ,
    const LHCb::CaloDigits*               hits       ,
    const DeCalorimeter*                  detector   ) const override
  {
    const unsigned int level = 0;
    std::vector<LHCb::CaloCellID>  seeds;
    return clusterize ( clusters , hits , detector, seeds, level) ;
  } ;
  StatusCode initialize() override;

protected:

  template<class TYPE> unsigned int _clusterize
  ( std::vector<LHCb::CaloCluster*>&      clusters   ,
    const TYPE&                           data       ,
    const DeCalorimeter*                  detector   ,
    const std::vector<LHCb::CaloCellID>&  seeds      ,
    const unsigned int                    level      ) const ;

  inline bool isLocMax
  ( const LHCb::CaloDigit*     digit ,
    const DirVector&     hits  ,
    const DeCalorimeter* det ) const ;

  inline void appliRulesTagger
  ( CelAutoTaggedCell*   taggedCell,
    DirVector&           taggedCellsDirect,
    const DeCalorimeter* detector,
    const bool           releaseBool) const ;

  inline StatusCode setEXYCluster( LHCb::CaloCluster*         cluster,const DeCalorimeter* detector ) const ;

private:

  Gaudi::Property<bool> m_withET{this, "withET", false};
  Gaudi::Property<double> m_ETcut{this, "ETcut", -10. * Gaudi::Units::GeV};
  Gaudi::Property<std::string> m_usedE{this, "CellSelectorForEnergy", "3x3"};
  Gaudi::Property<std::string> m_usedP{this, "CellSelectorForPosition", "3x3"};
  Gaudi::Property<unsigned int> m_passMax{this, "MaxIteration", 10};

  mutable Gaudi::Accumulators::StatCounter<> m_clusterEnergy{this, "Cluster energy"};
  mutable Gaudi::Accumulators::StatCounter<> m_negativeSeed{this, "Negative seed energy"};
  mutable Gaudi::Accumulators::StatCounter<> m_clusterSize{this, "Cluster size"};
  };
#endif // CALOCLUSTERIZATIONTOOL_H
