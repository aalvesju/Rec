#ifndef CALORECO_CALOSELECTORNOT_H
#define CALORECO_CALOSELECTORNOT_H 1
// Include files
// from STL
#include <string>
// from GaudiAlg
#include "GaudiAlg/GaudiTool.h"
// From CaloInterfaces
#include "CaloInterfaces/ICaloClusterSelector.h"
#include "CaloInterfaces/ICounterLevel.h"
/** @class CaloSelectorNOT CaloSelectorNOT.h
 *
 *  Helper concrete tool for selection of calocluster objects
 *  This selector selects the cluster if
 *  none  of its daughter selector select it!
 *
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
 *  @date   27/04/2002
 */
class CaloSelectorNOT :
  public virtual ICaloClusterSelector ,
  public          GaudiTool
{
public:
  /// container of types&names
  typedef std::vector<std::string>           Names     ;
  /// container of selectors
  typedef std::vector<ICaloClusterSelector*> Selectors ;

public:

  /** "select"/"preselect" method
   *  @see ICaloClusterSelector
   *  @param  cluster pointer to calo cluster object to be selected
   *  @return true if cluster is selected
   */
  bool select
  ( const LHCb::CaloCluster* cluster ) const  override;

  /** "select"/"preselect" method (functor interface)
   *  @see ICaloClusterSelector
   *  @param  cluster pointer to calo cluster object to be selected
   *  @return true if cluster is selected
   */
  bool operator ()
    ( const LHCb::CaloCluster* cluster ) const  override;

  /** stNOTard initialization of the tool
   *  @see IAlgTool
   *  @see AlgTool
   *  @see GaudiTool
   *  @return status code
   */
  StatusCode initialize() override;

  /** stNOTard finalization  of the tool
   *  @see IAlgTool
   *  @see AlgTool
   *  @see GaudiTool
   *  @return status code
   */
  StatusCode finalize() override;

  /** StNOTard constructor
   *  @see GaudiTool
   *  @see  AlgTool
   *  @see IAlgTool
   *  @param type   tool type (?)
   *  @param name   tool name
   *  @param parent tool parent
   */
  CaloSelectorNOT
  ( const std::string& type,
    const std::string& name,
    const IInterface* parent);

private:

  Gaudi::Property<Names> m_selectorsTypeNames {this, "SelectorTools"};
  Selectors m_selectors          ;
  ICounterLevel* counterStat = nullptr;
};
// ============================================================================
#endif // CALORECO_CALOSELECTORNOT_H
