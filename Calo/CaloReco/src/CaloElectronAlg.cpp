// ============================================================================
// Include files
// ============================================================================
// STD & STL
// ============================================================================
#include <algorithm>
// ============================================================================
#include "CaloUtils/CaloDataFunctor.h"
#include "Event/CellID.h"
#include "Event/CaloCluster.h"
#include "Event/CaloHypo.h"
#include "CaloInterfaces/ICaloClusterSelector.h"
#include "CaloInterfaces/ICaloHypoTool.h"
#include "CaloUtils/CaloAlgUtils.h"
#include "CaloUtils/CaloMomentum.h"
#include "CaloElectronAlg.h"
// ============================================================================
/** @file
 *
 *  Implementation file for class : CaloElectronAlg
 *  The implementation is based on F.Machefert's codes.
 *  @see CaloElectronAlg
 *
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
 *  @date 31/03/2002
 */
// ============================================================================
namespace {
    template <typename Container, typename Arg, typename Parent>
    bool apply(const Container& c, Arg& arg, const char* errmsg, const Parent& parent) {
        bool r = std::all_of( std::begin(c), std::end(c),
                            [&](typename Container::const_reference elem)
                            { return (*elem)(&arg).isSuccess(); } );
        if (UNLIKELY(!r)) parent.Error( errmsg, StatusCode::FAILURE ).ignore();
        return r;
    }
}
DECLARE_COMPONENT( CaloElectronAlg )
// ============================================================================
/*  Standard constructor
 *  @param name algorithm name
 *  @param pSvc service locator
 */
// ============================================================================
CaloElectronAlg::CaloElectronAlg
( const std::string& name ,
  ISvcLocator*       pSvc )
  : GaudiAlgorithm ( name , pSvc )
{
  // Default context-dependent locations
  m_inputData = LHCb::CaloAlgUtils::CaloClusterLocation( "Ecal", context() );
  m_outputData= LHCb::CaloAlgUtils::CaloHypoLocation(  "Electrons", context() );
  m_detData   = LHCb::CaloAlgUtils::DeCaloLocation("Ecal");

  setProperty ( "PropertiesPrint" , true ) ;
}
// ============================================================================
/*   standard Algorithm initialization
 *   @return status code
 */
// ============================================================================
StatusCode CaloElectronAlg::initialize()
{
  // initialize  the base class
  StatusCode sc = GaudiAlgorithm::initialize();
  if( sc.isFailure() ){ return Error("Could not initialize the base class GaudiAlgorithm!",sc);}

  // check the geometry information
  m_det = getDet<DeCalorimeter>( m_detData ) ;
  if( !m_det ) { return Error("Detector information is not available!");}

  // locate selector tools
  std::transform(m_selectorsTypeNames.begin(), m_selectorsTypeNames.end(),
                 std::back_inserter( m_selectors ),
                 [&](const std::string& name)
                 { return this->tool<ICaloClusterSelector>( name , this ); } );
  if ( m_selectors.empty() )info()<<  "No Cluster Selection tools are specified!" <<endmsg ;
  // locate correction tools
  std::transform( m_correctionsTypeNames.begin(), m_correctionsTypeNames.end(),
                  std::back_inserter(m_corrections),
                  [&](const std::string& name)
                  { return this->tool<ICaloHypoTool>( name , this ); } );
  if ( m_corrections.empty() )info() << "No Hypo Correction(1) tools are specified!" << endmsg ;
  // locate other hypo  tools
  std::transform( m_hypotoolsTypeNames.begin(), m_hypotoolsTypeNames.end(),
                  std::back_inserter(m_hypotools),
                  [&](const std::string& name)
                  { return this->tool<ICaloHypoTool>( name , this ); } );
  if ( m_hypotools.empty() )info() << "No Hypo Processing(1) tools are specified!"  << endmsg ;
  // locate correction tools
  std::transform( m_correctionsTypeNames2.begin(), m_correctionsTypeNames2.end() ,
                  std::back_inserter(m_corrections2),
                  [&](const std::string& name)
                  { return this->tool<ICaloHypoTool>( name , this ); } );
  if ( m_corrections2.empty() ) info() << "No Hypo Correction(2) tools are specified!"  << endmsg ;
  // locate other hypo  tools
  std::transform( m_hypotoolsTypeNames2.begin(), m_hypotoolsTypeNames2.end(),
                  std::back_inserter(m_hypotools2),
                  [&](const std::string& name)
                  { return this->tool<ICaloHypoTool>( name , this ); } );
  if ( m_hypotools2.empty() )info() << "No Hypo    Processing(2) tools are specified!" << endmsg  ;
  ///
  counterStat = tool<ICounterLevel>("CounterLevel");
  return StatusCode::SUCCESS;
}
// ============================================================================
/*   standard Algorithm finalization
 *   @return status code
 */
// ============================================================================
StatusCode CaloElectronAlg::finalize()
{
  if( UNLIKELY( msgLevel(MSG::DEBUG) ) ) debug()<<"Finalize"<<endmsg;

  // clear containers
  m_selectors             .clear () ;
  m_corrections           .clear () ;
  m_hypotools             .clear () ;
  m_corrections2          .clear () ;
  m_hypotools2            .clear () ;
  m_selectorsTypeNames    .clear () ;
  m_correctionsTypeNames  .clear () ;
  m_hypotoolsTypeNames    .clear () ;
  m_correctionsTypeNames2 .clear () ;
  m_hypotoolsTypeNames2   .clear () ;
  // finalize the base class
  return GaudiAlgorithm::finalize() ;
}
// ============================================================================
/*   standard Algorithm execution
 *   @return status code
 */
// ============================================================================
StatusCode
CaloElectronAlg::execute(){

  // get input clusters
  const auto* clusters = get<LHCb::CaloClusters> ( m_inputData );

  // create and the output container of hypotheses and put in to ETS
  auto*    hypos = new LHCb::CaloHypos() ;
  put ( hypos , m_outputData );

  // loop over clusters
  using namespace  LHCb::CaloDataFunctor;
  LHCb::CaloDataFunctor::EnergyTransverse<const DeCalorimeter*> eT ( m_det ) ;
  for ( auto cluster = clusters->begin() ; clusters->end() != cluster ; ++cluster ){

    // loop over all selectors
    bool select = ( eT(*cluster)>=m_eTcut ) &&
                    std::all_of( m_selectors.begin(), m_selectors.end(),
                                 [&](const auto& selector)
                                 { return (*selector)(*cluster); } );
    // cluster to be selected?
    if ( !select ) { continue ; }

    // create "Hypo"/"Photon" object
    auto hypo = std::make_unique<LHCb::CaloHypo>();
    //
    hypo->setHypothesis( LHCb::CaloHypo::Hypothesis::EmCharged );
    hypo->addToClusters( *cluster );
    hypo->setPosition( std::make_unique<LHCb::CaloPosition>((*cluster)->position()) );

    bool ok = apply( m_corrections, *hypo,"Error from Correction Tool, skip the cluster " , *this ) // loop over all corrections and apply corrections
           && apply( m_hypotools, *hypo, "Error from Other Hypo Tool, skip the cluster " , *this) // loop over other hypo tools (e.g. add extra digits)
           && apply( m_corrections2, *hypo, "Error from Correction Tool 2 , skip the cluster " , *this) // loop over all corrections and apply corrections
           && apply( m_hypotools2, *hypo, "Error from Other Hypo Tool 2 , skip the cluster ", *this); // loop over other hypo tools (e.g. add extra digits)
    if (!ok) continue  ;                                  // CONTINUE

    // set "correct" hypothesis
    hypo->setHypothesis( LHCb::CaloHypo::Hypothesis::EmCharged ); /// final!

    // check momentum after all corrections :
    if( LHCb::CaloMomentum( hypo.get() ).pt() >= m_eTcut )
      hypos->insert ( hypo.release() ) ;  /// add the hypo into container of hypos

  } // end of the loop over all clusters

  if( UNLIKELY( msgLevel(MSG::DEBUG) ) )
    debug () << " # of created Electron Hypos is  " << hypos->size() << endmsg ;

  if(counterStat->isQuiet())counter ( m_inputData + "=>" + m_outputData) += hypos->size() ;

  return StatusCode::SUCCESS;
}
