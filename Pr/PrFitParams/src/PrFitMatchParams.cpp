// Include files

// from Gaudi
#include "GaudiKernel/SystemOfUnits.h"

// from LHCbEvent
#include "Event/MCParticle.h"
#include "Event/MCVertex.h"
#include "Event/MCHit.h"

#include "Event/TrackParameters.h"
#include "Event/State.h"
#include "Event/Track.h"
#include "Event/MCTrackInfo.h"

#include "Linker/LinkedTo.h"
#include "Linker/LinkedFrom.h"


// local
#include "PrFitMatchParams.h"

//-----------------------------------------------------------------------------
// Implementation file for class : PrFitMatchParams
//
// 2017-02-22 : Sevda Esen
//-----------------------------------------------------------------------------

DECLARE_COMPONENT( PrFitMatchParams )

//=============================================================================
// Initialisation. Check parameters
//=============================================================================
StatusCode PrFitMatchParams::initialize() {
  StatusCode sc = GaudiTupleAlg::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm


  m_magFieldSvc = svc<ILHCbMagnetSvc>( "MagneticFieldSvc", true );
  m_magnetScaleFactor = m_magFieldSvc->signedRelativeCurrent();
  debug() << "==> Initialize" << endmsg;

  if( m_zMagParams.empty() ) info() << "no starting values for magnet parameters provided. Will not calculate anything" << endmsg;
  if( m_momParams.empty() ) info() << "no starting values for momentum  parameters provided. Will not calculate anything" << endmsg;
  if( m_bendYParams.empty() ) info() << "no starting values for y bending  parameters provided. Will not calculate anything" << endmsg;


  m_fitTool = tool<IPrFitTool>( "PrFitTool" );

  m_zMagPar.init( "zMagnet"  , m_zMagParams );
  m_momPar.init ( "momentum" , m_momParams );
  m_bendParamY.init( "bendParamY", m_bendYParams);


  return StatusCode::SUCCESS;
}
//=============================================================================
// Main execution
//=============================================================================
StatusCode PrFitMatchParams::execute() {

  debug() << "==> Execute" << endmsg;

  m_nEvent++;


  // -- As this algorithm is the only one running, even the usual counter is not included.
  if( m_nEvent % 100 == 0 ) info() << "Event " << m_nEvent << endmsg;

  debug() << "Processing event: " << m_nEvent << endmsg;



  if(m_resolution) {
    resolution();
    return StatusCode::SUCCESS;
  }


  LHCb::MCParticles* partCtnr = get<LHCb::MCParticles>( LHCb::MCParticleLocation::Default );

  // Get the FT and UT hits
  LHCb::MCHits* ftHits = get<LHCb::MCHits>( LHCb::MCHitLocation::FT );
  LHCb::MCHits* utHits = get<LHCb::MCHits>( LHCb::MCHitLocation::UT );

  // Get the Velo hits
  LHCb::MCHits* veloHits = get<LHCb::MCHits>( LHCb::MCHitLocation::VP );

  LHCb::MCParticles::const_iterator pItr;
  const LHCb::MCParticle* part;
  SmartRefVector<LHCb::MCVertex> vDecay;

  // A container for used hits
  std::vector<Gaudi::XYZPoint> trHits;
  std::vector<Gaudi::XYZPoint> UTHits;
  std::vector<Gaudi::XYZPoint> vHits;

  MCTrackInfo trackInfo = make_MCTrackInfo( evtSvc(), msgSvc() );


  for ( pItr = partCtnr->begin(); partCtnr->end() != pItr; pItr++ ) {
    part = *pItr;

     // only long track reconstructible
    if( !trackInfo.hasVeloAndT( part ) ) continue;

    // Select particles to work with:
    // == Origin vertex position
    const LHCb::MCVertex*  vOrigin = part->originVertex();
    if ( nullptr == vOrigin ) continue;
    if ( m_maxZVertex < vOrigin->position().z() ) continue;
    // == Decay vertices

    // == Momentum cut
    double momentum = part->momentum().R();
    if ( m_minMomentum > momentum ) continue;
    if ( m_maxMomentum < momentum ) continue;
    if ( 11 == abs( part->particleID().pid() ) )  continue;  // no electrons

    bool hasInteractionVertex = false;
    auto endV = part->endVertices();
    for ( const auto itV: endV ) {
      if ( (*itV).position().z() < 9500. ) {
	hasInteractionVertex = true;
	break;
      }
    }
    if ( hasInteractionVertex ) continue;


    debug() << "--- Found particle key " << part->key() << endmsg;

    UTHits.clear();
    trHits.clear();
    vHits.clear();

    // Get the Velo hits
    for ( auto vHitIt = veloHits->begin() ; veloHits->end() != vHitIt ; vHitIt++ ) {
      if ( (*vHitIt)->mcParticle() ==  part ) {
        vHits.push_back( (*vHitIt)->midPoint() );
      }
    }

    // Get the FT hits
    for ( auto iHitFt = ftHits->begin() ; ftHits->end() != iHitFt ; iHitFt++ ) {
      if ( (*iHitFt)->mcParticle() ==  part ) {
        trHits.push_back( (*iHitFt)->midPoint() );
      }
    }

    // Get the UT hits
    for ( auto iHitut = utHits->begin();  utHits->end() != iHitut ; iHitut++ ) {
      if ( (*iHitut)->mcParticle() ==  part ) {
        UTHits.push_back( (*iHitut)->midPoint() );
      }
    }

    if ( 12 > trHits.size() || 6 > vHits.size() )  continue;
    if( m_requireUTHits){
      if( 3 > UTHits.size() ) continue;
    }


    debug() << " particle momentum = " << part->momentum().R() / Gaudi::Units::GeV << " GeV"
            << endmsg;

    //== Fill ntuple
    double pz = part->momentum().Z();
    double plXT = part->momentum().X() / pz;
    double plYT = part->momentum().Y() / pz;

    Tuple tTrack = nTuple( m_tupleName );

    tTrack->column("pz",pz);
    tTrack->column("plXT", plXT);
    tTrack->column("plYT", plYT);


    m_nTrack++;

    //== Fit the UT area
    // -- This is not needed at the moment for the matching, but it was kept it
    // -- as it might be used at some point in the future
    debug() << "  UT: ";
    const auto utFitXLine = m_fitTool->fitLine(UTHits, IPrFitTool::XY::X, m_zUT1);
    const auto utFitY = m_fitTool->fitLine(UTHits, IPrFitTool::XY::Y, m_zUT1);
    const auto utFitXParab = m_fitTool->fitParabola(UTHits, IPrFitTool::XY::X, m_zUT1);
    //m_fitTool->fitLine( UTHits, 1, m_zUT1, ayt, byt, cyt );
    if (!utFitXLine || !utFitY || !utFitXParab)
    {
        err() << "UT fit matrix is singular" << endmsg;
        continue;
    }

    const double axt = std::get<0>(*utFitXLine),
          bxt = std::get<1>(*utFitXLine),
          ayt = std::get<0>(*utFitY),
          byt = std::get<1>(*utFitY),
          axt2 = std::get<0>(*utFitXParab),
          bxt2 = std::get<1>(*utFitXParab),
          cxt2 = std::get<2>(*utFitXParab);

    debug() << format( " x %7.1f tx %7.4f   y %7.1f ty %7.4f ",
                       axt, bxt, ayt, byt )
            << endmsg;;
    tTrack->column( "axt" , axt );
    tTrack->column( "bxt", bxt );
    tTrack->column( "ayt" , ayt );
    tTrack->column( "byt", byt );
    tTrack->column( "axt2", axt2 );
    tTrack->column( "bxt2", bxt2 );
    tTrack->column( "cxt2", cxt2 );


    std::vector<Gaudi::XYZPoint>::const_iterator itP;
    if ( msgLevel( MSG::DEBUG ) ) {
      for ( itP = UTHits.begin(); UTHits.end() > itP; itP++ ) {
        const double dz = (*itP).z()-m_zUT1;

        debug() << format( "    : %7.1f %7.1f %7.1f  dx %7.3f  dy %7.3f",
                           (*itP).x(), (*itP).y(), (*itP).z(),
                           (*itP).x()-(axt+bxt*dz),
                           (*itP).y()-(ayt+byt*dz)
                           ) << endmsg;
      }
    }

    // -- Fit the T-stations
    // -- x(z) = ax + bx*z + cx*z*z + dx*z*z*z
    // -- y(z) = ay + by*z
    const auto tFitX = m_fitTool->fitCubic(trHits, IPrFitTool::XY::X, m_zRef);
    const auto tFitY = m_fitTool->fitLine(trHits, IPrFitTool::XY::Y, m_zRef);
    if (!tFitX || !tFitY)
    {
        err() << "T-stations fit matrix is singular" << endmsg;
        continue;
    }

    const double ax = std::get<0>(*tFitX),
          bx = std::get<1>(*tFitX),
          cx = std::get<2>(*tFitX),
          dx = std::get<3>(*tFitX),
          ay = std::get<0>(*tFitY),
          by = std::get<1>(*tFitY);

    tTrack->column( "ax", ax );
    tTrack->column( "bx", bx );
    tTrack->column( "cx", cx );
    tTrack->column( "dx", dx );
    tTrack->column( "ay", ay );
    tTrack->column( "by", by );

    if ( msgLevel( MSG::DEBUG ) ) {
      debug() << format( "  tr: x%7.1f %7.4f %7.3f %7.3f  y%7.1f %7.4f",
                         ax, bx, 1.e6*cx, 1.e9*dx, ay, by ) << endmsg;

      for ( itP = trHits.begin(); trHits.end() > itP; itP++ ) {
        const double dz = (*itP).z()-m_zRef;
        debug() << format( "    : %7.1f %7.1f %7.1f  dx %7.3f  dy %7.3f",
                           (*itP).x(), (*itP).y(), (*itP).z(),
                           (*itP).x()-(ax + bx*dz + cx*dz*dz + dx*dz*dz*dz),
                           (*itP).y()-(ay + by*dz)
                           ) << endmsg;
      }
    }

    // -- Fit the velo area
    // -- x(z) = axv + bxv*z
    // -- y(z) = ayv + byv*z
    const auto veloFitX = m_fitTool->fitLine(vHits, IPrFitTool::XY::X, m_zVelo);
    const auto veloFitY = m_fitTool->fitLine(vHits, IPrFitTool::XY::Y, m_zVelo);
    if (!veloFitX || !veloFitY)
    {
        err() << "Velo fit matrix is singular" << endmsg;
        continue;
    }

    const double axv = std::get<0>(*veloFitX),
          bxv = std::get<1>(*veloFitX),
          ayv = std::get<0>(*veloFitY),
          byv = std::get<1>(*veloFitY);

    tTrack->column( "axv", axv );
    tTrack->column( "bxv", bxv );
    tTrack->column( "ayv", ayv );
    tTrack->column( "byv", byv );

    if ( msgLevel( MSG::DEBUG ) ) {
      debug() << format( "  velo: x%7.1f %7.4f y%7.1f %7.4f",
                         axv, bxv, ayv, byv ) << endmsg;

      for ( itP = vHits.begin(); vHits.end() > itP; itP++ ) {
        const double dz = (*itP).z()-m_zRef;
        debug() << format( "    : %7.1f %7.1f %7.1f  dx %7.3f  dy %7.3f",
                           (*itP).x(), (*itP).y(), (*itP).z(),
                           (*itP).x()-(ax + bx*dz + cx*dz*dz + dx*dz*dz*dz),
                           (*itP).y()-(ay + by*dz)
                           ) << endmsg;
      }
    }

    // -- This is for finding the zMagnet position when using straight lines from the Velo and the T-stations
    // -- Only really makes sense in x, as for y the different y resolutions of Velo and T-stations matter a lot
    double zMagnet = (axv-bxv*m_zVelo - (ax-bx*m_zRef) ) / (bx-bxv);
    double zMagnety = (ayt-byt*m_zUT1 - (ay-by*m_zRef) ) / (by-byt);
    double dSlope =  fabs(bx - bxv);
    double dSlopeY = fabs(by - byv);

    m_zMagPar.setFun( 0, 1. );
    m_zMagPar.setFun( 1, fabs(dSlope) );
    m_zMagPar.setFun( 2, dSlope*dSlope );
    m_zMagPar.setFun( 3, fabs(ax) );
    m_zMagPar.setFun( 4, bxv*bxv );

    double zEst = m_zMagPar.sum();

    m_zMagPar.addEvent( zMagnet-zEst );

    tTrack->column( "zEst", zEst );
    tTrack->column( "zMagnet", zMagnet );
    tTrack->column( "zMagnety", zMagnety );

    // -- This is the parameter that defines the bending in y
    double bendParamY = (ay - ayv) + ( m_zMatchY - m_zRef )*by - (m_zMatchY - m_zVelo)*byv;

    m_bendParamY.setFun( 0, dSlope*dSlope*byv );
    m_bendParamY.setFun( 1, dSlopeY*dSlopeY*byv );

    double bendParamYEst = m_bendParamY.sum();
    m_bendParamY.addEvent( bendParamY - bendParamYEst );
    // ------------------------------------------------------

    tTrack->column("dSlope",dSlope);
    tTrack->column("dSlope2",dSlope*dSlope);

    // -- Need to write something to calculate the momentum Params
    const double charge = part->particleID().threeCharge()/3;
    const double qOverP = charge/momentum;

    // -- The magnet scale factor is hardcoded, as then one does not need to run the
    // -- field service
    const double proj = sqrt( ( 1. + bxv*bxv + byv*byv ) / ( 1. + bxv*bxv ) );
    const double coef = (bxv - bx)/(proj*m_magnetScaleFactor*Gaudi::Units::GeV*qOverP);

    m_momPar.setFun(0, 1.);
    m_momPar.setFun(1, bx*bx );
    m_momPar.setFun(2, bx*bx*bx*bx );
    m_momPar.setFun(3, bx*bxv );
    m_momPar.setFun(4, byv*byv );
    m_momPar.setFun(5, byv*byv*byv*byv );

    double coefEst = m_momPar.sum();
    m_momPar.addEvent( coef - coefEst );

    tTrack->write();

  }

  return StatusCode::SUCCESS;
}


//=============================================================================
//  Determine resolution
//=============================================================================
void PrFitMatchParams::resolution(){

  LinkedFrom<LHCb::Track, LHCb::MCParticle> myVeloLink ( evtSvc(), msgSvc(),LHCb::TrackLocation::Velo);
  LinkedFrom<LHCb::Track, LHCb::MCParticle> mySeedLink ( evtSvc(), msgSvc(),LHCb::TrackLocation::Seed);

  // Get the IT and TT hits
  LHCb::MCHits* ftHits = get<LHCb::MCHits>( LHCb::MCHitLocation::FT );
  //LHCb::MCHits* utHits = get<LHCb::MCHits>( LHCb::MCHitLocation::UT );
  LHCb::MCHits* vpHits = get<LHCb::MCHits>( LHCb::MCHitLocation::VP );


  // A container for used hits
  std::vector<Gaudi::XYZPoint> FTHits;
  std::vector<Gaudi::XYZPoint> VPHits;

   LHCb::MCParticles* mcParts = getIfExists<LHCb::MCParticles>( LHCb::MCParticleLocation::Default );
   if ( msgLevel( MSG::ERROR ) && !mcParts ) error() << "Could not find MCParticles at: "
						     <<  LHCb::MCParticleLocation::Default
						     << endmsg;

   MCTrackInfo trackInfo = make_MCTrackInfo( evtSvc(), msgSvc() );

   for(const LHCb::MCParticle* mcPart:  *mcParts){


     if( mcPart == nullptr ) continue;
     if( std::abs(mcPart->particleID().pid()) == 11 ) continue;

     if(  !trackInfo.hasVeloAndT( mcPart)  ) continue;

     LHCb::Track* vtrack = myVeloLink.first( mcPart );
     LHCb::Track* ttrack = mySeedLink.first( mcPart );

     if( vtrack == nullptr ) continue;
     if( ttrack == nullptr ) continue;


     const LHCb::MCVertex* vOrigin = mcPart->originVertex();
     if ( nullptr == vOrigin ) continue;
     if ( 100. < vOrigin->position().R() ){
       debug()<< "Too far from beampipe" << endmsg;
       continue;  // particles from close the beam line
     }

     bool hasInteractionVertex = false;
     SmartRefVector<LHCb::MCVertex> endV = mcPart->endVertices();
     for ( SmartRefVector<LHCb::MCVertex>::const_iterator itV = endV.begin() ;
	   endV.end() != itV; itV++ ) {
      if ( (*itV)->position().z() < 9500. ) hasInteractionVertex = true;
    }
    if ( hasInteractionVertex ){
      debug()<< "Interaction vertex found. skipping" << endmsg;
      continue;
    }


    FTHits.clear();
    VPHits.clear();

    // Get the Velo hits
    for ( auto vHitIt = vpHits->begin() ; vpHits->end() != vHitIt ; vHitIt++ ) {
      if ( (*vHitIt)->mcParticle() ==  mcPart ) {
        VPHits.push_back( (*vHitIt)->midPoint() );
      }
    }

    // Get the FT hits
    for ( auto iHitFt = ftHits->begin() ; ftHits->end() != iHitFt ; iHitFt++ ) {
      if ( (*iHitFt)->mcParticle() ==  mcPart ) {
        FTHits.push_back( (*iHitFt)->midPoint() );
      }
    }

    if ( 12 > FTHits.size() || 6 > VPHits.size() )  continue;

    debug()<<"got my hits  " <<  FTHits.size()+VPHits.size()  << endmsg;

    //== Fit the TT area
    debug() << "  VP: ";
    const auto vpFitX = m_fitTool->fitLine(VPHits, IPrFitTool::XY::X, m_zVelo);
    const auto vpFitY = m_fitTool->fitLine(VPHits, IPrFitTool::XY::Y, m_zVelo);
    if (!vpFitX || !vpFitY)
    {
        err() << "VP fit matrix is singular" << endmsg;
        continue;
    }
    const double axv = std::get<0>(*vpFitX),
          bxv = std::get<1>(*vpFitX),
          ayv = std::get<0>(*vpFitY),
          byv = std::get<1>(*vpFitY);

    const auto ftFitX = m_fitTool->fitCubic(FTHits, IPrFitTool::XY::X, m_zRef);
    const auto ftFitY = m_fitTool->fitLine(FTHits, IPrFitTool::XY::Y, m_zRef);
    if (!ftFitX || !ftFitY)
    {
        err() << "FT fit matrix is singular" << endmsg;
        continue;
    }
    const double ax = std::get<0>(*ftFitX),
          bx = std::get<1>(*ftFitX),
          //cx = std::get<2>(*ftFitX),
          //dx = std::get<3>(*ftFitX),
          //ay = std::get<0>(*ftFitY),
          by = std::get<1>(*ftFitY);

    const auto zMagnet = (axv-bxv*m_zVelo - (ax-bx*m_zRef) ) / (bx-bxv);
    const auto xMagnet = axv + ( zMagnet - m_zVelo ) * bxv;
    const auto yMagnet = ayv + ( zMagnet - m_zVelo ) * byv;

    LHCb::State* tstate = &ttrack->closestState( 10000. );
    LHCb::State* vstate = &vtrack->closestState( m_zVelo );
    //now other things
    const auto dSlopeExp       = std::abs( vstate->tx() - tstate->tx() );
    const auto dSlope2Exp      = dSlopeExp*dSlopeExp;
    const auto dSlopeYExp      = std::abs( vstate->ty() - tstate->ty() );
    const auto dSlopeY2Exp     = dSlopeYExp*dSlopeYExp;

    const auto zMagnetExp =m_zMagParams [0] +
      m_zMagParams[1] * dSlopeExp +
      m_zMagParams[2] * dSlope2Exp +
      m_zMagParams[3] * std::abs(tstate->x()) +
      m_zMagParams[4] * vstate->tx()*vstate->tx() ;


    debug()<< m_zMagParams [0] <<"  "<<
      m_zMagParams[1]  <<"  " <<
      m_zMagParams[2]  <<"  " <<
      m_zMagParams[3]  <<"  " <<
      m_zMagParams[4]  << endmsg;


    const float xVExp = vstate->x() + (zMagnetExp     - vstate->z()) * vstate->tx();
    // -- This is the function that calculates the 'bending' in y-direction
    // -- The parametrisation can be derived with the MatchFitParams package
    const float yVExp = vstate->y() + (m_zMatchY - vstate->z()) * vstate->ty() +
      vstate->ty() * ( m_bendYParams[0]*dSlope2Exp + m_bendYParams[1]*dSlopeY2Exp );


    const float xSExp = tstate->x() + (zMagnetExp     - tstate->z()) * tstate->tx();
    const float ySExp = tstate->y() + (m_zMatchY - tstate->z()) * tstate->ty();

    const float distXExp = xSExp - xVExp;
    const float distYExp = ySExp - yVExp;

    const float dslXExp  = vstate->tx() - tstate->tx();
    const float dslYExp  = vstate->ty() - tstate->ty();
    const float teta2 = vstate->ty()*vstate->ty() + tstate->tx()*tstate->tx();

    // -- Check difference in momentum
    const auto p =  mcPart->momentum().R();
    const auto pExp = momentum(vstate, tstate);

    Tuple resoTuple = nTuple( "resoTuple", "resoTuple" );
    resoTuple->column( "zMagnet", zMagnet );
    resoTuple->column( "yMagnet", yMagnet );
    resoTuple->column( "xMagnet", xMagnet );
    resoTuple->column( "zMagnetExp", zMagnetExp );

    resoTuple->column( "yVExp", yVExp );
    resoTuple->column( "ySExp", ySExp );

    resoTuple->column( "distXExp", distXExp );
    resoTuple->column( "distYExp", distYExp );

    resoTuple->column( "xVExp", xVExp );
    resoTuple->column( "xSExp", xSExp );

    resoTuple->column( "dSlopeExp", dSlopeExp );

    resoTuple->column( "pExp", pExp );
    resoTuple->column( "pTrue", p );


    resoTuple->column( "byvExp", vstate->ty() );
    resoTuple->column( "byv", byv );

    resoTuple->column( "bxvExp", vstate->tx() );
    resoTuple->column( "bxv", bxv );


    resoTuple->column( "byExp", tstate->ty() );
    resoTuple->column( "by", by );

    resoTuple->column( "bxExp", tstate->tx());
    resoTuple->column( "bx", bx );

    resoTuple->column( "dslXExp", dslXExp );
    resoTuple->column( "dslYExp", dslYExp );
    resoTuple->column( "teta2", teta2 );

    resoTuple->write();

   }

}

//=============================================================================
//  Finalize
//=============================================================================
StatusCode PrFitMatchParams::finalize() {

  debug() << "==> Finalize" << endmsg;

  MsgStream& msg = info() << "============================================" << endmsg;
  msg << "  Processed " << m_nEvent << " events and " << m_nTrack << " tracks. " << endmsg;
  msg << "============================================" << endmsg;
  m_zMagPar.updateParameters( msg );
  m_momPar.updateParameters(  msg );
  m_bendParamY.updateParameters( msg );

  std::cout << std::endl;
  m_zMagPar.printPythonParams( name() );
  m_momPar.printPythonParams(  name() );
  m_bendParamY.printPythonParams( name() );
  std::cout << std::endl;

  m_zMagPar.printParams( "PrMatch" );
  m_momPar.printParams(  "PrMatch" );
  m_bendParamY.printParams( "PrMatch" );
  std::cout << std::endl;

  return GaudiTupleAlg::finalize();  // must be called after all other actions
}


//=============================================================================
//  Finalize
//=============================================================================
float PrFitMatchParams::momentum(const LHCb::State* vState, const LHCb::State* tState ) {
  const float txT = tState->tx();
  const float txV = vState->tx();
  const float tyV = vState->ty();
  float vars[5] = { txT*txT, txT*txT*txT*txT,
		    txT*txV,
		    tyV*tyV, tyV*tyV*tyV*tyV };


  const float coef = m_momParams[0]* vars[0]
    +m_momParams[1]* vars[1]
    +m_momParams[2]* vars[2]
    +m_momParams[3]* vars[3]
    +m_momParams[4]* vars[4];

  debug()<< m_momParams [0] <<"  "<<
    m_momParams[1]  <<"  " <<
    m_momParams[2]  <<"  " <<
    m_momParams[3]  <<"  " <<
    m_momParams[4]  << endmsg;

  debug()<< vars[0] <<"  "<<
    vars[1]  <<"  " <<
    vars[2]  <<"  " <<
    vars[3]  <<"  " <<
    vars[4]  << endmsg;

  float proj = sqrt( ( 1. + txV*txV + tyV*tyV ) / ( 1. + txV*txV ) );
  float scaleFactor = m_magFieldSvc->signedRelativeCurrent();

  float P = ( coef * Gaudi::Units::GeV * proj *scaleFactor )/(txV-txT);

  return P;

}
