// Include files 

// local
#include "PrFitTool.h"

//-----------------------------------------------------------------------------
// Implementation file for class : PrFitTool
//
// 2006-12-08 : Olivier Callot
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory
DECLARE_COMPONENT( PrFitTool )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
PrFitTool::PrFitTool( const std::string& type,
                  const std::string& name,
                  const IInterface* parent )
: extends ( type, name , parent ),
  m_fit2(2), m_fit3(3), m_fit4(4)
{
  declareInterface<IPrFitTool>(this);
}
//=========================================================================
//  Fit a simple line in the specified projection
//=========================================================================
boost::optional<std::tuple<double, double>>
PrFitTool::fitLine(const std::vector<Gaudi::XYZPoint>& hits, XY mode, double z0) const
{
  m_fit2.clear();
  for (const auto& p: hits)
      m_fit2.accumulate((mode == XY::Y ? p.y() : p.x()), 1., p.z() - z0);
  if (!m_fit2.solve()) return boost::none;
  return std::make_tuple(m_fit2[0], m_fit2[1]);
}
//=========================================================================
//  Fit a parabola in the specified projection
//=========================================================================
boost::optional<std::tuple<double, double, double>>
PrFitTool::fitParabola(const std::vector<Gaudi::XYZPoint>& hits, XY mode, double z0) const
{
  m_fit3.clear();
  for (const auto& p: hits)
      m_fit3.accumulate((mode == XY::Y ? p.y() : p.x()), 1., 1e-3 * (p.z() - z0));
  if (!m_fit3.solve()) return boost::none;
  return std::make_tuple(m_fit3[0], 1e-3 * m_fit3[1], 1e-6 * m_fit3[2]);
}
//=========================================================================
//  Fit a Cubic in the specified projection
//=========================================================================
boost::optional<std::tuple<double, double, double, double>>
PrFitTool::fitCubic(const std::vector<Gaudi::XYZPoint>& hits, XY mode, double z0) const
{
  m_fit4.clear();
  for (const auto& p: hits)
      m_fit4.accumulate((mode == XY::Y ? p.y() : p.x()), 1., 1e-3 * (p.z() - z0));
  if (!m_fit4.solve()) return boost::none;
  return std::make_tuple(m_fit4[0], 1e-3 * m_fit4[1], 1e-6 * m_fit4[2], 1e-9 * m_fit4[3]);
}

//=============================================================================
