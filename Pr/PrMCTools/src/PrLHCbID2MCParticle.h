#ifndef PRLHCBID2MCPARTICLE_H 
#define PRLHCBID2MCPARTICLE_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"
#include "GaudiKernel/AnyDataHandle.h"
// from LinkerEvent
#include "Linker/LinkerWithKey.h"
#include "Linker/LinkedTo.h"

#include "GaudiKernel/ToolHandle.h"
#include "TfKernel/IOTHitCreator.h"


/** @class PrLHCbID2MCParticle PrLHCbID2MCParticle.h
 *  A clone of PatLHCbIDs2MCParticle, to be used with upgraded detectors.
 *
 *  @author Victor Coco (Original PatLHCbIDs2MCParticle by Olivier Callot)
 *  @date   2010-03-22
 */

class PrLHCbID2MCParticle : public GaudiAlgorithm {
public:
  /// Standard constructor
  PrLHCbID2MCParticle( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode execute   () override;    ///< Algorithm execution

private:

  void linkAll( LinkedTo<LHCb::MCParticle>& ilink, LinkerWithKey<LHCb::MCParticle>& olink,
                LHCb::LHCbID id, const std::vector<unsigned int>& ids );  // link all particles to the specified id
   
  std::string m_targetName;
  ToolHandle<Tf::IOTHitCreator>    m_otHitCreator;
  bool m_otReady = false;
  
  AnyDataHandle<FastClusterContainer<LHCb::FTLiteCluster,int>> m_clusters{ LHCb::FTLiteClusterLocation::Default , Gaudi::DataHandle::Reader, this};
};
#endif // PRLHCBID2MCPARTICLE_H
