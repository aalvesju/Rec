// Include files 

// local
#include "PrKernel/UTHit.h"
#include "PrKernel/PrHit.h"
#include "Associators/Associators.h"
#include "PrTrackerDumper.h"
#include <TTree.h>
#include <TFile.h>
#include <TString.h>
//-----------------------------------------------------------------------------
// Implementation file for class : PrTrackerDumper
//
// 2017-11-06 : Renato Quagliani
// Simple tool dumping all hits in the detector information to enable studies. Format is important depending on what you do offline:
// 1 entry = 1 MCParticle in the event. In the tree there is a particular MCParticle with p< 0, for which you get all the hits associated to corresponding to the hits not linked to any MCParticle.
//--- We dump hits associated to MCParticle without any protection against the fact the MCParticle is reconstructible in that detector, this can be checked filtering hits for isLong or isDown or hasSciFi etc..
//--- If you want to get for the current event all the hits in SciFi you will need to read all the entries and go through the vector of hits of the entry in SciFi when nSciFiHits >0 + go through all hits in the netry for p<0 to get 
//--- the non associated hits
//---
// Branches produced regarding the MCParticle information:  
/*
  fullInfo   : has full info MCParticle
  hasSciFi   : reconstructible in SciFi
  hasUT      : reconstructible in UT
  hasVelo    : reconstructible in Velo
  isDown     : reconstructible in UT and SciFi
  isDown_noVelo : reconstructible in UT and SciFi but not in Velo
  isLong        : reconstructible in Velo and SciFi
  isLong_andUT  : reconstructible in Velo and SciFi and UT
  p             : track momentum (cut for p>0 & at least having 1 hit in one of sub-detector) [among MCPartcile there are also the intermediate particles]
  pt            : track trsansverse momentum
  pid           : PID of the particle (can distinguish among muon/electrons/pion/kaons/protons etc...)
  eta           : track pseudorapidity
  ovtx_x        : track origin position X
  ovtx_y        : track origin position Y
  ovtx_z        : track origin position Z
  fromBeautyDecay : the track belongs to a decay chain with a b-quark hadron 
  fromCharmDecay  : the track belongs to a decay chain with a c-quark hadron
  fromStrangeDecay : the track belongs to a decay chain with a s-quark hadron
  DecayOriginMother_pid : it store the PID of the head particle in the decay chain if found , you can filter based on the simulated sample. If for instance you run over Bs->PhiPhi, you can filter the 4 kaons among all tracks requiring Bs PID for this variable
*/ 
/*  VELO related part
  "nVeloHits" : Number of VeloHits associated to the MCParticle
  Velo_x       : vector of x position for Velo  hits (size = nVeloHits) 
  Velo_y       : vector of y position for Velo  hits (size = nVeloHits)   
  Velo_z       : vector of z position for Velo  hits (size = nVeloHits)   
  Velo_Module  : vector of ModuleID Velo  hits (size = nVeloHits)   
  Velo_Sensor  : vector of SensorID Velo  hits (size = nVeloHits)  
  Velo_Station : vector of StationID Velo  hits (size = nVeloHits)  
  Velo_lhcbID  : vector of lhcbID Velo  hits (size = nVeloHits)  
*/
/*  SciFi related part
  nFTHits   : Number of FTHits associated to the MCParticle
  FT_x      : vector of x(y=0) position for SciFi
  FT_z      : vector of z(y=0) position for SciFi hits
  FT_w      : vector of weight error   for SciFi hits
  FT_dxdy   : vector of slopes dxdy for SciFi hits
  FT_YMin   : vector of yMin  for SciFi hits
  FT_YMax   : vector of yMax  for SciFi hits
  FT_hitPlaneCode : vector of planeCode  for SciFi hits
  FT_hitzone      : vector of hitzone (up/down)  for SciFi hits
  FT_lhcbID       : vector of lhcbID  for SciFi hits
*/
/* UT related part
   nUTHits   : Number of UTHits associated to the MCParticle
  //---- see private members of UT:Hit in PrKernel package
  UT_cos  
  UT_cosT
  UT_dxDy
  UT_highthreshold
  UT_lhcbID
  UT_planeCode
  UT_sinT
  UT_size
  UT_tanT
  UT_weight
  UT_xAtYEq0
  UT_xAtYMid
  UT_xMax
  UT_xMin
  UT_xT
  UT_yBegin
  UT_yEnd
  UT_yMax
  UT_yMid
  UT_yMin
  UT_zAtYEq0
*/
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( PrTrackerDumper )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================


PrTrackerDumper::PrTrackerDumper( const std::string& name,
                                        ISvcLocator* pSvcLocator) :
Consumer(name, pSvcLocator, {
    KeyValue{"MCParticlesLocation", LHCb::MCParticleLocation::Default},
    KeyValue{"VPLightClusterLocation", LHCb::VPClusterLocation::Light},
    KeyValue{"FTHitsLocation", PrFTInfo::FTHitsLocation},
    KeyValue{"UTHitsLocation",  UT::Info::HitLocation },
    KeyValue{"ODINLocation", LHCb::ODINLocation::Default},
    KeyValue{"LinkerLocation", Links::location("Pr/LHCbID")} }){
}



void PrTrackerDumper::operator()( 
                                 const LHCb::MCParticles &  MCParticles,
                                 const LHCb::VPLightClusters & VPClusters,
                                 const PrFTHitHandler<PrHit>& prFTHitHandler,
                                 const UT::HitHandler & prUTHitHandler,
                                 const LHCb::ODIN& odin, 
                                 const LHCb::LinksByKey& links) const
{
  

  //Look for associated MC  particle to the hit
  InputLinks<ContainedObject, LHCb::MCParticle> HitMCParticleLinks(links);

  TString filename = "DumperFTUTHits_runNb_"+std::to_string( odin.runNumber()) + "_evtNb_"+std::to_string(odin.eventNumber())+".root";
  TFile * file  = new TFile( filename.Data(),"RECREATE");

  TTree * tree = new TTree("Hits_detectors", "Hits_detectors");
  
  //SciFi 
  std::map< const LHCb::MCParticle* , std::vector< PrHit >  > FTHits_on_MCParticles;
  std::vector<PrHit> non_Assoc_FTHits;
  for ( unsigned int zone = 0; PrFTInfo::nbZones() > zone; ++zone ) {
    for(const auto & hit : prFTHitHandler.hits(zone)){
      //get the LHCbID from the PrHit
      LHCb::LHCbID lhcbid   = hit.id();
      //Get the linking to the MCParticle given the LHCbID
      auto mcparticlesrelations = HitMCParticleLinks.from( lhcbid.lhcbID() );
      if( mcparticlesrelations.empty()){
        non_Assoc_FTHits.push_back( hit);
      }
      for(const auto & mcp : mcparticlesrelations){
        //MCP is MCParticle* 
        auto MCP = mcp.to();
        //---> weightassociation = mcp.weight();
        FTHits_on_MCParticles[MCP].push_back( hit );
      }
    }
  }
  
  //UT detector. loop over all hits in detector, extract for each MCParticle the vector<Hit> , then 
  //See Pr/PrKernel/UTHit definitions to know the info to store 
  std::map< const LHCb::MCParticle* , std::vector< UT::Hit >  > UTHits_on_MCParticles;
  std::vector<UT::Hit> non_Assoc_UTHits;
  for(int iStation = 0; iStation < 2; ++iStation){
    for(int iLayer = 0; iLayer < 2; ++iLayer){
      for( auto & hit : prUTHitHandler.hits( iStation, iLayer )){
        LHCb::LHCbID lhcbid = hit.lhcbID();
        auto mcparticlesrelations = HitMCParticleLinks.from( lhcbid.lhcbID() );
        if( mcparticlesrelations.empty() ){
          non_Assoc_UTHits.push_back( hit) ;
        }else{

          for( const auto & mcp : mcparticlesrelations){
            auto MCP = mcp.to();
            //---> weightassociation = mcp.weight();
            UTHits_on_MCParticles[MCP].push_back( hit );
          }
        }
      }
    }
  }
  //VP Detector 
  std::map< const LHCb::MCParticle*, std::vector<LHCb::VPLightCluster> > VPHits_on_MCParticles;  
  std::vector<LHCb::VPLightCluster> non_Assoc_VPHits;
  std::cout<<"Nb Velo Clusters in TES = "<< VPClusters.size()<<std::endl;
  for( const auto & vpclus : VPClusters){
    LHCb::LHCbID lhcbid = LHCb::LHCbID( vpclus.second.channelID() );
    auto mcparticlesrelations = HitMCParticleLinks.from( lhcbid.lhcbID() );
    if( mcparticlesrelations.empty()){
      non_Assoc_VPHits.push_back(vpclus.second);
    }else{
      for( const auto & mcp : mcparticlesrelations){
        auto MCP = mcp.to();
        VPHits_on_MCParticles[MCP].push_back(vpclus.second);
      }
    }
  }

  
  //---- We use trackInfo for a given MCParticle to know if the particle is reconstructible or not



  MCTrackInfo trackInfo = make_MCTrackInfo( evtSvc(), msgSvc() );
  //Velo
  std::vector<float> Velo_x;
  std::vector<float> Velo_y;
  std::vector<float> Velo_z;
  std::vector<int> Velo_Module;
  std::vector<int> Velo_Sensor;
  std::vector<int> Velo_Station;
  std::vector<unsigned int> Velo_lhcbID;
  int nVeloHits;
  //branches for consistency checks offline when calculating nb hits in various detectors
  int nbHits_in_Velo;
  int nbHits_in_UT;
  int nbHits_in_SciFi;
  
  tree->Branch("nbHits_in_Velo", & nbHits_in_Velo);
  tree->Branch("nbHits_in_UT", & nbHits_in_UT);
  tree->Branch("nbHits_in_SciFi", & nbHits_in_SciFi);
  nbHits_in_Velo = VPClusters.size();
  nbHits_in_UT  =    (int)prUTHitHandler.hits().size();
  nbHits_in_SciFi =  (int)prFTHitHandler.hits().size();
  

  tree->Branch("nVeloHits", & nVeloHits);  
  tree->Branch("Velo_x", & Velo_x);
  tree->Branch("Velo_y", & Velo_y);
  tree->Branch("Velo_z", & Velo_z);
  tree->Branch("Velo_Module", & Velo_Module);
  tree->Branch("Velo_Sensor", & Velo_Sensor);
  tree->Branch("Velo_Station", & Velo_Station);
  tree->Branch("Velo_lhcbID", & Velo_lhcbID);



  //SciFi 
  std::vector< float> FT_hitx;
  std::vector< float> FT_hitz;
  std::vector< float> FT_hitw;
  std::vector< float> FT_hitDXDY;
  std::vector< float> FT_hitYMin;
  std::vector< float> FT_hitYMax;
  std::vector< int> FT_hitPlaneCode;
  std::vector< int> FT_hitzone;
  std::vector<unsigned int> FT_lhcbID;

  int nFTHits;
  tree->Branch("nFTHits", & nFTHits);  
  tree->Branch("FT_x", & FT_hitx);
  tree->Branch("FT_z", & FT_hitz);
  tree->Branch("FT_w", & FT_hitw);
  tree->Branch("FT_dxdy", & FT_hitDXDY );
  tree->Branch("FT_YMin", & FT_hitYMin );
  tree->Branch("FT_YMax", & FT_hitYMax );
  tree->Branch("FT_hitPlaneCode", & FT_hitPlaneCode);
  tree->Branch("FT_hitzone", & FT_hitzone);
  tree->Branch("FT_lhcbID", & FT_lhcbID);


  //UT info
  std::vector<float> UT_cos;
  std::vector<float> UT_cosT;
  std::vector<float> UT_dxDy;
  std::vector<bool>  UT_highthreshold;
  std::vector<unsigned int> UT_lhcbID;
  std::vector<int>   UT_planeCode;
  std::vector<float> UT_sinT;
  std::vector<int> UT_size;
  std::vector<float> UT_tanT;
  std::vector<float> UT_weight;
  std::vector<float> UT_xAtYEq0;
  std::vector<float> UT_xAtYMid;
  std::vector<float> UT_xMax;
  std::vector<float> UT_xMin;
  std::vector<float> UT_xT;
  std::vector<float> UT_yBegin;
  std::vector<float> UT_yEnd;
  std::vector<float> UT_yMax;
  std::vector<float> UT_yMid;
  std::vector<float> UT_yMin;
  std::vector<float> UT_zAtYEq0;
 
  int nUTHits;
  tree->Branch("nUTHits", &nUTHits);
  tree->Branch("UT_cos",& UT_cos);
  tree->Branch("UT_cosT",& UT_cosT);
  tree->Branch("UT_dxDy",& UT_dxDy);
  tree->Branch("UT_highthreshold",& UT_highthreshold);
  tree->Branch("UT_lhcbID",& UT_lhcbID);
  tree->Branch("UT_planeCode",& UT_planeCode);
  tree->Branch("UT_sinT",& UT_sinT);
  tree->Branch("UT_size",& UT_size);
  tree->Branch("UT_tanT",& UT_tanT);
  tree->Branch("UT_weight",& UT_weight);
  tree->Branch("UT_xAtYEq0",& UT_xAtYEq0);
  tree->Branch("UT_xAtYMid",& UT_xAtYMid);
  tree->Branch("UT_xMax",& UT_xMax);
  tree->Branch("UT_xMin",& UT_xMin);
  tree->Branch("UT_xT",& UT_xT);
  tree->Branch("UT_yBegin",& UT_yBegin);
  tree->Branch("UT_yEnd",& UT_yEnd);
  tree->Branch("UT_yMax",& UT_yMax);
  tree->Branch("UT_yMid",& UT_yMid);
  tree->Branch("UT_yMin",& UT_yMin);
  tree->Branch("UT_zAtYEq0",& UT_zAtYEq0);



  bool fullInfo;
  bool hasSciFi;
  bool hasUT;
  bool hasVelo;
  bool isDown;
  bool isDown_noVelo;
  bool isLong;
  bool isLong_andUT;
  double p;
  double pt;
  double eta;
  //vertex origin of the particle
  double ovtx_x;
  double ovtx_y;
  double ovtx_z;
  int  pid;
  bool fromBeautyDecay;
  bool fromCharmDecay;
  bool fromStrangeDecay;
  int  DecayOriginMother_pid;
  int  key;
  
  tree->Branch("fullInfo",      &fullInfo);
  tree->Branch("hasSciFi",      &hasSciFi );
  tree->Branch("hasUT",         &hasUT );  
  tree->Branch("hasVelo",       &hasVelo );
  tree->Branch("isDown",        &isDown );
  tree->Branch("isDown_noVelo", &isDown_noVelo );
  tree->Branch("isLong",        &isLong );
  tree->Branch("key" , & key);
  tree->Branch("isLong_andUT",  &isLong_andUT );
  tree->Branch("p",             &p );
  tree->Branch("pt",            &pt );
  tree->Branch("pid",           &pid );  
  tree->Branch("eta",           &eta );
  tree->Branch("ovtx_x",        &ovtx_x );
  tree->Branch("ovtx_y",        &ovtx_y );
  tree->Branch("ovtx_z",        &ovtx_z );
  tree->Branch("fromBeautyDecay",       &fromBeautyDecay );
  tree->Branch("fromCharmDecay",        &fromCharmDecay );
  tree->Branch("fromStrangeDecay",      &fromStrangeDecay );
  tree->Branch("DecayOriginMother_pid", &DecayOriginMother_pid );


  for( const auto * mcparticle : MCParticles){
    //---- We can speed up things if we filter only tracks which are either reconstructible in Velo or UT or SciFi, 
    //---- Here is very inefficient, we go through ALL MCParticles in the chain, even the non-final states one
    /*
      if( ! ( trackInfo.hasVelo( mcparticle) || trackInfo.hasT( mcparticle) || trackInfo.hasSciFiT( mcparticle) ) ){
        continue;
      };
    */
    //Velo
    nVeloHits = 0;
    Velo_x.clear();
    Velo_y.clear();
    Velo_z.clear();
    Velo_Module.clear();
    Velo_Sensor.clear();
    Velo_Station.clear();
    Velo_lhcbID.clear();

    if( VPHits_on_MCParticles.find( mcparticle) != VPHits_on_MCParticles.end()){
      nVeloHits = (int) VPHits_on_MCParticles[mcparticle].size();
      for( auto & vphit :VPHits_on_MCParticles[mcparticle] ){
        Velo_x.push_back( vphit.x());
        Velo_y.push_back( vphit.y());
        Velo_z.push_back( vphit.z());
        Velo_Module.push_back( vphit.channelID().sensor()/4 );
        Velo_Sensor.push_back( vphit.channelID().sensor() );
        Velo_Station.push_back( vphit.channelID().station());
        Velo_lhcbID.push_back( LHCb::LHCbID(vphit.channelID()).lhcbID() );
      }
    }


    nFTHits = 0;
    //SciFi 
    FT_hitz.clear();
    FT_hitx.clear();
    FT_hitw.clear();
    FT_hitPlaneCode.clear();
    FT_hitzone.clear();
    FT_hitDXDY.clear();
    FT_hitYMin.clear();
    FT_hitYMax.clear();
    FT_lhcbID.clear();

    if( FTHits_on_MCParticles.find(  mcparticle ) != FTHits_on_MCParticles.end() ){
      nFTHits = (int) FTHits_on_MCParticles[mcparticle].size();
      for( auto & fthit : FTHits_on_MCParticles[mcparticle] ){
        FT_hitz.push_back( fthit.z() );
        FT_hitx.push_back( fthit.x() );   
        FT_hitw.push_back(fthit.w());
        FT_hitPlaneCode.push_back(fthit.planeCode());
        FT_hitzone.push_back(fthit.zone());
        FT_hitDXDY.push_back(fthit.dxDy());
        FT_hitYMin.push_back(fthit.yMin());
        FT_hitYMax.push_back(fthit.yMax());
        FT_lhcbID.push_back(fthit.id().lhcbID());
      }
    }


    nUTHits = 0;
    UT_cos.clear();
    UT_cosT.clear();
    UT_dxDy.clear();
    UT_highthreshold.clear();
    UT_lhcbID.clear();
    UT_planeCode.clear();
    UT_sinT.clear();
    UT_size.clear();
    UT_tanT.clear();
    UT_weight.clear();
    UT_xAtYEq0.clear();
    UT_xAtYMid.clear();
    UT_xMax.clear();
    UT_xMin.clear();
    UT_xT.clear();
    UT_yBegin.clear();
    UT_yEnd.clear();
    UT_yMax.clear();
    UT_yMid.clear();
    UT_yMin.clear();
    UT_zAtYEq0.clear();
    if( UTHits_on_MCParticles.find(  mcparticle ) != UTHits_on_MCParticles.end() ){
      nUTHits = (int) UTHits_on_MCParticles[mcparticle].size();
      for(const  auto & uthit : UTHits_on_MCParticles[mcparticle]){
         UT_cos.push_back(uthit.cos()  );
         UT_cosT.push_back(uthit.cosT() );
         UT_dxDy.push_back(uthit.dxDy()  );
         UT_highthreshold.push_back(uthit.highThreshold() );
         UT_lhcbID.push_back(uthit.lhcbID().lhcbID()  );
         UT_planeCode.push_back(uthit.planeCode()  );
         UT_sinT.push_back(uthit.sinT()  );
         UT_size.push_back(uthit.size()  );
         UT_tanT.push_back(uthit.tanT()  );
         UT_weight.push_back(uthit.weight()  );
         UT_xAtYEq0.push_back(uthit.xAtYEq0()  );
         UT_xAtYMid.push_back(uthit.xAtYMid()  );
         UT_xMax.push_back(uthit.xMax() );
         UT_xMin.push_back(uthit.xMin()  );
         UT_xT.push_back(uthit.xT()  );
         UT_yBegin.push_back(uthit.yBegin()   );
         UT_yEnd.push_back(uthit.yEnd()  );
         UT_yMax.push_back(uthit.yMax()  );
         UT_yMid.push_back(uthit.yMid()  );
         UT_yMin.push_back(uthit.yMin() );
         UT_zAtYEq0.push_back(uthit.zAtYEq0()  );
      }
    }
    //skip the MC particles without any hits in the tracking system
    if( nFTHits == 0 && nVeloHits ==0 && nUTHits == 0) continue;
    //probably if fullInfo ==0 skip does the same , to check
    fullInfo= trackInfo.fullInfo(mcparticle);
    hasSciFi      = trackInfo.hasT( mcparticle);
    hasUT         = trackInfo.hasTT( mcparticle);
    hasVelo       = trackInfo.hasVelo( mcparticle);
    isDown        = hasSciFi  && hasUT ;
    isDown_noVelo = hasSciFi && hasUT && !hasVelo;
    isLong       = hasSciFi && hasVelo;
    isLong_andUT = hasSciFi && hasVelo && hasUT;
    p  = mcparticle->p();
    pt = mcparticle->pt();
    eta = mcparticle->momentum().Eta();
    pid = mcparticle->particleID().pid(); //offline you want to match the PID eventually to the e+, e- or whatever
    fromBeautyDecay = false;
    fromCharmDecay = false; 
    fromStrangeDecay = false;
    DecayOriginMother_pid = -999999; 
    ovtx_x = std::numeric_limits<double>::min();
    ovtx_y = std::numeric_limits<double>::min();
    ovtx_z = std::numeric_limits<double>::min();
    key  = mcparticle->key();
    
    //navigate decay back to mother origin
    if( nullptr != mcparticle->originVertex() ){
      //store the mcparticle origin vertex information , and navigate back to mother of the particle!

      ovtx_x = mcparticle->originVertex()->position().x();
      ovtx_y = mcparticle->originVertex()->position().y();
      ovtx_z = mcparticle->originVertex()->position().z();
      const LHCb::MCParticle* mother =  mcparticle->originVertex()->mother();
      if ( nullptr != mother ) {
        if ( nullptr != mother->originVertex() ) {
          double rOrigin = mother->originVertex()->position().rho();
          if ( fabs( rOrigin ) < 5. ) { //radial origin position of the mother within 5 mm from beam pipe
            int pid = abs( mother->particleID().pid() );
            if ( 130 == pid ||    // K0L
                 310 == pid ||    // K0S
                 3122 == pid ||   // Lambda
                 3222 == pid ||   // Sigma+
                 3212 == pid ||   // Sigma0
                 3112 == pid ||   // Sigma-
                 3322 == pid ||   // Xsi0
                 3312 == pid ||   // Xsi-
                 3334 == pid      // Omega-
                 ) {
              fromStrangeDecay = true;
            }
          }
        }
      }
      while( nullptr != mother ) {
        if ( mother->particleID().hasBottom() &&
             ( mother->particleID().isMeson() ||  mother->particleID().isBaryon() ) ){
              DecayOriginMother_pid = mother->particleID().pid();
              fromBeautyDecay = true;
            }
        if ( mother->particleID().hasCharm() &&
             ( mother->particleID().isMeson() ||  mother->particleID().isBaryon() ) ){
              fromCharmDecay = true;
              DecayOriginMother_pid  = mother->particleID().pid();
            }
        mother = mother->originVertex()->mother();
      }
    }
    tree->Fill();
  }


  //We filled the tree with hits having a MCParticle linked to [ no filter done if the particle is reconstructible or not in Velo/UT/SciFi]
  //---- Offline, to grab the hits on reconstructible tracks plot the ones having the flag hasUT or hasT or hasVelo or combine the flags to your preference

  //Empty the vectors of info before fillong the remaining non-associated hits [offline you want to check uniqueness of lhcbID info to have the actual hits to use for tracking, since 
  //1 hit can be associated to more MCParticles]
  FT_hitz.clear();
  FT_hitx.clear();
  FT_hitw.clear();
  FT_hitPlaneCode.clear();
  FT_hitzone.clear();
  FT_hitDXDY.clear();
  FT_hitYMin.clear();
  FT_hitYMax.clear();
  FT_lhcbID.clear();
  //store all remaining hits in a dummy tuple, non associated ones in FT for the event!
  nFTHits = non_Assoc_FTHits.size();
  for(const auto & fthit :non_Assoc_FTHits ){
        FT_hitz.push_back(fthit.z() );
        FT_hitx.push_back(fthit.x() );   
        FT_hitw.push_back(fthit.w());
        FT_hitPlaneCode.push_back(fthit.planeCode());
        FT_hitzone.push_back(fthit.zone());
        FT_hitDXDY.push_back(fthit.dxDy());
        FT_hitYMin.push_back(fthit.yMin());
        FT_hitYMax.push_back(fthit.yMax());
        FT_lhcbID.push_back(fthit.id().lhcbID());
  }
  
  nUTHits = non_Assoc_UTHits.size();
  //Empty the vectors of info before fillong the remaining non-associated hits [offline you want to check uniqueness of lhcbID info to have the actual hits to use for tracking, since 
  //1 hit can be associated to more MCParticles]
  UT_cos.clear();
  UT_cosT.clear();
  UT_dxDy.clear();
  UT_highthreshold.clear();
  UT_lhcbID.clear();
  UT_planeCode.clear();
  UT_sinT.clear();
  UT_size.clear();
  UT_tanT.clear();
  UT_weight.clear();
  UT_xAtYEq0.clear();
  UT_xAtYMid.clear();
  UT_xMax.clear();
  UT_xMin.clear();
  UT_xT.clear();
  UT_yBegin.clear();
  UT_yEnd.clear();
  UT_yMax.clear();
  UT_yMid.clear();
  UT_yMin.clear();
  UT_zAtYEq0.clear();
  for( const auto & uthit :non_Assoc_UTHits ){
    UT_cos.push_back(uthit.cos()  );
    UT_cosT.push_back(uthit.cosT() );
    UT_dxDy.push_back(uthit.dxDy()  );
    UT_highthreshold.push_back(uthit.highThreshold() );
    UT_lhcbID.push_back(uthit.lhcbID().lhcbID()  );
    UT_planeCode.push_back(uthit.planeCode()  );
    UT_sinT.push_back(uthit.sinT()  );
    UT_size.push_back(uthit.size()  );
    UT_tanT.push_back(uthit.tanT()  );
    UT_weight.push_back(uthit.weight()  );
    UT_xAtYEq0.push_back(uthit.xAtYEq0()  );
    UT_xAtYMid.push_back(uthit.xAtYMid()  );
    UT_xMax.push_back(uthit.xMax() );
    UT_xMin.push_back(uthit.xMin()  );
    UT_xT.push_back(uthit.xT()  );
    UT_yBegin.push_back(uthit.yBegin()   );
    UT_yEnd.push_back(uthit.yEnd()  );
    UT_yMax.push_back(uthit.yMax()  );
    UT_yMid.push_back(uthit.yMid()  );
    UT_yMin.push_back(uthit.yMin() );
    UT_zAtYEq0.push_back(uthit.zAtYEq0()  );
  }
  
  //velo part
  Velo_x.clear();
  Velo_y.clear();
  Velo_z.clear();
  Velo_Module.clear();
  Velo_Sensor.clear();
  Velo_Station.clear();
  Velo_lhcbID.clear();
  nVeloHits = non_Assoc_VPHits.size();
  for( const auto & vphit : non_Assoc_VPHits){
    Velo_x.push_back(vphit.x());
    Velo_y.push_back(vphit.y());
    Velo_z.push_back(vphit.z());
    Velo_Module.push_back( vphit.channelID().sensor()/4 );
    Velo_Sensor.push_back( vphit.channelID().sensor() );
    Velo_Station.push_back( vphit.channelID().station());
    Velo_lhcbID.push_back( LHCb::LHCbID(vphit.channelID()).lhcbID() );    
  }
  
  fullInfo= false;
  hasSciFi= false;
  hasUT   = false;
  hasVelo = false;
  isDown  = false;
  isDown_noVelo = false;
  isLong  = false;
  isLong_andUT = false;
  p   = -9999999999999.;
  pt =  -9999999999999.;
  eta = -9999999999999.;
  pid = -999999;
  fromBeautyDecay = false;
  fromCharmDecay = false; 
  fromStrangeDecay = false;
  DecayOriginMother_pid = -999999;
  ovtx_x = -9999999999999.;
  ovtx_y = -9999999999999.;
  ovtx_z = -9999999999999.;
  key    = -999999;
  tree->Fill();
  file->Write();
  file->Close();
}



int PrTrackerDumper::mcVertexType(const LHCb::MCParticle& particle) const{
  const LHCb::MCVertex &vertex = findMCOriginVertex(particle);  
  return vertex.type();
}
const LHCb::MCVertex* PrTrackerDumper::findMCOriginVertex(const LHCb::MCParticle& particle, 
                                                           const double decaylengthtolerance) const{
  
  const LHCb::MCVertex* ov = particle.originVertex();
  if (!ov) return ov;
  const LHCb::MCParticle* mother = ov->mother();
  if (mother && mother != &particle) {
    const LHCb::MCVertex* mov = mother->originVertex();
    if (!mov) return ov;
    const double d = (mov->position() - ov->position()).R();
    if (mov == ov || d < decaylengthtolerance) {
      ov = findMCOriginVertex(*mother, decaylengthtolerance);
    }
  }
  return ov;
}
