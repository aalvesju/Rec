// Include files

// local
#include "PrMatchNN.h"
#include "Event/StateParameters.h"
//-----------------------------------------------------------------------------
// Implementation file for class : PrMatchNN
//
// 2013-11-15 : Michel De Cian, migration to Upgrade
//
// 2007-02-07 : Olivier Callot
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( PrMatchNN )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
PrMatchNN::PrMatchNN( const std::string& name, ISvcLocator* pSvcLocator )
    : Transformer( name, pSvcLocator,
                   {KeyValue{"VeloInput", LHCb::TrackLocation::Velo}, KeyValue{"SeedInput", LHCb::TrackLocation::Seed}},
                   KeyValue{"MatchOutput", LHCb::TrackLocation::Match} )
{
}

//=============================================================================
// Initialization
//=============================================================================
StatusCode PrMatchNN::initialize()
{
  auto sc = Transformer::initialize();
  if ( sc.isFailure() ) return sc; // error printed already by GaudiAlgorithm

  if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) debug() << "==> Initialize" << endmsg;

  if ( m_addUT ) m_addUTHitsTool = tool<IPrAddUTHitsTool>( m_addUTHitsToolName );

  m_fastMomentumTool = tool<ITrackMomentumEstimate>( m_fastMomentumToolName );

  if ( UNLIKELY( m_matchDebugToolName != "" ) )
    m_matchDebugTool = tool<IPrDebugMatchTool>( m_matchDebugToolName, this );
  // Input variable names for TMVA
  // they are only needed to make sure same variables are used in MVA
  // we can remove them once the algorithm is final
  std::array<std::string, 6> inputVars = {"var0", "var1", "var2", "var3", "var4", "var5"};

  m_MLPReader = new ReadMLPMatching( inputVars );

  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
LHCb::Tracks PrMatchNN::operator()( const LHCb::Tracks& velos, const LHCb::Tracks& seeds ) const
{
  LHCb::Tracks matches;
  matches.reserve( 200 );

  if ( velos.empty() ) {
    error() << "Track container '" << inputLocation<0>() << "' is empty" << endmsg;
    return matches;
  }

  if ( seeds.empty() ) {
    error() << "Track container '" << inputLocation<1>() << "' is empty" << endmsg;
    return matches;
  }

  std::vector<MatchCandidate> cands;
  cands.reserve( seeds.size() );

  std::array<float, 6> mLPReaderInput = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0};

  // -- make pairs of Velo track and state
  // -- TrackStatePair is std::pair<const LHCb::Track*, const LHCb::State*>
  // -- TrackStatePairs is std::vector<TrackStatePair>
  // -- typedef in header file
  TrackStatePairs veloPairs;
  veloPairs.reserve( velos.size() );

  for ( const LHCb::Track* vTr : velos ) {
    if ( vTr->checkFlag( LHCb::Track::Flags::Invalid ) ) continue;
    if ( vTr->checkFlag( LHCb::Track::Flags::Backward ) ) continue;
    const LHCb::State& vState = vTr->closestState( 0.0 );
    veloPairs.emplace_back( vTr, &vState );
  }

  // -- sort according to approx y position
  // -- We don't know deltaSlope, so we just extrapolate linearly
  std::sort( veloPairs.begin(), veloPairs.end(), [&]( const TrackStatePair& sP1, const TrackStatePair& sP2 ) {
    const float posA = sP1.second->y() + ( 0.0 - sP1.second->z() ) * sP1.second->ty();
    const float posB = sP2.second->y() + ( 0.0 - sP2.second->z() ) * sP2.second->ty();
    return posA < posB;
  } );

  // -- make pairs of Seed track and state
  TrackStatePairs seedPairs;
  seedPairs.reserve( seeds.size() );

  for ( const LHCb::Track* sTr : seeds ) {
    if ( sTr->checkFlag( LHCb::Track::Flags::Invalid ) ) continue;
    const LHCb::State& sState = sTr->closestState( m_zMatchY );
    seedPairs.emplace_back( sTr, &sState );
  }

  // -- sort according to approx y position
  std::sort( seedPairs.begin(), seedPairs.end(), [&]( const TrackStatePair& sP1, const TrackStatePair& sP2 ) {
    const float posA = sP1.second->y() + ( m_zMatchY - sP1.second->z() ) * sP1.second->ty();
    const float posB = sP2.second->y() + ( m_zMatchY - sP2.second->z() ) * sP2.second->ty();
    return posA < posB;
  } );

  for ( const auto& vP : veloPairs ) {
    cands.clear();

    const float posYApproxV = vP.second->y() + ( m_zMatchY - vP.second->z() ) * vP.second->ty();
    // -- The TrackStatePairs are sorted according to the approximate extrapolated y position
    // -- We can use a binary search to find the starting point from where we need to calculate the chi2
    // -- The tolerance should be large enough such that it is essentially losseless, but speeds things up
    // significantly.
    auto it = std::lower_bound(
        seedPairs.begin(), seedPairs.end(), m_fastYTol, [&]( const TrackStatePair& sP, const float tol ) {
          const float posYApproxS = sP.second->y() + ( m_zMatchY - sP.second->z() ) * sP.second->ty();
          return posYApproxS < posYApproxV - tol;
        } );

    // -- The loop to calculate the chi2 between Velo and Seed track
    for ( ; it < seedPairs.end(); ++it ) {
      TrackStatePair sP = *it;

      // -- Stop the loop at the upper end of the tolerance interval
      const float posYApproxS = sP.second->y() + ( m_zMatchY - sP.second->z() ) * sP.second->ty();
      if ( posYApproxS > posYApproxV + m_fastYTol ) break;

      const float chi2 = getChi2Match( *vP.second, *sP.second, mLPReaderInput );

      if ( UNLIKELY( m_matchDebugToolName != "" ) ) {
        std::vector<float> v( std::begin( mLPReaderInput ), std::end( mLPReaderInput ) );
        m_matchDebugTool->fillTuple( *vP.first, *sP.first, v );
      }

      if ( chi2 < m_maxChi2 ) {
        const float mlp = m_MLPReader->GetMvaValue( mLPReaderInput );
        if ( mlp > m_minNN ) cands.emplace_back( vP.first, sP.first, mlp );
      }
    }

    std::sort( cands.begin(), cands.end(),
               []( const MatchCandidate& lhs, const MatchCandidate& rhs ) { return lhs.dist() > rhs.dist(); } );

    // convert unused match candidates to tracks
    for ( const MatchCandidate& cand : cands ) {

      if ( cands[0].dist() - cand.dist() > m_maxdDist ) break;

      const LHCb::Track* vTr = cand.vTr();
      const LHCb::Track* sTr = cand.sTr();

      if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) {
        debug() << " Candidate"
                << " Velo " << vTr->key() << " Seed " << sTr->key() << " chi2 " << cand.dist() << endmsg;
      }

      LHCb::Track* match = new LHCb::Track();
      matches.insert( match );

      makeTrack( *vTr, *sTr, *match, cand.dist() );

      if ( m_addUT ) {
        StatusCode sc = m_addUTHitsTool->addUTHits( *match );
        if ( sc.isFailure() ) Warning( "adding UT clusters failed!", sc ).ignore();
      }
    } // end loop match cands
  }   // end loop velo tracks

  return matches;
}
//=============================================================================
//

float PrMatchNN::getChi2Match( const LHCb::State& vState, const LHCb::State& sState,
                               std::array<float, 6>& mLPReaderInput ) const
{
  const float tx2 = vState.tx() * vState.tx();
  const float ty2 = vState.ty() * vState.ty();

  const float dSlope = vState.tx() - sState.tx();
  if ( std::abs( dSlope ) > 1.5 ) return 99.;

  const float dSlopeY = vState.ty() - sState.ty();
  if ( std::abs( dSlopeY ) > 0.15 ) return 99.;

  const float zForX = m_zMagParams[0] + m_zMagParams[1] * std::abs( dSlope ) + m_zMagParams[2] * dSlope * dSlope +
                      m_zMagParams[3] * std::abs( sState.x() ) + m_zMagParams[4] * vState.tx() * vState.tx();

  const float dxTol2      = m_dxTol * m_dxTol;
  const float dxTolSlope2 = m_dxTolSlope * m_dxTolSlope;

  const float xV = vState.x() + ( zForX - vState.z() ) * vState.tx();
  // -- This is the function that calculates the 'bending' in y-direction
  // -- The parametrisation can be derived with the MatchFitParams package
  const float yV = vState.y() + ( m_zMatchY - vState.z() ) * vState.ty() +
                   vState.ty() * ( m_bendYParams[0] * dSlope * dSlope + m_bendYParams[1] * dSlopeY * dSlopeY );

  const float xS = sState.x() + ( zForX - sState.z() ) * sState.tx();
  const float yS = sState.y() + ( m_zMatchY - sState.z() ) * sState.ty();

  const float distX = xS - xV;
  if ( std::abs( distX ) > 400 ) return 99.;
  const float distY = yS - yV;
  if ( std::abs( distX ) > 250 ) return 99.;

  const float teta2 = tx2 + ty2;
  const float tolX  = dxTol2 + dSlope * dSlope * dxTolSlope2;
  const float tolY  = m_dyTol * m_dyTol + teta2 * m_dyTolSlope * m_dyTolSlope;

  float chi2 = distX * distX / tolX + distY * distY / tolY;

  // chi2 += dslY * dslY / sState.errTy2() / 16.;
  chi2 += dSlopeY * dSlopeY * 10000 * 0.0625;

  if ( m_maxChi2 < chi2 ) return chi2;

  mLPReaderInput[0] = chi2;
  mLPReaderInput[1] = teta2;
  mLPReaderInput[2] = std::abs( distX );
  mLPReaderInput[3] = std::abs( distY );
  mLPReaderInput[4] = std::abs( dSlope );
  mLPReaderInput[5] = std::abs( dSlopeY );

  return chi2;
}

void PrMatchNN::makeTrack( const LHCb::Track& velo, const LHCb::Track& seed, LHCb::Track& output, float chi2 ) const
{
  //== add ancestors
  output.addToAncestors( velo );
  output.addToAncestors( seed );
  //== Adjust flags
  output.setType( LHCb::Track::Types::Long );
  output.setHistory( LHCb::Track::History::PrMatch );
  output.setPatRecStatus( LHCb::Track::PatRecStatus::PatRecIDs );
  output.addInfo( LHCb::Track::AdditionalInfo::MatchChi2, chi2 );
  //== copy LHCbIDs
  output.addSortedToLhcbIDs( velo.lhcbIDs() );
  output.addSortedToLhcbIDs( seed.lhcbIDs() );
  //== copy Velo and T states at the usual pattern reco positions
  std::vector<LHCb::State*> newstates;
  newstates.reserve( 6 );
  if ( velo.hasStateAt( LHCb::State::ClosestToBeam ) )
    newstates.push_back( velo.stateAt( LHCb::State::ClosestToBeam )->clone() );
  if ( velo.hasStateAt( LHCb::State::FirstMeasurement ) )
    newstates.push_back( velo.stateAt( LHCb::State::FirstMeasurement )->clone() );
  if ( velo.hasStateAt( LHCb::State::EndVelo ) ) newstates.push_back( velo.stateAt( LHCb::State::EndVelo )->clone() );
  newstates.push_back( seed.closestState( StateParameters::ZBegT ).clone() );
  newstates.push_back( seed.closestState( StateParameters::ZMidT ).clone() );
  // make sure we don't include same state twice
  if ( std::abs( newstates[newstates.size() - 2]->z() - newstates.back()->z() ) < 300. ) {
    delete newstates.back();
    newstates.pop_back();
  }
  newstates.push_back( seed.closestState( StateParameters::ZEndT ).clone() );
  // make sure we don't include same state twice
  if ( std::abs( newstates[newstates.size() - 2]->z() - newstates.back()->z() ) < 300. ) {
    delete newstates.back();
    newstates.pop_back();
  }

  //== estimate q/p
  double             qOverP, sigmaQOverP;
  bool const         cubicFit = seed.checkHistory( LHCb::Track::History::PrSeeding );
  const LHCb::State& vState   = velo.closestState( 0. );
  const LHCb::State& sState   = seed.closestState( m_zMatchY );
  StatusCode         sc       = m_fastMomentumTool->calculate( &vState, &sState, qOverP, sigmaQOverP, cubicFit );
  if ( sc.isFailure() ) {
    Warning( "momentum determination failed!", sc ).ignore();
    // assume the Velo/T station standalone reco do something reasonable
  } else {
    // adjust q/p and its uncertainty
    sigmaQOverP = sigmaQOverP * sigmaQOverP;
    for ( LHCb::State* st : newstates ) {
      st->covariance()( 4, 4 ) = sigmaQOverP;
      st->setQOverP( qOverP );
    }
  }
  //== add copied states to output track
  output.addToStates( newstates );
}
