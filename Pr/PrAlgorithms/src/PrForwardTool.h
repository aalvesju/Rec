#ifndef PRFORWARDTOOL_H
#define PRFORWARDTOOL_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/extends.h"
#include "PrKernel/PrFTHitHandler.h"
#include "PrKernel/PrFTZoneHandler.h"
#include "Event/Track.h"
#include "IPrForwardTool.h"
#include "PrGeometryTool.h"
#include "PrPlaneCounter.h"
#include "PrLineFitter.h"

#include "IPrAddUTHitsTool.h"

//NN for ghostprob
#include "weights/TMVA_MLP_Forward1stLoop.h"
#include "weights/TMVA_MLP_Forward2ndLoop.h"

#include "boost/range/iterator_range.hpp"

// Parameters passed around in PrForwardTool
struct PrParameters {
  PrParameters(unsigned int minXHits_, float maxXWindow_,
               float maxXWindowSlope_, float maxXGap_,
               unsigned int minStereoHits_)
    : minXHits{minXHits_}, maxXWindow{maxXWindow_},
    maxXWindowSlope{maxXWindowSlope_}, maxXGap{maxXGap_},
    minStereoHits{minStereoHits_} {}
  const unsigned int minXHits;
  const float        maxXWindow;
  const float        maxXWindowSlope;
  const float        maxXGap;
  unsigned int       minStereoHits;
};

/** @class PrForwardTool PrForwardTool.h
 *  Extend a Velo track to the T stations
 *
 *  @author Olivier Callot
 *  @date   2012-03-20
 *  @author Thomas Nikodem
 *  @date   2016-03-09
 */
class PrForwardTool : public extends<GaudiTool, IPrForwardTool>{
public:
  template<typename T>
  using range_of_ = boost::iterator_range<typename std::vector<T>::iterator>;

  template<typename T>
  using range_of_const_ = boost::iterator_range<typename std::vector<T>::const_iterator>;
  
  /// Standard constructor
  PrForwardTool( const std::string& type,
                 const std::string& name,
                 const IInterface* parent);

  virtual StatusCode initialize() override;
  
  // -- MAIN METHOD
  void extendTrack( const LHCb::Tracks& velo, LHCb::Tracks& result, const PrFTHitHandler<PrHit>& FTHitHandler ) const override;
  
  // ====================================================================================
  // -- DEBUG HELPERS
  // ====================================================================================
  void setDebugParams( IPrDebugTool* tool, const int key, const int veloKey ) override {     
    m_debugTool = tool;     
    m_wantedKey = key;     
    m_veloKey   = veloKey;   
  }   

  bool matchKey( const PrHit& hit ) const override{     
    if ( m_debugTool ) return m_debugTool->matchKey( hit.id(), m_wantedKey );     
    return false;   
  };

  void printHit ( const PrHit& hit, const std::string title = "" ) const override {
    info() << "  " << title
           << format( "Plane%3d zone%2d z0 %8.2f x0 %8.2f",
                      hit.planeCode(), hit.zone(), hit.z(), hit.x() );
    if ( m_debugTool ) m_debugTool->printKey( info(), hit.id() );     
    if ( matchKey( hit ) ) info() << " ***";
    info() << endmsg;
  }

  //=========================================================================
  //  Print the content of a track
  //=========================================================================
  void printTrack( const PrForwardTrack& track ) const {
    if ( 0 < track.nDoF() ){
      info() << format( "   Track nDoF %3d   chi2/nDoF %7.3f quality %6.3f",
                        int(track.getChi2nDof().get(1)), track.chi2PerDoF(), track.quality() )
             << endmsg;
    } else {
      info() << "   Track" << endmsg;
    }
    
    for( const auto& hit : track.hits()){
      info() << format( "dist %7.3f dy %7.2f chi2 %7.2f ", track.distance( *hit ), track.distance( *hit )/hit->dxDy(), track.chi2( *hit ) );
      printHit( *hit );
    }
    
  }
  
  //=========================================================================
  //  Print the hits for a given track
  //=========================================================================
  void printHitsForTrack( const PrHits& hits, const PrForwardTrack& track ) const {
    info() << "=== Hits to Track ===" << endmsg;
    for( const auto& hit : hits){
      info() << format( "dist %7.3f dy %7.2f chi2 %7.2f ", track.distance( *hit ), track.distance( *hit )/hit->dxDy(), track.chi2( *hit ) );
      printHit( *hit );
    }
  }
  






protected:
  
private:
  
  Gaudi::Property<std::string>  m_addUTHitsToolName  {this,  "AddUTHitsToolName", "PrAddUTHitsTool"          };
  Gaudi::Property<float>        m_yTolUVSearch       {this,  "YToleranceUVsearch", 11.* Gaudi::Units::mm     };
  Gaudi::Property<float>        m_tolY               {this,  "TolY",               5.* Gaudi::Units::mm      };
  Gaudi::Property<float>        m_tolYSlope          {this,  "TolYSlope",          0.002 * Gaudi::Units::mm  };
  Gaudi::Property<float>        m_maxChi2LinearFit   {this,  "MaxChi2LinearFit",   100.                      };
  Gaudi::Property<float>        m_maxChi2XProjection {this,  "MaxChi2XProjection", 15.                       };
  Gaudi::Property<float>        m_maxChi2PerDoF      {this,  "MaxChi2PerDoF",      7.                        };

  Gaudi::Property<float>        m_tolYMag            {this,  "TolYMag",            10.* Gaudi::Units::mm     };
  Gaudi::Property<float>        m_tolYMagSlope       {this,  "TolYMagSlope",       0.015                     };
  Gaudi::Property<float>        m_minYGap            {this,  "MinYGap",            0.4 * Gaudi::Units::mm    };

  Gaudi::Property<unsigned int> m_minTotalHits       {this,  "MinTotalHits",       10                        };
  Gaudi::Property<float>        m_maxChi2StereoLinear{this, "MaxChi2StereoLinear", 60.                       };
  Gaudi::Property<float>        m_maxChi2Stereo      {this,  "MaxChi2Stereo",      4.5                       };

  //first loop Hough Cluster search
  Gaudi::Property<unsigned int> m_minXHits           {this, "MinXHits",            5                         };
  Gaudi::Property<float>        m_maxXWindow         {this, "MaxXWindow",          1.2 * Gaudi::Units::mm    };
  Gaudi::Property<float>        m_maxXWindowSlope    {this, "MaxXWindowSlope",     0.002 * Gaudi::Units::mm  };
  Gaudi::Property<float>        m_maxXGap            {this, "MaxXGap",             1.2 * Gaudi::Units::mm    };
  Gaudi::Property<unsigned int> m_minSingleHits      {this, "MinSingleHits",       2                         };
  
  //second loop Hough Cluster search
  Gaudi::Property<bool>         m_secondLoop         {this, "SecondLoop",          true                      };
  Gaudi::Property<unsigned int> m_minXHits2nd        {this, "MinXHits2nd",         4                         };
  Gaudi::Property<float>        m_maxXWindow2nd      {this, "MaxXWindow2nd",       1.5 * Gaudi::Units::mm    };
  Gaudi::Property<float>        m_maxXWindowSlope2nd {this, "MaxXWindowSlope2nd",  0.002 * Gaudi::Units::mm  };
  Gaudi::Property<float>        m_maxXGap2nd         {this, "MaxXGap2nd",          0.5 * Gaudi::Units::mm    };

  //preselection (if done here??!!)
  Gaudi::Property<bool>         m_preselection       {this, "Preselection",        false                     };
  Gaudi::Property<float>        m_preselectionPT     {this, "PreselectionPT",      400.* Gaudi::Units::MeV   };

  //collectX search
  Gaudi::Property<float>        m_minPT              {this, "MinPT",                50 * Gaudi::Units::MeV   };
  //stereo hit matching
  Gaudi::Property<float>        m_tolYCollectX       {this, "TolYCollectX",        4.1* Gaudi::Units::mm     };
  Gaudi::Property<float>        m_tolYSlopeCollectX  {this, "TolYSlopeCollectX",   0.0018 * Gaudi::Units::mm };
  Gaudi::Property<float>        m_tolYTriangleSearch {this, "TolYTriangleSearch",  20.f                      };
  //veloUT momentum estimate
  Gaudi::Property<bool>         m_useMomentumEstimate{this, "UseMomentumEstimate", true                      };
  Gaudi::Property<bool>         m_useWrongSignWindow {this, "UseWrongSignWindow",  true                      };
  Gaudi::Property<float>        m_wrongSignPT        {this, "WrongSignPT",         2000.* Gaudi::Units::MeV  };

  //Track Quality (Neural Net)
  Gaudi::Property<float>        m_maxQuality         {this, "MaxQuality",          0.9                       };
  Gaudi::Property<float>        m_deltaQuality       {this, "DeltaQuality",        0.1                       };

  //make full PrForward Track candidates
  void selectFullCandidates( PrForwardTracks& trackCandidates, const PrFTHitHandler<PrHit>& FTHitHandler, 
                             PrParameters& pars, const float magScaleFactor) const;

  // work wth with X-hits on x projection, side = top or bottom
  template<PrHitZone::Side SIDE>
  void collectAllXHits(std::vector<ModPrHit>& allXHits, const PrForwardTrack& track,
                       const PrFTHitHandler<PrHit>& FTHitHandler,
                       const PrParameters& pars, const float magScaleFactor) const;

  template<PrHitZone::Side SIDE>
  void selectXCandidates(PrForwardTracks& trackCandidates, const PrForwardTrack& seed,
                         std::vector<ModPrHit>& allXHits, const PrFTHitHandler<PrHit>& FTHitHandler,
                         const PrParameters& pars) const;

  void fastLinearFit(PrForwardTrack& track, const PrParameters& pars, PrPlaneCounter& pc) const;

  //also used in the end for "complete" fit
  bool fitXProjection(PrForwardTrack& track, const PrParameters& pars, PrPlaneCounter& pc) const;

  template<PrHitZone::Side SIDE>
  bool addHitsOnEmptyXLayers( PrForwardTrack& track, bool refit,
                              const PrFTHitHandler<PrHit>& FTHitHandler, const PrParameters& pars,
                              PrPlaneCounter& pc) const;

  //work with U/V hits on y projection
  std::vector<ModPrHit> collectStereoHits( PrForwardTrack& track, const PrFTHitHandler<PrHit>& FTHitHandler) const;

  bool selectStereoHits( PrForwardTrack& track , const PrFTHitHandler<PrHit>& FTHitHandler,
                         const std::vector<ModPrHit>& allStereoHits, const PrParameters& pars ) const;

  bool fitYProjection(PrForwardTrack& track, std::vector<const PrHit*>& stereoHits,
                      const PrParameters& pars, PrPlaneCounter& pc) const;

  bool addHitsOnEmptyStereoLayers( PrForwardTrack& track, std::vector<const PrHit*>& stereoHits,
                                   const PrFTHitHandler<PrHit>& FTHitHandler, const PrParameters& pars,
                                   PrPlaneCounter& pc) const;

  //save good tracks
  void makeLHCbTracks( PrForwardTracks& trackCandidates, LHCb::Tracks& result ) const;

  // -- Debug vars
  IPrDebugTool*   m_debugTool = nullptr;
  int             m_wantedKey;
  int             m_veloKey;

  //Tools
  IPrAddUTHitsTool*  m_addUTHitsTool = nullptr;
  PrGeometryTool*    m_geoTool = nullptr;

  // Neural Net The neural net readers are classes generated by TMVA. They are not threadsafe
  // and have been modified by hand. That will have to be done every time the MLP is retuned!
  // For as long as there are the hand tuned networks, no mutex here.
  ReadMLP_Forward1stLoop m_MLPReader_1st;
  ReadMLP_Forward2ndLoop m_MLPReader_2nd;

  /// derived condition caching computed zones
  PrFTZoneHandler* m_zoneHandler = nullptr;

  //helper functions
  void  setHitsUsed(std::vector<ModPrHit>::iterator it,
                    const std::vector<ModPrHit>::iterator& itEnd,
                    const PrForwardTrack& track,
                    const PrParameters& pars) const;

  float zMagnet(const PrForwardTrack& track) const;
  float calcqOverP(const PrForwardTrack& track, const float magScaleFactor) const;
  void  merge6Sorted(std::vector<ModPrHit>& allXHits, const std::array<int, 7>& boundaries) const;
  void  setTrackParameters( PrForwardTrack& track, const float xAtRef) const;
  void  xAtRef_SamePlaneHits(const PrForwardTrack& track,
                             std::vector<ModPrHit>::iterator itH,
                             const std::vector<ModPrHit>::iterator itEnd) const;

  // -- Calculate window size based on minimum PT and slope (= minimum p)
  float calcDxRef(const VeloSeed& seed, const float pt) const {
    return 3973000. * sqrt( seed.slope2 ) / pt - 2200. *  seed.ty2 - 1000. * seed.tx2; // tune this window
  }
  

  //find matching stereo hit if available
  inline bool matchStereoHit(std::vector<PrHit>::const_iterator& itUV1,
                             const std::vector<PrHit>::const_iterator& itUV1end,
                             const float xMinUV, const float xMaxUV) const{
    //search for same side UV hit
    for ( ; itUV1end != itUV1; ++itUV1 ) {
      if( (*itUV1).x() > xMinUV) return (*itUV1).x() < xMaxUV;
    }
    return false;
  }

  //find matching stereo hit if available, also searching in triangle region
  template<PrHitZone::Side SIDE>
  inline bool
  matchStereoHitWithTriangle(std::vector<PrHit>::const_iterator& itUV2,
                             const std::vector<PrHit>::const_iterator& itUV2end,
                             const float yInZone,
                             const float xMinUV, const float xMaxUV) const;
};
#endif // PRFORWARDTOOL_H
