#ifndef PRGECFILTER_H 
#define PRGECFILTER_H 1

// Include files
// from Gaudi
#include "GaudiAlg/FilterPredicate.h"
#include "Event/FTLiteCluster.h"
#include "Event/STLiteCluster.h"
#include "Kernel/FastClusterContainer.h"

/** @class PrGECFilter PrGECFilter.h
 *  \brief give decision concerning GEC
 */
typedef FastClusterContainer <LHCb::FTLiteCluster, int> FTLiteClusters;
typedef FastClusterContainer <LHCb::STLiteCluster, int> UTLiteClusters;

class PrGECFilter : public Gaudi::Functional::FilterPredicate<bool(const FTLiteClusters&, const UTLiteClusters&)> {

 public:
  /// Standard constructor
  PrGECFilter(const std::string& name, ISvcLocator* pSvcLocator);

  /// Algorithm execution
  bool operator()(const FTLiteClusters&, const UTLiteClusters&) const override;

 private:

  Gaudi::Property<int> m_nFTUTClusters{this, "NumberFTUTClusters", -1 };

};
#endif // PRGECFILTER_H

