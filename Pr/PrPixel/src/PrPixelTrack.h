#ifndef PRPIXELTRACK_H
#define PRPIXELTRACK_H 1

// LHCb
#include "Event/StateVector.h"
// Local
#include "PrPixelHit.h"
#include "PrPixelModuleHits.h"
#include <xmmintrin.h>
#include <boost/dynamic_bitset.hpp>
namespace LHCb
{
  class State;
}

/** @class PrPixelTrack PrPixelTrack.h
 *  Working tracks, used inside the PrPixelTracking
 *
 *  @author Olivier Callot
 *  @date   2012-01-06
 */

class PrPixelTrack final
{
public:
  explicit PrPixelTrack( size_t size )
      : m_hitsX( size, 0 ), m_hitsY( size, 0 ), m_hitsZ( size, 0 ), m_tx( 0. ), m_ty( 0. ), m_x0( 0. ), m_y0( 0. )
  {
  }


  /// Return the list of hits on this track.
  const aligned_vector<float>& hitsX() { return m_hitsX; }
  const aligned_vector<float>& hitsY() { return m_hitsY; }
  const aligned_vector<float>& hitsZ() { return m_hitsZ; }
  //aligned_vector<LHCb::LHCbID>& hitsID() { return m_hitsID; }

  /// Chi2 / degrees-of-freedom of straight-line fit
  float chi2() const
  {
    float ch = 0.;
    int nDoF = -4;
    for ( size_t i = 0; i < m_size; ++i ) {
      ch += chi2( m_hitsX[i], m_hitsY[i], m_hitsZ[i] );
      nDoF += 2;
    }
    return ch / nDoF;
  }
  /// Chi2 constribution from a given hit
  float chi2( float x, float y, float z ) const noexcept
  {
    // the errors are currently the same for all hits, otherwise this would have to be handled differently
    float dx = PrPixelModuleHits::wxerr() * ( ( m_x0 + m_tx * z ) - x );
    float dy = PrPixelModuleHits::wyerr() * ( ( m_y0 + m_ty * z ) - y );

    return dx * dx + dy * dy;
  }
  /// Position at given z from straight-line fit
  float xAtZ( const float z ) const { return m_x0 + m_tx * z; }
  float yAtZ( const float z ) const { return m_y0 + m_ty * z; }

  /// Create a track state vector: position and the slopes: dx/dz and dy/dz
  LHCb::StateVector state( const float z ) const
  {
    LHCb::StateVector temp;
    temp.setX( m_x0 + z * m_tx );
    temp.setY( m_y0 + z * m_ty );
    temp.setZ( z );
    temp.setTx( m_tx );
    temp.setTy( m_ty );
    temp.setQOverP( 0. ); // Q/P is Charge/Momentum ratio
    return temp;
  }

  // Calculate the z-pos. where the track passes closest to the beam
  float zBeam() const { return -( m_x0 * m_tx + m_y0 * m_ty ) / ( m_tx * m_tx + m_ty * m_ty ); }

  Gaudi::TrackSymMatrix covariance( const float z ) const;

  /// Perform a straight-line fit.
  void fit();

  void fill( std::vector<PrPixelModuleHits>& modulehits, const std::array<size_t, 71>& hitbuffer )
  {
    m_size = hitbuffer[0]/2;
    size_t cnt = 0;
    for ( size_t i = 1; i < 1+hitbuffer[0]; i+=2 ) {
      PrPixelModuleHits& mod = modulehits[hitbuffer[i]];
      size_t idx = hitbuffer[i+1];
      mod.SetUsed( idx );
      m_hitsX[cnt] = mod.getX(idx);
      m_hitsY[cnt] = mod.getY(idx);
      m_hitsZ[cnt] = mod.getZ(idx);
      ++cnt;
    }
  }
  void fill( PrPixelModuleHits& mod0, PrPixelModuleHits& mod1, PrPixelModuleHits& mod2, const size_t h0, const size_t h1, const size_t h2 )
  {
    m_size     = 3;
    m_hitsX[0] = mod0.getX(h0);
    m_hitsY[0] = mod0.getY(h0);
    m_hitsZ[0] = mod0.getZ(h0);
    m_hitsX[1] = mod1.getX(h1);
    m_hitsY[1] = mod1.getY(h1);
    m_hitsZ[1] = mod1.getZ(h1);
    m_hitsX[2] = mod2.getX(h2);
    m_hitsY[2] = mod2.getY(h2);
    m_hitsZ[2] = mod2.getZ(h2);
  }

  /// Fit with a K-filter with scattering. Return the chi2
  float fitKalman( LHCb::State& state, const int direction, const float noise2PerLayer ) const;

  // Number of hits assigned to the track
  unsigned int size( void ) const { return m_size; }

private:
  /// List of hits
  aligned_vector<float> m_hitsX;
  aligned_vector<float> m_hitsY;
  aligned_vector<float> m_hitsZ;
  /// Straight-line fit parameters
  float m_tx;
  float m_ty;
  float m_x0;
  float m_y0;
  size_t m_size;

  /// Sums for the x-slope fit
  __m128 v_sx, v_sxz, v_s0;
  /// Constants
  static const __m128 v_compValue, v_1s, v_2s, v_sign_mask;
};

typedef std::vector<PrPixelTrack> PrPixelTracks; // vector of tracks

#endif // PRPIXELTRACK_H
