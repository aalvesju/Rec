#ifndef LHCB_CONVERTERS_TRACK_V1_FROMPRVELOUTTRACK_H 
#define LHCB_CONVERTERS_TRACK_V1_FROMPRVELOUTTRACK_H 


// Include files 
// from Gaudi
#include "GaudiAlg/ScalarTransformer.h"
#include "Event/Track.h"
#include "PrKernel/PrVeloUTTrack.h"

/** @class fromPrVeloUTTrack fromPrVeloUTTrack.h
 *  
 *  Small helper to convert std::vector of custom class for output of PrVeloUT
 *  to LHCb::Tracks.
 *
 *  @author Michel De Cian
 *  @date   2018-03-09
 */

namespace LHCb{
  namespace Converters{
    namespace Track{
      namespace v1{
        
        struct fromPrVeloUTTrack : public Gaudi::Functional::ScalarTransformer<fromPrVeloUTTrack, LHCb::Tracks(const PrVeloUTTracks&)>{
          
          fromPrVeloUTTrack( const std::string& name, ISvcLocator* pSvcLocator ):    
            ScalarTransformer(name, pSvcLocator,
                              KeyValue{"InputTracksName", LHCb::TrackLocation::VeloTT} ,
                              KeyValue{"OutputTracksName", "Rec/Track/VeloTTLHCbTracks"}){}
          
          
          using ScalarTransformer::operator();
          
          /// The main function, converts the track
          LHCb::Track* operator()(const PrVeloUTTrack& track) const {

            auto outTr = std::make_unique<LHCb::Track>(*(track.veloTr));
            // set q/p in all of the existing states
            for(auto& state : outTr->states()) state->setQOverP(track.qOverP);
            
            // -- As we don't need the state in the UT, it is not added in PrVeloUT
            // -- and can't be added here.
            outTr->addToLhcbIDs( track.UTIDs );
            outTr->setType( LHCb::Track::Upstream );
            outTr->setHistory( LHCb::Track::PrVeloUT );
            outTr->addToAncestors( track.veloTr );
            outTr->setPatRecStatus( LHCb::Track::PatRecIDs );
            
            return outTr.release();
          }
          
        };
        DECLARE_COMPONENT(fromPrVeloUTTrack)
      }
    }
  }
}





#endif // LHCB_CONVERTERS_TRACK_V1_FROMPRVELOUTTRACK_H  
