#ifndef UTTRACKMONITOR_H
#define UTTRACKMONITOR_H 1

// Include files

// from Gaudi
#include "TrackMonitorBase.h"
#include "Event/STMeasurement.h"

namespace LHCb {
  class Track ;
  class LHCbID ;
}

/** @class UTTrackMonitor UTTrackMonitor.h "TrackCheckers/UTTrackMonitor"
 *
 * Class for UT track monitoring
 *  @author M. Needham.
 *  @date   16-1-2009
 */

class UTTrackMonitor : public TrackMonitorBase {

 public:

  /** Standard construtor */
  UTTrackMonitor( const std::string& name, ISvcLocator* pSvcLocator );

  /** Algorithm execute */
  StatusCode execute() override;

 private:


  void fillHistograms(const LHCb::Track& track,
                      const std::vector<LHCb::LHCbID>& itIDs,
                      const std::string& type) const ;

  Gaudi::Property<double> m_refZ { this, "ReferenceZ", 2500.0, "midpoint of UT" };
  double m_xMax;
  double m_yMax;

  Gaudi::Property<unsigned int> m_minNumUTHits { this, "minNumUTHits", 2u };
  Gaudi::Property<std::string> m_clusterLocation{ this, "InputData" , LHCb::STClusterLocation::UTClusters };

};


#endif // UTTRACKMONITOR_H
