#ifndef PATPVOFFLINE_H
#define PATPVOFFLINE_H 1
// Include files:
// from Gaudi
#include "GaudiAlg/Transformer.h"
// Interfaces
#include "TrackInterfaces/IPVOfflineTool.h"
// Local
#include "Event/Track.h"
#include "Event/RecVertex.h"

/** @class PatPVOffline PatPVOffline.h
 *
 *
 *  @author Mariusz Witek
 *  @date   2010-10-05
 */
class PatPVOffline : public Gaudi::Functional::Transformer<LHCb::RecVertices(const LHCb::Tracks&)> {
public:
  // Standard constructor
  PatPVOffline(const std::string& name, ISvcLocator* pSvcLocator);

  /// Algorithm execution
  LHCb::RecVertices operator()(const LHCb::Tracks&) const override;

private:
  // Tools
  ToolHandle<IPVOfflineTool> m_pvsfit {"PVOfflineTool",this};
};
#endif // PATPVOFFLINE_H
