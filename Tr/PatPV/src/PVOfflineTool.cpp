// Include files:
#include <numeric>
// from Gaudi
#include "GaudiKernel/SystemOfUnits.h"
// from Event
#include "Event/Track.h"
#include "Event/State.h"
#include "Event/RecVertex.h"
#include "Kernel/STLExtensions.h"
#include "DetDesc/Condition.h"
#include "GaudiKernel/IUpdateManagerSvc.h"
// Local
#include "PVOfflineTool.h"

DECLARE_COMPONENT( PVOfflineTool )

namespace {

static const std::string s_beamSpotCond = "/dd/Conditions/Online/Velo/MotionSystem";

template <typename Iterator, typename Projection, typename Threshold>
std::pair<Iterator,Threshold> max_above_threshold( Iterator begin, Iterator end, Projection proj, Threshold mx ) {
    auto r = std::make_pair( end, std::move(mx) );
    for ( ; begin != end; ++begin ) {
        auto val = Gaudi::invoke( proj, *begin );
        if ( val > r.second ) {
            r.second = std::move(val);
            r.first = begin;
        }
    }
    return r;
}

template <typename Container, typename ValueType, typename Projection>
ValueType min_value(const Container& c, ValueType init, Projection proj) {
    using std::begin; using std::end;
    return std::accumulate( begin(c), end(c), init,
                             [&]( ValueType v, const auto& r) {
             return std::min(v,Gaudi::invoke(proj,r));
    });
}

class velo_overlap_with {
    std::vector<LHCb::LHCbID> m_ref;
public:
    velo_overlap_with(LHCb::span<const LHCb::LHCbID> ids) {
        m_ref.reserve(ids.size());
        std::copy_if( ids.begin(), ids.end(),
                      std::back_inserter(m_ref),
                      [](const LHCb::LHCbID& id) { return id.isVelo(); });
    }
    velo_overlap_with(const LHCb::Track& trk) : velo_overlap_with( trk.lhcbIDs() ) {
    }

    std::pair<int,int> operator()(LHCb::span<const LHCb::LHCbID> ids) const {
      auto first1 = m_ref.begin() ;
      auto last1  = m_ref.end() ;
      auto first2 = ids.begin() ;
      auto last2  = ids.end() ;
      auto n = std::make_pair(0,0);
      while (first1 != last1 && first2 != last2) {
        if ( *first1 < *first2 ) {
          ++first1;
        } else if ( *first2 < *first1 ) {
          if (first2->isVelo()) ++n.second;
          ++first2;
        } else {
          ++n.first;
          ++n.second;
          ++first1;
          ++first2;
        }
      }
      return n;
    }
    std::pair<int,int> operator()(const LHCb::Track& trk) const
    { return (*this)(trk.lhcbIDs()); }
};
}



double zCloseBeam(const LHCb::Track& track) {
  Gaudi::XYZVector unitVect = track.firstState().slopes().Unit();
  const LHCb::State& stateG = track.firstState();
  return stateG.z() - unitVect.z() *
    (unitVect.x() * stateG.x() + unitVect.y() * stateG.y()) /
    (1.0 - std::pow(unitVect.z(),2));
}
//=============================================================================
// Store dummy vertices
//=============================================================================
std::vector<LHCb::RecVertex>
storeDummyVertices(const std::vector<Gaudi::XYZPoint>& seeds,
                   const std::vector<const LHCb::Track*>& rtracks)
{
  std::vector<LHCb::RecVertex> out; out.reserve(seeds.size());
  for (const auto& seed : seeds) {
    out.emplace_back();
    auto& tVertex = out.back();
    tVertex.setPosition( seed );
    Gaudi::SymMatrix3x3 errMat;
    for(int i = 0; i < 3; i++) {
      for(int j = 0; j < 3; j++) {
        errMat(i,j) = 1.0;
      }
    }
    tVertex.setCovMatrix(errMat);
    tVertex.setNDoF(1);
    tVertex.setChi2(99999.0);
    // Fill close tracks
    auto is_close = [z=seed.Z()](const auto* trk) {
        return std::abs(zCloseBeam(*trk)-z) < 3.0 * Gaudi::Units::mm;
    };
    for(const auto& trk : rtracks) {
      if (is_close(trk)) tVertex.addToTracks(trk);
    }
  }
  return out;
}




//=========================================================================
// Initialize
//=========================================================================
StatusCode PVOfflineTool::initialize()
{
  StatusCode sc = extends::initialize();
  if (!sc) return sc;

  if (m_useBeamSpotRCut.value() ) {
    IUpdateManagerSvc* m_updMgrSvc = svc<IUpdateManagerSvc>("UpdateManagerSvc", true);
    m_updMgrSvc->registerCondition(this, s_beamSpotCond, &PVOfflineTool::UpdateBeamSpot);
    StatusCode scu = m_updMgrSvc->update(this);
    if(!scu.isSuccess())
      return Error("Failed to update conditions!",scu);
  }

  if ( m_doTiming) {
    m_timerTool.retrieve();
    m_timer[static_cast<int>(timers_t::Total)] = m_timerTool->addTimer( "PatPV total" );
    m_timerTool->increaseIndent();
    m_timer[static_cast<int>(timers_t::Seeding)] = m_timerTool->addTimer( "PatPV seeding" );
    m_timer[static_cast<int>(timers_t::Fitting)] = m_timerTool->addTimer( "PatPV fitting" );
    m_timerTool->decreaseIndent();
  } else {
    m_timerTool.disable();
  }

  return StatusCode::SUCCESS;
}


//=============================================================================
// reconstruct PV for a given seed
//=============================================================================
StatusCode PVOfflineTool::reconstructSinglePV(const LHCb::Tracks& inputTracks,
                                              const Gaudi::XYZPoint xyzseed,
                                              LHCb::RecVertex& outvtx) const
{
  std::vector<const LHCb::Track*> tracks2remove;
  return m_pvfit->fitVertex( xyzseed, readTracks(inputTracks), outvtx, tracks2remove );
}
//=============================================================================
// reconstruct PV for a given seed using tracks from the list
//=============================================================================
StatusCode PVOfflineTool::reconstructSinglePVFromTracks(const Gaudi::XYZPoint xyzseed,
                                                        const std::vector<const LHCb::Track*>& tracks2use,
                                                        LHCb::RecVertex& outvtx) const
{
  std::vector<const LHCb::Track*> tracks2remove;
  return m_pvfit->fitVertex( xyzseed, tracks2use, outvtx, tracks2remove );
}

//=============================================================================
// reconstruct PV for a given seed with a list of tracks to be excluded
//=============================================================================
StatusCode PVOfflineTool::reDoSinglePV(const LHCb::Tracks& inputTracks,
                                       const Gaudi::XYZPoint xyzseed,
                                       std::vector<const LHCb::Track*>& tracks2exclude,
                                       LHCb::RecVertex& outvtx) const
{
  auto rtracks = readTracks(inputTracks);
  if(!tracks2exclude.empty()) removeTracksByLHCbIDs(rtracks, tracks2exclude);
  std::vector<const LHCb::Track*> tracks2remove;
  return m_pvfit->fitVertex( xyzseed, rtracks, outvtx, tracks2remove );
}


//=============================================================================
// multi vtx search and fit. Return new vtx after track removal
//=============================================================================
StatusCode PVOfflineTool::reDoMultiPV(const LHCb::Tracks& inputTracks,
                                      const LHCb::RecVertex& invtx,
                                      std::vector<const LHCb::Track*>& tracks2exclude,
                                      LHCb::RecVertex& outvtx) const
{
  auto rtracks = readTracks(inputTracks);

  if(!tracks2exclude.empty()) removeTracksByLHCbIDs(rtracks, tracks2exclude);

  std::vector<LHCb::RecVertex> outvtxVec;
  StatusCode scvfit = reconstructMultiPVFromTracks(rtracks,outvtxVec);

  // check which vtx corresponds to input vtx
  if (scvfit.isFailure()) return scvfit;
  auto pvtx = matchVtxByTracks(invtx, outvtxVec);
  if (!pvtx) return StatusCode::FAILURE;
  outvtx = *pvtx;
  return StatusCode::SUCCESS;
}

//=============================================================================
// multi vtx search and fit with tracks from default location
//=============================================================================
StatusCode PVOfflineTool::reconstructMultiPV(const LHCb::Tracks& inputTracks,
                                             std::vector<LHCb::RecVertex>& outvtxvec) const
{
  return reconstructMultiPVFromTracks(readTracks(inputTracks), outvtxvec);
}


//=============================================================================
// multi vtx search and fit with tracks specified
//=============================================================================
StatusCode PVOfflineTool::reconstructMultiPVFromTracks(const std::vector<const LHCb::Track*>& tracks2use,
                                                       std::vector<LHCb::RecVertex>& outvtxvec) const
{
  auto totaltime_guard = make_timeguard( timers_t::Total );

  auto rtracks = tracks2use;

  outvtxvec.clear();

  if (m_saveSeedsAsPV) {
    auto seeds = m_pvSeedTool->getSeeds(rtracks, {m_beamSpotX, m_beamSpotY, 0.0 });
    outvtxvec = storeDummyVertices(seeds, rtracks);
    return StatusCode::SUCCESS;
  }

  if (!m_pvfit) throw GaudiException( "no PV fitting tool", "PVOffLineTool", StatusCode::FAILURE );

  int nvtx_before = -1;
  int nvtx_after  =  0;
  while ( nvtx_after > nvtx_before ) {
    nvtx_before = outvtxvec.size();
    // reconstruct vertices

    auto seeds = [&](){ auto seedingtime_guard = make_timeguard( timers_t::Seeding );
      // seeding
      return m_pvSeedTool->getSeeds(rtracks, {m_beamSpotX, m_beamSpotY, 0.0} );
    }();


    if(msgLevel(MSG::DEBUG)) {
      debug() << " seeds  " << seeds.size()
              << " pos: " << seeds << endmsg;
    }
    for (const auto& seed : seeds) {
      LHCb::RecVertex recvtx;
      if(msgLevel(MSG::VERBOSE)) verbose() << "ready to fit" << endmsg;

      std::vector<const LHCb::Track*> tracks2remove;
      { auto fittime_guard = make_timeguard( timers_t::Fitting );
      // fitting
      StatusCode scvfit = m_pvfit->fitVertex( seed, rtracks, recvtx, tracks2remove );
      if (!scvfit.isSuccess()) continue;
      }

      if (!separatedVertex(recvtx,outvtxvec)) continue;
      if ( m_useBeamSpotRCut.value() && m_veloClosed ) {
        const auto& pos = recvtx.position();
        auto r2 = std::pow(pos.x()-m_beamSpotX,2) + std::pow( pos.y()-m_beamSpotY,2);
        auto r  = (  recvtx.tracks().size() <  m_beamSpotRMT.value() ? m_beamSpotRCut.value() :m_beamSpotRCutHMC.value() );
        if ( r2 > r*r ) continue;
      }
      outvtxvec.push_back(recvtx);
      removeTracks(rtracks, tracks2remove);
    }//iterate on seeds
    nvtx_after = outvtxvec.size();
  }//iterate on vtx

  return StatusCode::SUCCESS;

}

bool PVOfflineTool::separatedVertex( const LHCb::RecVertex& rvtx, const std::vector<LHCb::RecVertex>& outvtxvec) const {

  double chi2min = min_value( outvtxvec, 1e10,
                              [rz = rvtx.position().z(), sigma2z = rvtx.covMatrix()(2,2)]
                              (const LHCb::RecVertex& v) {
            return std::pow(rz - v.position().z(), 2)/(sigma2z+v.covMatrix()(2,2));
  });

  bool vsepar = true;
  if ( chi2min < m_pvsChi2Separation ) vsepar = false;
  // protect secondary vertices of B signal
  if ( chi2min < m_pvsChi2SeparationLowMult && rvtx.tracks().size() < 7 ) vsepar = false;
  return vsepar;
}

//=============================================================================
// Read tracks
//=============================================================================
std::vector<const LHCb::Track*> PVOfflineTool::readTracks(const LHCb::Tracks& inputTracks) const
{
  std::vector<const LHCb::Track*> rtracks; rtracks.reserve(inputTracks.size());

  std::copy_if(inputTracks.begin(), inputTracks.end(),
               std::back_inserter(rtracks),
               [=](const LHCb::Track *trk) { return !m_requireVelo || trk->hasVelo(); } );

  if (msgLevel(MSG::DEBUG)) debug() << "readTracks: " << rtracks.size() << endmsg;

  return rtracks;
}

//=============================================================================
// removeTracks
//=============================================================================
template <typename C>
void PVOfflineTool::removeTracks(std::vector<const LHCb::Track*>& tracks,
                                 const C& tracks2remove) const
{
  if(msgLevel(MSG::DEBUG)) {
    debug() << "removeTracks. Input number of tracks: " << tracks.size()
            << "  To remove:" << tracks2remove.size() << endmsg;
  }

  for(const LHCb::Track* trk : tracks2remove) {
    auto i = std::find(tracks.begin(),tracks.end(), trk);
    if ( i != tracks.end()) tracks.erase(i);
  }

  if(msgLevel(MSG::DEBUG)) {
    debug() << "removeTracks. Output number of tracks: " << tracks.size() << endmsg;
  }
}

//=============================================================================
// removeTracks
//=============================================================================
void PVOfflineTool::removeTracksByLHCbIDs(std::vector<const LHCb::Track*>& tracks,
                                          const std::vector<const LHCb::Track*>& tracks2remove) const
{
  if(msgLevel(MSG::DEBUG)) {
    debug() << "removeTracksByLHCbIDs. Input number of tracks: " << tracks.size() << endmsg;
  }

  for(const auto* ptr1 : tracks2remove) {
    auto compute_overlap = velo_overlap_with(*ptr1);
    auto i = std::find_if( tracks.begin(), tracks.end(),
                           [&](const LHCb::Track* trk) {
        auto n = compute_overlap(*trk);
        return 1.*n.first > 0.99*n.second;
    });
    if (i!=tracks.end()) tracks.erase(i);
  } // over tracks2remove

  if(msgLevel(MSG::DEBUG)) {
    debug() << "removeTracksByLHCbIDs. Output number of tracks: " << tracks.size() << endmsg;
  }
}

//=============================================================================
// removeTracksUsedByVertex
//=============================================================================
void PVOfflineTool::removeTracksUsedByVertex(std::vector<const LHCb::Track*>& tracks,
                                             LHCb::RecVertex& rvtx) const

{
  if(msgLevel(MSG::VERBOSE)) {
    verbose() << " removeTracksUsedByVertex method" << endmsg;
  }

  removeTracks(tracks, rvtx.tracks() );

  if(msgLevel(MSG::VERBOSE)) {
    verbose() << " leaving removeTracksUsedByVertex method" << endmsg;
  }
}

//=============================================================================
// Match vtx in vector of vtx by matching tracks
//=============================================================================
LHCb::RecVertex* PVOfflineTool::matchVtxByTracks(const LHCb::RecVertex& invtx,
                                           std::vector<LHCb::RecVertex>& outvtxvec) const
{
  auto is_in = [](const auto& haystack) {
      return [&](const LHCb::Track* needle) {
          return std::any_of( haystack.begin(), haystack.end(),
                              [&](const LHCb::Track* i) { return i==needle; } );
      };
  };
  const auto& tracksIn =  invtx.tracks();
  auto best = max_above_threshold( outvtxvec.begin(), outvtxvec.end(),
                                   [&](const auto& ovtx) {
                                      return std::count_if( tracksIn.begin(), tracksIn.end(),
                                                            is_in(ovtx.tracks()) );
                                   },
                                   std::lround( std::floor( 0.3*tracksIn.size() ) ) );
  if ( best.first == outvtxvec.end() ) return nullptr;
  // if tracksIn.empty(), we cannot get here... so dividing by tracksIn.size() will always be possible
  if ( msgLevel(MSG::DEBUG) ) debug() << " vtx succesfully matched at tracks rate: " << double(best.second)/tracksIn.size() << endmsg;
  return &*best.first;
}

StatusCode PVOfflineTool::removeTracksAndRecalculatePV(const LHCb::RecVertex* pvin,
					const std::vector<const LHCb::Track*>& tracks2remove,
					LHCb::RecVertex& vtx) const
{
  return m_pvRecalc->RecalculateVertex(pvin, tracks2remove, vtx);
}

//=============================================================================
// Update of the beam spot position
//=============================================================================
StatusCode PVOfflineTool::UpdateBeamSpot()
{
  if ( !exist<Condition>(detSvc(), s_beamSpotCond) ){
    Warning( "Unable to locate beam spot condition" ) ;
    return StatusCode::FAILURE;
  }
  Condition *myCond =  get<Condition>(detSvc(), s_beamSpotCond );
  //
  const double xRC = myCond -> paramAsDouble ( "ResolPosRC" ) ;
  const double xLA = myCond -> paramAsDouble ( "ResolPosLA" ) ;
  const double   Y = myCond -> paramAsDouble ( "ResolPosY"  ) ;
  //
  m_beamSpotX = ( xRC + xLA ) / 2;
  m_beamSpotY = Y ;

  m_veloClosed = (std::abs ( xRC - m_beamSpotX ) < m_resolverBound.value()  &&
                  std::abs ( xLA - m_beamSpotX ) < m_resolverBound.value() );

  return StatusCode::SUCCESS;
}
