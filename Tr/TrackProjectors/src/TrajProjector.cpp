// Include files

// from Gaudi
#include "GaudiKernel/SystemOfUnits.h"

// local
#include "TrajProjector.h"

//-----------------------------------------------------------------------------
/// Standard constructor, initializes variables
//-----------------------------------------------------------------------------
template <typename T>
TrajProjector<T>::TrajProjector( const std::string& type,
                                 const std::string& name,
                                 const IInterface* parent )
  : TrackProjector( type, name, parent )
{
  setProperty( "Tolerance", T::defaultTolerance() );
}

/// declare and instantiate ST and Velo projectors...
struct ST {
   static constexpr double defaultTolerance() { return 0.002*Gaudi::Units::mm; }
};

typedef TrajProjector<ST> TrajSTProjector;
DECLARE_COMPONENT( TrajSTProjector )

struct Velo {
  static constexpr double defaultTolerance() { return 0.0005*Gaudi::Units::mm; }
};

typedef TrajProjector<Velo> TrajVeloProjector;
DECLARE_COMPONENT( TrajVeloProjector )

struct VP {
  static constexpr double defaultTolerance() { return 0.0005*Gaudi::Units::mm; }
};

typedef TrajProjector<VP> TrajVPProjector;
DECLARE_COMPONENT( TrajVPProjector )

struct FT {
  static constexpr double defaultTolerance() { return 0.002*Gaudi::Units::mm; }
};

typedef TrajProjector<FT> TrajFTProjector;
DECLARE_COMPONENT( TrajFTProjector )

struct Muon {
  static constexpr double defaultTolerance() { return 0.002*Gaudi::Units::mm; }
};

typedef TrajProjector<Muon> TrajMuonProjector;
DECLARE_COMPONENT( TrajMuonProjector )
