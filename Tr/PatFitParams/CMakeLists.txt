################################################################################
# Package: PatFitParams
################################################################################
gaudi_subdir(PatFitParams v4r4)

gaudi_depends_on_subdirs(Event/LinkerEvent
                         Event/MCEvent
                         Event/TrackEvent
                         GaudiAlg
                         GaudiKernel)

find_package(AIDA)

find_package(Boost)
find_package(ROOT)
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS})

gaudi_add_module(PatFitParams
                 src/*.cpp
                 INCLUDE_DIRS AIDA
                 LINK_LIBRARIES LinkerEvent MCEvent TrackEvent GaudiAlgLib GaudiKernel)

