#ifndef TRACKTOOLS_FASTMOMENTUMESTIMATE_H
#define TRACKTOOLS_FASTMOMENTUMESTIMATE_H 1

// Include files
// -------------
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "Kernel/ILHCbMagnetSvc.h"


// from TrackInterfaces
#include "TrackInterfaces/ITrackMomentumEstimate.h"

/** @class FastMomentumEstimate FastMomentumEstimate.h
 *
 *  Calculate momentum of a seed track or a match track using the states and a parametrisation
 *  The parameters for VeloTCubic can be obtained using the "MatchFitParams" algorithm.
 *
 *  Parameters:
 *
 *  - ParamsTCubic: Coefficients for momentum calculation using a state in the T stations when using a cubic fit.
 *  - ParamsTParabola: Coefficients for momentum calculation using a state in the T stations when using a parbolic fit.
 *  - ParamsVeloTCubic: Coefficients for momentum calculation using a state in the Velo and the T stations when using a cubic fit.
 *  - ParamsVeloTParabola: Coefficients for momentum calculation using a state in the Velo and the T stations when using a parbolic fit.
 *  - TResolution: Resolution for q/p, given as: sigma(q/p) = TResolution * (q/p)
 *  - VeloPlusTResolution: Resolution for q/p, given as: sigma(q/p) = VeloPlusTResolution * (q/p)
 *
 *  Note: When giving the momentum parameters / coefficients in the options file, all 4 vectors have to be given.
 *
 *  @author Stephanie Hansmann-Menzemer
 *  @date   2007-10-30
 */
class FastMomentumEstimate : public extends<GaudiTool, ITrackMomentumEstimate> {
public:
  /// Standard constructor
  using base_class::base_class;

  StatusCode initialize() override;

  /// Estimate the momentum P of a State in T at ZAtMidT
  StatusCode calculate( const LHCb::State* tState, double& qOverP, double& sigmaQOverP,
                        bool tCubicFit ) const override;

  /// Estimate the momentum P of a velo State and a State in T (for matching)
  StatusCode calculate( const LHCb::State* veloState, const LHCb::State* tState,
                        double& qOverP, double& sigmaQOverP,
                        bool tCubicFit ) const override;
private:

  ILHCbMagnetSvc*     m_magFieldSvc = nullptr;
  /// Momentum parameters (coefficients) for T state when using cubic fit
  Gaudi::Property<std::vector<double>> m_paramsTCubic { this,  "ParamsTCubic", {0.0} };
  /// Momentum parameters (coefficients) for T state when using parabolic fit
  Gaudi::Property<std::vector<double>> m_paramsTParab { this,  "ParamsTParabola", {0.0} };
  /// Momentum parameters (coefficients) for Velo and T state when using cubic fit
  Gaudi::Property<std::vector<double>> m_paramsVeloTCubic { this,  "ParamsVeloTCubic", {0.0} };
  /// Momentum parameters (coefficients) for Velo and T state when using parabolic fit
  Gaudi::Property<std::vector<double>> m_paramsVeloTParab { this,  "ParamsVeloTParabola", {0.0} };
  Gaudi::Property<double> m_tResolution { this,  "TResolution", 0.025 };
  Gaudi::Property<double> m_veloPlusTResolution { this,  "VeloPlusTResolution", 0.015 };

};
#endif // TRACKTOOLS_FASTMOMENTUMESTIMATE_H
