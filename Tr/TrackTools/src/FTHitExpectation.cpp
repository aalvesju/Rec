// from GaudiKernel
#include "GaudiKernel/SystemOfUnits.h"

// Event
#include "Event/Track.h"
#include "Event/State.h"

// Tsa
#include "TsaKernel/Line.h"
#include "TsaKernel/Parabola.h"
#include "TsaKernel/Line3D.h"

#include "LHCbMath/GeomFun.h"
#include "FTDet/DeFTDetector.h"

// local
#include "FTHitExpectation.h"

#include <algorithm>
#include <utility>

#include "LHCbMath/GeomFun.h"

using namespace LHCb;
using namespace Gaudi;

DECLARE_COMPONENT( FTHitExpectation )

//=============================================================================
//
//=============================================================================
StatusCode FTHitExpectation::initialize()
{
  StatusCode sc = THitExpectation::initialize();
  if (sc.isFailure()) return Error("Failed to initialize", sc);

  // Get all layers (need to know the z-plane of each layer)
  m_ftDet = getDet<DeFTDetector>(DeFTDetectorLocation::Default);
  if ( m_ftDet -> version() < 61 )
    return Error("This version requires FTDet v6.1 or higher", StatusCode::FAILURE);

  return StatusCode::SUCCESS;
}


IHitExpectation::Info FTHitExpectation::expectation(const LHCb::Track& aTrack) const{

  IHitExpectation::Info info;
  info.likelihood = 0.0;
  info.nFound = 0;
  info.nExpected = 0;

  const auto& ids = aTrack.lhcbIDs();
  std::vector<LHCb::LHCbID> ftHits; ftHits.reserve(ids.size());
  std::copy_if( ids.begin(), ids.end(), std::back_inserter(ftHits),
                [](const LHCb::LHCbID& id) { return id.isFT(); } );

  // Loop over all layers
  for( auto station : m_ftDet->stations() ) {
    for( auto layer : station->layers() ) {

      const DeFTMat* mat;
      Gaudi::XYZPoint intersectionPoint;
      bool expectHit = findIntersectingMat(layer, aTrack, mat, intersectionPoint);

      if( expectHit ) {
        ++(info.nExpected);

        // Check of this layer is present in the hits
        auto itIter = std::find_if( ftHits.begin(), ftHits.end(), [&mat] (const LHCb::LHCbID& id)
            {return id.ftID().uniqueLayer() == mat->elementID().uniqueLayer(); } );
        if (itIter != ftHits.end() ) ++info.nFound;
      }
    }
  }

  return info;
}


bool FTHitExpectation::findIntersectingMat(const DeFTLayer* layer, const LHCb::Track& aTrack,
    const DeFTMat*& mat, Gaudi::XYZPoint& intersectionPoint) const {

  // make a Line3D from the track
  double layerZ = layer->globalZ();
  Tf::Tsa::Line     line    = yLine    ( aTrack, layerZ );
  Tf::Tsa::Parabola aParab  = xParabola( aTrack, layerZ );
  Tf::Tsa::Line tanLine     = aParab.tangent( layerZ );
  Tf::Tsa::Line3D aLine3D   = Tf::Tsa::createLine3D( tanLine, line, layerZ );

  // find intersection point of track and plane of layer
  const Gaudi::Plane3D layerPlane = layer->plane();
  intersectionPoint = intersection(aLine3D, layerPlane);

  // find the module that is hit
  const DeFTModule* module = layer->findModule( intersectionPoint );
  if( module == nullptr ) return false;

  // find intersection point of track and plane of module
  // (to account for misalignments between module and layer)
  tanLine     = aParab.tangent( intersectionPoint.z() );
  aLine3D   = Tf::Tsa::createLine3D( tanLine, line, intersectionPoint.z() );
  const Gaudi::Plane3D modulePlane = module->plane();
  intersectionPoint = intersection(aLine3D, modulePlane);

  // Find the corresponding mat
  mat = module->findMat(intersectionPoint);

  // check if intersection point is inside the fibres of the module
  return (mat == nullptr ) ? false : mat->isInside( intersectionPoint );
}

void FTHitExpectation::collect(const LHCb::Track& aTrack,
                               std::vector<LHCb::LHCbID>& ids) const{
  // Loop over all layers
  for( auto station : m_ftDet->stations() ) {
    for( auto layer : station->layers() ) {

      const DeFTMat* mat;
      Gaudi::XYZPoint intersectionPoint;
      bool expectHit = findIntersectingMat(layer, aTrack, mat, intersectionPoint);

      if( expectHit ) {
        // Find the channel that is closest
        Gaudi::XYZPoint localP = mat->geometry()->toLocal( intersectionPoint );
        float frac;
        LHCb::FTChannelID ftChan = mat->calculateChannelAndFrac(localP.x(), frac);

        // Add the channels
        // JvT: Without the fraction the bare FTChannelID is pretty useless...
        if( std::abs(frac) <= 0.5f ) ids.push_back( LHCb::LHCbID(ftChan) );
      }
    }
  }
}


unsigned int FTHitExpectation::nExpected(const LHCb::Track& aTrack) const{
  return expectation(aTrack).nExpected;
}


Gaudi::XYZPoint FTHitExpectation::intersection(const Tf::Tsa::Line3D& line,
					       const Gaudi::Plane3D& aPlane) const{
  Gaudi::XYZPoint inter;
  double mu = 0;
  Gaudi::Math::intersection( line, aPlane, inter, mu );
  return inter;
}
