#ifndef Run2GhostId_H
#define Run2GhostId_H 1

// Include files
#include "TrackInterfaces/IHitExpectation.h"
//#include "TrackInterfaces/IVPExpectation.h"
#include "TrackInterfaces/IVeloExpectation.h"
#include "TrackInterfaces/IGhostProbability.h"
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/IIncidentListener.h"
#include "Event/Track.h"
#include "TfKernel/IOTHitCreator.h"
#include "GaudiKernel/AnyDataHandle.h"

class IClassifierReader;
namespace Track {
  class TabulatedFunction1D;
}


/** @class Run2GhostId Run2GhostId.h
 *
 *  @author Paul Seyfert
 *  @date   06-02-2015
 */
class Run2GhostId final : public extends<GaudiTool, IGhostProbability> {

public:
  /// Standard constructor
  Run2GhostId( const std::string& type, const std::string& name, const IInterface* parent);

  StatusCode finalize() override; ///< Algorithm initialization
  StatusCode initialize() override; ///< Algorithm initialization
  StatusCode execute(LHCb::Track& aTrack) const override;
  std::vector<float> netInputs(LHCb::Track& aTrack) const override;  /// TODO inline (sorry, you cannot inline a virtual function)
  std::vector<std::string> variableNames(LHCb::Track::Types type) const override;
  StatusCode beginEvent() override;

private:
  const Tf::IOTHitCreator* m_otHitCreator = nullptr;
  std::vector<std::unique_ptr<IClassifierReader>> m_readers;
  std::vector<std::unique_ptr<Track::TabulatedFunction1D>> m_flatters;

  std::vector<std::string> m_inNames;
  std::vector<int> m_vectorsizes;

  Gaudi::Property<bool> m_DaVinci { this, "InDaVinci", false };

  int m_veloHits;
  int m_ttHits;
  int m_itHits;
  int m_otHits;

  AnyDataHandle<LHCb::VeloLiteCluster::VeloLiteClusters> m_veloclusters{ LHCb::VeloLiteClusterLocation::Default, Gaudi::DataHandle::Reader, this };
  AnyDataHandle<LHCb::STLiteCluster::STLiteClusters> m_ttclusters{ LHCb::STLiteClusterLocation::TTClusters, Gaudi::DataHandle::Reader, this };
  AnyDataHandle<LHCb::STLiteCluster::STLiteClusters> m_itclusters{ LHCb::STLiteClusterLocation::ITClusters, Gaudi::DataHandle::Reader, this };
};

#endif // Run2GhostId_H
