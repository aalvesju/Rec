/** @class TrackSelectionToContainer TrackSelectionToContainer.h
 *
 *  Convert LHCb::Tracks to LHCb::Track::Selection
 */

#include "GaudiAlg/Transformer.h"
#include <string>
#include "Event/Track.h"

struct TrackSelectionToContainer final
     : Gaudi::Functional::Transformer<LHCb::Tracks(const LHCb::Track::Selection&)>
{
  TrackSelectionToContainer(const std::string& name, ISvcLocator* pSvcLocator)
  : Transformer( name, pSvcLocator,
                   { "InputLocation", {} },
                   { "OutputLocation", {} } )
  { }

  LHCb::Tracks operator()(const LHCb::Track::Selection& tracks) const override
  {
    LHCb::Tracks out;
    for (const auto& track : tracks ) {
        out.insert( new LHCb::Track(*track) );
    }
    return out;
  }
};

DECLARE_COMPONENT( TrackSelectionToContainer )
