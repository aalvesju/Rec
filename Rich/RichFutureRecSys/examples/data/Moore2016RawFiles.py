
from Configurables import Brunel
from PRConfig import TestFileDB

TestFileDB.test_file_db["2016NB_25ns_L0Filt0x1609"].run(configurable=Brunel()) 

Brunel().DataType  = "2016"
Brunel().Simulation = False
Brunel().WithMC     = False
