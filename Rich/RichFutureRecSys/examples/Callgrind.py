
from Gaudi.Configuration import *
from Configurables import CallgrindProfile, GaudiSequencer

appMgr = ApplicationMgr()

# --------------------------------------------------------------------------------------
# Reset number of events
# --------------------------------------------------------------------------------------

nEvents                   = 200
appMgr.EvtMax             = nEvents
EventSelector().PrintFreq = 10

# --------------------------------------------------------------------------------------
# Callgrind profiling control algorithm
# --------------------------------------------------------------------------------------
p = CallgrindProfile( StartFromEventN = 25,
                      StopAtEventN    = nEvents,
                      DumpAtEventN    = nEvents,
                      DumpName        = 'CALLGRIND-OUT' )
appMgr.TopAlg.insert(0,p)
# --------------------------------------------------------------------------------------

# --------------------------------------------------------------------------------------
# Turn off unwanted sequences
# --------------------------------------------------------------------------------------

# Monitoring
unwanted = ["RichMoni","UnpackMCVertex","UnpackMCParticle",
            "RichSumUnPack","RichCheck","ProtoMonitoring"] 

for alg in appMgr.TopAlg :
    # Find the All sequence
    if alg.name() == "All" :
        toKeep = [ ]
        for rAlg in alg.Members :
            if rAlg.name() not in unwanted :
                toKeep += [rAlg]
        alg.Members = toKeep

# --------------------------------------------------------------------------------------
# Example command lines
# --------------------------------------------------------------------------------------

# gaudirun.py -T --profilerName=valgrindcallgrind --profilerExtraOptions="__instr-atstart=no -v __smc-check=all-non-file __dump-instr=yes __trace-jump=yes __cache-sim=yes __branch-sim=yes"  ~/LHCbCMake/Feature/Rec/Rich/RichFutureRecSys/examples/{RichFuture.py,Callgrind.py,data/MCXDstUpgradeFiles.py} 2>&1 | tee profile.log

# --------------------------------------------------------------------------------------
