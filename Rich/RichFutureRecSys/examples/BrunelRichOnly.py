from Brunel.Configuration import *
from Configurables import Brunel, LHCbApp, CondDB, L0Conf

def turnOffTrigger() :
    from Configurables import GaudiSequencer
    GaudiSequencer("DecodeTriggerSeq").Members = [ ]
    GaudiSequencer("CaloBanksHandler").Members = [ ]
appendPostConfigAction(turnOffTrigger)

Brunel().VetoHltErrorEvents  = False
L0Conf().EnsureKnownTCK      = False
#CondDB().IgnoreHeartBeat     = True
#CondDB().EnableRunStampCheck = False
Brunel().WriteFSR = False

Brunel().DataType = "Upgrade"

Brunel().SplitRawEventInput = 4.3

Brunel().EvtMax    = 10000
Brunel().PrintFreq = 100

richSeq = "RICH"
#richSeq = "RICHFUTURE"

# Extra options for running over DST
from Configurables import Brunel, RecMoniConf
#Brunel().RichSequences   = [richSeq]
Brunel().RecoSequence    = [richSeq]
Brunel().InitSequence    = [ "Brunel" ]
#Brunel().MCCheckSequence = [ richSeq ]
Brunel().MCCheckSequence = [ ]

from Configurables import DstConf
ApplicationMgr().ExtSvc += [ "DataOnDemandSvc" ]
DstConf().EnableUnpack = [ "Reconstruction" ]

# No output files
Brunel().OutputType = "None"

# Only RICH Monitoring
from Configurables import RecMoniConf
#RecMoniConf().MoniSequence = [richSeq]
RecMoniConf().MoniSequence = [ ]

# Don't reject HLT error events
Brunel().VetoHltErrorEvents = False
