
#pragma once

// Array properties
#include "GaudiKernel/ParsersFactory.h"
#include "GaudiKernel/StdArrayAsProperty.h"

// STL
#include <algorithm>
#include <functional>
#include <type_traits>
#include <array>
#include <cmath>

// Base class
#include "RichBasePhotonReco.h"

// RichDet
#include "RichDet/DeRich1.h"
#include "RichDet/DeRich2.h"
#include "RichDet/DeRichSphMirror.h"

// Quartic Solver
#include "RichRecUtils/QuarticSolverNewton.h"

// interfaces
#include "RichInterfaces/IRichMirrorSegFinderLookUpTable.h"

// Utils
#include "RichUtils/RichRayTracingUtils.h"

// VDT
#include "vdt/asin.h"

namespace Rich
{
  namespace Future
  {
    namespace Rec
    {

      // Use the functional framework
      using namespace Gaudi::Functional;

      // pull in methods from Rich::RayTracingUtils
      using namespace Rich::RayTracingUtils;

      /** @class CommonQuarticPhotonReco RichCommonQuarticPhotonReco.h
       *
       *  Common base for quartic algorithms (scalar and vector).
       *
       *  @author Chris Jones
       *  @date   2017-10-13
       */

      class CommonQuarticPhotonReco : public BasePhotonReco
      {

      public:

        /// Standard constructor
        CommonQuarticPhotonReco( const std::string& name,
                                 ISvcLocator* pSvcLocator );

        /// Initialization after creation
        virtual StatusCode initialize() override;

      protected:

        /// Access the DeRich beam pipe objects
        inline const DeRichBeamPipe* deBeam( const Rich::DetectorType rich ) const noexcept
        {
          return m_deBeam[rich];
        }

        /// acos for double
        inline double myacos( const double x ) const noexcept { return vdt::fast_acos(x);  }

        /// acos for float
        inline float  myacos( const float  x ) const noexcept { return vdt::fast_acosf(x); }

        /// Truncate the give value to a given d.p.
        template < typename TYPE >
        inline TYPE truncate( const TYPE x ) const
        {
          constexpr long long int scale = 10000000; // 7 d.p.
          //const long long int i = std::round( x * scale );
          const long long int i = ( x * scale );
          return (TYPE)(i)/scale;
        }

      protected: // data

        /// Newton Quartic Solver
        Rich::Rec::QuarticSolverNewton m_quarticSolver;

        /// Rich1 and Rich2 detector elements
        DetectorArray<const DeRich *> m_rich = {{}};

        /// RICH beampipe object for each radiator
        DetectorArray<const DeRichBeamPipe*> m_deBeam = {{}};

        /** @brief Flag to indicate if the unambiguous photon test should be performed
         *  for each radiator
         *
         *  If set to true the reconstruction is first performed twice, using the
         *  track segment start and end points as the assumed emission points and the
         *  nominal mirror geometry. If both calculations give the same mirror segments
         *  then this photon is flagged as unambiguous and the reconstruction is then
         *  re-performed using these mirror segments and the best-guess emission point
         *  (usually the track segment centre point)
         *
         *  If false, then the photon is first reconstructed using the segment centre
         *  point as the emission point and using the nominal mirror geometry. The mirror
         *  segments found for this calculation are then used to re-do the calculation.
         */
        Gaudi::Property< RadiatorArray<bool> > m_testForUnambigPhots
        { this, "FindUnambiguousPhotons", { false, false, false } };

        /** Flag to turn on rejection of ambiguous photons
         *
         *  If set true photons which are not unambiguous will be rejected
         *  Note, setting this true automatically means m_testForUnambigPhots
         *  will be set true
         */
        Gaudi::Property< RadiatorArray<bool> > m_rejectAmbigPhots
        { this, "RejectAmbiguousPhotons", { false, false, false } };

        /** @brief Flag to indicate if Cherenkov angles should be computed using the
         *  absolute mirror segment alignment.
         *
         *  If set to true, then the reconstruction will be performed twice,
         *  once with the nominal mirror allignment in order to find the appropriate
         *  mirror segments, and then again with thos segments.
         *
         *  If false, only the nominal mirror allignment will be used
         */
        Gaudi::Property< RadiatorArray<bool> > m_useAlignedMirrSegs
        { this, "UseMirrorSegmentAlignment", { true, true, true } };

        /** Maximum number of iterations of the quartic solution, for each radiator,
         *  in order to account for the non-flat secondary mirrors.
         */
        Gaudi::Property< RadiatorArray<int> > m_nMaxQits
        { this, "NQuarticIterationsForSecMirrors", { 1, 1, 3 } };

        /** Minimum number of iterations of the quartic solution, for each radiator,
         *  in order to account for the non-flat secondary mirrors.
         */
        Gaudi::Property< RadiatorArray<int> > m_nMinQits
        { this, "MinIterationsForTolCheck", { 0, 0, 0 } };

        /// Turn on/off the checking of photon trajectories against the beam pipe
        Gaudi::Property< RadiatorArray<bool> > m_checkBeamPipe
        { this, "CheckBeamPipe", { false, false, false } };

        /** Turn on/off the checking of photon trajectories against the mirror
         *  segments to verify if the photon hit the real active area (and not,
         *  for instance, the gaps).
         */
        Gaudi::Property< RadiatorArray<bool> > m_checkPrimMirrSegs
        { this, "CheckPrimaryMirrorSegments", { false, false, false } };

        /// Minimum active segment fraction in each radiator
        Gaudi::Property< RadiatorArray<float> > m_minActiveFrac
        { this, "MinActiveFraction", { 0.2f, 0.2f, 0.2f } };

        /// Minimum tolerance for mirror reflection point during iterations
        Gaudi::Property< RadiatorArray<float> > m_minSphMirrTolIt
        { this, "MinSphMirrTolIt",
          { static_cast<float>( std::pow( 0.10*Gaudi::Units::mm, 2 ) ),
            static_cast<float>( std::pow( 0.08*Gaudi::Units::mm, 2 ) ),
            static_cast<float>( std::pow( 0.05*Gaudi::Units::mm, 2 ) ) } };

        /// Flag to control if the secondary mirrors are treated as if they are completely flat
        Gaudi::Property< DetectorArray<bool> > m_treatSecMirrsFlat
        { this, "AssumeFlatSecondaryMirrors", { true, false } };

        /// Mirror segment finder tool
        ToolHandle<const IMirrorSegFinderLookUpTable> m_mirrorSegFinder
        { this, "MirrorFinder", "Rich::Future::MirrorSegFinderLookUpTable/MirrorFinder:PUBLIC" };

      };

    }
  }
}
