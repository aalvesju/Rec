
#pragma once

// Base class
#include "RichCommonQuarticPhotonReco.h"

// Gaudi
#include "GaudiAlg/Transformer.h"

// Utils
#include "RichRecUtils/RichRadCorrLocalPositions.h"
#include "RichUtils/RichRayTracingUtils.h"
#include "RichFutureUtils/RichSIMDMirrorData.h"

// Kernel
#include "Kernel/RichDetectorType.h"
#include "Kernel/RichSide.h"

namespace Rich
{
  namespace Future
  {
    namespace Rec
    {

      namespace
      {
        /// Shortcut to the output data type
        using OutData = std::tuple<SIMDCherenkovPhoton::Vector,Relations::PhotonToParents::Vector>;
      }

      /** @class SIMDQuarticPhotonReco RichSIMDQuarticPhotonReco.h
       *
       *  Reconstructs SIMD photon candidates using the Quartic solution.
       *
       *  @author Chris Jones
       *  @date   2017-10-16
       */

      class SIMDQuarticPhotonReco final :
        public MultiTransformer< OutData( const LHCb::RichTrackSegment::Vector&,
                                          const CherenkovAngles::Vector&,
                                          const CherenkovResolutions::Vector&,
                                          const SegmentPanelSpacePoints::Vector&,
                                          const SegmentPhotonFlags::Vector&,
                                          const SIMDPixelSummaries&,
                                          const Relations::TrackToSegments::Vector& ),
                                 Traits::BaseClass_t<CommonQuarticPhotonReco> >
      {

      public:

        /// Standard constructor
        SIMDQuarticPhotonReco( const std::string& name,
                               ISvcLocator* pSvcLocator );

        /// Initialization after creation
        StatusCode initialize() override;

      public:

        /// Functional operator
        OutData operator()( const LHCb::RichTrackSegment::Vector& segments,
                            const CherenkovAngles::Vector& ckAngles,
                            const CherenkovResolutions::Vector& ckResolutions,
                            const SegmentPanelSpacePoints::Vector& trHitPntsLoc,
                            const SegmentPhotonFlags::Vector& segPhotFlags,
                            const SIMDPixelSummaries& pixels,
                            const Relations::TrackToSegments::Vector& tkToSegRels ) const override;

      private:

        /// Basic precision (float)
        using FP      = BasePhotonReco::FP;
        /// SIMD version of FP
        using SIMDFP  = BasePhotonReco::SIMDFP;
        /// Type for container of mirror pointers. Same size as SIMDFP.
        using Mirrors = BasePhotonReco::Mirrors;

      private:

        /// Compute the best emission point for the gas radiators using
        /// the given spherical mirror reflection points
        SIMDFP::mask_type getBestGasEmissionPoint( const Rich::RadiatorType radiator,
                                                   const SIMD::Point<FP> & sphReflPoint1,
                                                   const SIMD::Point<FP> & sphReflPoint2,
                                                   const SIMD::Point<FP> & detectionPoint,
                                                   const LHCb::RichTrackSegment & segment,
                                                   SIMD::Point<FP> & emissionPoint,
                                                   SIMDFP & fraction ) const;

        /// Find mirror segments and reflection points for given data
        void findMirrorData( const Rich::DetectorType rich,
                             const Rich::Side side,
                             const SIMD::Point<FP> & virtDetPoint,
                             const SIMD::Point<FP> & emissionPoint,
                             Mirrors& primMirr,
                             Mirrors& secMirr,
                             SIMD::Point<FP> & sphReflPoint,
                             SIMD::Point<FP> & secReflPoint,
                             SIMDFP::mask_type & OK ) const;

      private: // SIMD cache of various quantities

        /// SIMD Minimum active segment fraction in each radiator
        RadiatorArray<SIMDFP> m_minActiveFracSIMD = {{}};

        /// SIMD Minimum tolerance for mirror reflection point during iterations
        RadiatorArray<SIMDFP> m_minSphMirrTolItSIMD = {{}};

      };

    }
  }
}
