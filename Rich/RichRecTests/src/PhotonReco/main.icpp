
// SIMD types
#include "RichUtils/RichSIMDTypes.h"

// Gaudi
//#include "GaudiKernel/Transform3DTypes.h"
#include "GaudiKernel/Point3DTypes.h"
#include "GaudiKernel/Vector3DTypes.h"

// Quartic Solver
#include "RichRecUtils/QuarticSolverNewton.h"

// STL
#include <random>
#include <vector>
#include <iostream>
#include <string>
#include <typeinfo>

#include "../Timing.h"

// Make an instance of the quartic solver
Rich::Rec::QuarticSolverNewton qSolver;

using DoubleV = Rich::SIMD::FP<double>;
using FloatV  = Rich::SIMD::FP<float>;

ROOT::Math::PositionVector3D< ROOT::Math::Cartesian3D<float>   > sphReflPointF;
ROOT::Math::PositionVector3D< ROOT::Math::Cartesian3D<double>  > sphReflPointD;
ROOT::Math::PositionVector3D< ROOT::Math::Cartesian3D<FloatV>  > sphReflPointFSIMD;
ROOT::Math::PositionVector3D< ROOT::Math::Cartesian3D<DoubleV> > sphReflPointDSIMD;

template< typename TYPE, typename GENTYPE = TYPE >
class Data
{
public:
  using Vector = Rich::SIMD::STDVector<Data>;
  using Scalar = TYPE;
public:
  ROOT::Math::PositionVector3D< ROOT::Math::Cartesian3D<TYPE> > emissPnt;
  ROOT::Math::PositionVector3D< ROOT::Math::Cartesian3D<TYPE> > centOfCurv;
  ROOT::Math::PositionVector3D< ROOT::Math::Cartesian3D<TYPE> > virtDetPoint;
  TYPE radius;
public:
  Data() 
  {
    // random generator
    static std::default_random_engine gen;
    // Distributions for each member
    static std::uniform_real_distribution<GENTYPE> r_emiss_x(-800,800), r_emiss_y(-600,600), r_emiss_z(10000,10500);
    static std::uniform_real_distribution<GENTYPE> r_coc_x(-3000,3000), r_coc_y(-20,20),     r_coc_z(3300,3400);
    static std::uniform_real_distribution<GENTYPE> r_vdp_x(-3000,3000), r_vdp_y(-200,200),   r_vdp_z(8100,8200);
    static std::uniform_real_distribution<GENTYPE> r_rad(8500,8600);
    // setup data
    emissPnt     = { r_emiss_x(gen), r_emiss_y(gen), r_emiss_z(gen) };
    centOfCurv   = { r_coc_x(gen),   r_coc_y(gen),   r_coc_z(gen)   };
    virtDetPoint = { r_vdp_x(gen),   r_vdp_y(gen),   r_vdp_z(gen)   };
    radius       = r_rad(gen);
  }
};

template< typename DATA, typename POINT >
inline void solve( const DATA & data, POINT & sphReflPoint )
{
  qSolver.solve<typename DATA::Scalar,3,2>( data.emissPnt, 
                                            data.centOfCurv, 
                                            data.virtDetPoint,
                                            data.radius, 
                                            sphReflPoint );
}

template< typename DATA, typename POINT >
unsigned long long int __attribute__((noinline)) 
solveV( const typename DATA::Vector & dataV, POINT & sphReflPoint )
{
  unsigned long long int best_dur{ 99999999999999999 };

  timespec start, end;

  const unsigned int nTests = 5000;
  for ( unsigned int i = 0; i < nTests; ++i )
  {
    clock_gettime(CLOCK_MONOTONIC_RAW,&start);
    // iterate over the data and solve it...
    for ( const auto& data : dataV ) { solve(data,sphReflPoint); }
    clock_gettime(CLOCK_MONOTONIC_RAW,&end);
    const auto duration = time_diff(&start,&end);
    if ( duration < best_dur ) { best_dur = duration; }
  }

  return best_dur ;
}

int main ( int /*argc*/, char** /*argv*/ )
{
  const std::size_t nPhotons = 48e2; // must be devisible by 16
  
  Data<double>::Vector dataVD;
  Data<float>::Vector  dataVF;
  dataVD.reserve( nPhotons );
  dataVF.reserve( nPhotons );
  // Construct the data to work on
  std::cout << "Creating " << nPhotons << " random photons ...";
  for ( std::size_t i = 0; i < nPhotons; ++i ) 
  { 
    dataVD.emplace_back( );
    dataVF.emplace_back( ); 
  }
  // SIMD photons
  using DVSIMD = Data<DoubleV,double>;
  using FVSIMD = Data<FloatV,float>;
  DVSIMD::Vector dataVDSIMD;
  FVSIMD::Vector dataVFSIMD;
  const auto nPhotsDSIMD = nPhotons; // / DoubleV::Size;
  const auto nPhotsFSIMD = nPhotons; // / FloatV::Size;
  dataVDSIMD.reserve( nPhotsDSIMD );
  dataVFSIMD.reserve( nPhotsFSIMD );
  for ( std::size_t i = 0; i < nPhotsDSIMD; ++i ) { dataVDSIMD.emplace_back(); }
  for ( std::size_t i = 0; i < nPhotsFSIMD; ++i ) { dataVFSIMD.emplace_back(); }
  std::cout << " done." << std::endl;
  
  // double
  if ( false )
  {

    // run the solver for scalar doubles
    auto dTime = solveV< Data<double> >( dataVD, sphReflPointD );
    std::cout << "Scalar Double " << dTime << std::endl;
    
    // run the solver for SIMD doubles
    auto dTimeSIMD = solveV<DVSIMD>( dataVDSIMD, sphReflPointDSIMD );
    std::cout << "SIMD   Double " << dTimeSIMD 
              << " per scalar SpeedUp " << DoubleV::Size * (double)dTime/(double)dTimeSIMD
              << std::endl;

    asm volatile ("" : "+x"(dTime));
    asm volatile ("" : "+x"(dTimeSIMD));
    
  }

  // float
  //if ( false )
  {

    // run the solver for scalar floats
    auto fTime = solveV< Data<float> >( dataVF, sphReflPointF );
    std::cout << "Scalar Float  " << fTime << std::endl;
    
    // run the solver for SIMD doubles
    auto fTimeSIMD = solveV<FVSIMD>( dataVFSIMD, sphReflPointFSIMD );
    std::cout << "SIMD   Float  " << fTimeSIMD 
              << " per scalar SpeedUp " <<  FloatV::Size * (double)fTime/(double)fTimeSIMD
              << std::endl;
    
    // make sure we are not optimized away
    asm volatile ("" : "+x"(fTime));
    asm volatile ("" : "+x"(fTimeSIMD));
    
  }

  return 0;
}
