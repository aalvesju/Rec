// Include files

// CondDB
#include "GaudiKernel/IDetDataSvc.h"

// local
#include "LumiCheckCondDB.h"
#include "GetLumiParameters.h"

//-----------------------------------------------------------------------------
// Implementation file for class : LumiCheckCondDB
//
// 2009-02-27 : Jaap Panman
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( LumiCheckCondDB )

//=============================================================================
// Initialization
//=============================================================================
StatusCode LumiCheckCondDB::initialize()
{
  const StatusCode sc = GaudiAlgorithm::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm
  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Initialize" << endmsg;

  // prepare database tool (public)
  m_dbTool = tool<IGetLumiParameters>("GetLumiParameters","lumiDatabaseTool");

  // load the detectorDataSvc
  m_dds = detSvc();
  if ( !m_dds ) { return sc; }

  //check extended file incidents are defined
#ifdef GAUDI_FILE_INCIDENTS
  // incident service
  m_incSvc = svc<IIncidentSvc> ( "IncidentSvc" , true );
  m_incSvc->addListener( this, IncidentType::BeginInputFile );
  m_incSvc->addListener( this, IncidentType::EndInputFile   );
  if ( msgLevel(MSG::DEBUG) ) debug() << "registered with incSvc" << endmsg;
  //if not then the counting is not reliable
#else
  warn() << "cannot register with incSvc" << endmsg;
#endif //GAUDI_FILE_INCIDENTS

  return sc;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode LumiCheckCondDB::execute()
{
  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Execute" << endmsg;

  // check the DB in execution mode
  checkDB("execute");

  return StatusCode::SUCCESS;
}

//=============================================================================
//  Stop
//=============================================================================
StatusCode LumiCheckCondDB::stop()
{
  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Stop" << endmsg;

  // check the DB in stop mode
  checkDB("stop");

  return GaudiAlgorithm::stop();  // must be called after all other actions
}

//=============================================================================
//  Finalize
//=============================================================================
StatusCode LumiCheckCondDB::finalize()
{
  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Finalize" << endmsg;

  // check the DB in finalize mode
  // checkDB("finalize");

  return GaudiAlgorithm::finalize();  // must be called after all other actions
}

// ==========================================================================
// IIncindentListener interface
// ==========================================================================
void LumiCheckCondDB::handle( const Incident& incident )
{
  //check extended file incidents are defined
#ifdef GAUDI_FILE_INCIDENTS
  if ( incident.type() == IncidentType::BeginInputFile )
  {
    if ( msgLevel(MSG::DEBUG) ) debug() << "==> Incident: BeginInputFile" << endmsg;
    // check the DB in file-open mode
    checkDB("BeginInputFile");
  }
#endif
}

//=============================================================================
// check DB in event loop
//=============================================================================
void LumiCheckCondDB::checkDB( const std::string& state ) const
{
  info() << "========== Probe DB: START ========== " << state << endmsg;

  for ( unsigned long long ixt = 0; ixt < m_numberSteps; ++ixt )
  {

    const unsigned long long xt = (ixt*3600*m_stepHours) + m_startTime;
    info() << "Generate event at unix time: " << xt << endmsg;

    // set event time and trigger event
    const Gaudi::Time gxt = xt * 1000*1000*1000;
    m_dds->setEventTime( gxt );
    if ( updMgrSvc()->newEvent(gxt).isFailure() ) { error() << "ERROR newEvent" << endmsg; }
    const Gaudi::Time xtfound = m_dds->eventTime();
    if ( xtfound != gxt ) error() << "time read back: " << xtfound << endmsg;
    info() << "Event time (UTC) : " << xtfound.format(false,"%Y-%m-%d %H:%M") << endmsg;

    // look at the new DB parameters
    info() << " TCK:           " << m_dbTool->getTCK()
           << " OdinFraction:  " << m_dbTool->OdinFraction()
           << " Randomrate:    " << m_dbTool->HLTRandomRate()
           << " LHCfrequency:  " << m_dbTool->LHCFrequency()
           << " BB RandomRate: " << m_dbTool->RandomRateBB()
           << " bunches:       " << m_dbTool->CollidingBunches()
           << " absolutescale: " << m_dbTool->CalibScale()
           << endmsg;

  }

  info() << "========== Probe DB: END   ==========" << endmsg;
}
