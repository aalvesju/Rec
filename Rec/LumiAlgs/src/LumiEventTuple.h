#ifndef LUMIEVENTTUPLE_H
#define LUMIEVENTTUPLE_H 1

#include <GaudiAlg/GaudiTupleAlg.h>

/** @class LumiEventTuple LumiEventTuple.h
 *
 *  \brief Write counters and ODIN data for lumi events to a tuple
 *
 *  Two behaviours exist, which are controlled by the ArrayColumns
 *  option. If false (default), each lumi counter is written in a
 *  column. If true, the keys and the counters are written in two
 *  array columns.
 *
 *  It is assumed (without any protection) that all events have the
 *  same lumi counters. This is guaranteed as long as events taken
 *  with different TCKs are not mixed in the same job.
 *
 *  @author Rosen Matev
 *  @date   2015-05-05
 */
class LumiEventTuple : public GaudiTupleAlg {
public:

  using GaudiTupleAlg::GaudiTupleAlg;

  StatusCode execute() override;

private:

  bool fillOdin(Tuples::Tuple& tuple, const LHCb::ODIN* odin);
  bool fillCounters(Tuples::Tuple& tuple, const LHCb::HltLumiSummary* summary);

  std::vector<unsigned short> m_keys;
  std::vector<unsigned short> m_values;

  // properties
  Gaudi::Property<std::string> m_inputLocation { this, "HltLumiSummaryLocation", LHCb::HltLumiSummaryLocation::Default };
  Gaudi::Property<std::string> m_ntupleName { this, "NTupleName", "ntuple"};
  Gaudi::Property<bool> m_arrayColumns { this, "ArrayColumns", false};
  Gaudi::Property<bool> m_onlyRandom { this, "OnlyRandom", false};

};
#endif // LUMIEVENTTUPLE_H
