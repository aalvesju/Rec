#ifndef FILTERONLUMISUMMARY_H
#define FILTERONLUMISUMMARY_H 1

// Include files
// from Gaudi
#include "GaudiAlg/FilterPredicate.h"

// event model
#include "Event/HltLumiSummary.h"

/** @class FilterOnLumiSummary FilterOnLumiSummary.h
 *
 *
 *  @author Jaap Panman
 *  @date   2010-01-29
 */
class FilterOnLumiSummary : public Gaudi::Functional::FilterPredicate<bool(const LHCb::HltLumiSummary&)>
{
public:
  /// Standard constructor
  FilterOnLumiSummary( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override;    ///< Algorithm initialization
  bool operator()(const LHCb::HltLumiSummary&) const override;

private:

  Gaudi::Property<std::string> m_ValueName { this, "ValueName", "RandomMethod"};                                                 // value required
  Gaudi::Property<std::string> m_CounterName { this, "CounterName", "Method" };                                  // counter looked at
  int m_Counter = 0;                  // int value required
  int m_Value = 0;			          // int counter looked at
};
#endif // FILTERONLUMISUMMARY_H
