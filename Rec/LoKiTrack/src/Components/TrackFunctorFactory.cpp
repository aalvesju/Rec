// ============================================================================
// Include files
// ============================================================================
// GaudiKernel
// ============================================================================
#include "GaudiKernel/System.h"
// ============================================================================
// LoKi
// ============================================================================
#include "LoKi/HybridBase.h"
#include "LoKi/TrackFactoryLock.h"
#include "LoKi/ITrackFunctorFactory.h"
#include "LoKi/ITrackFunctorAntiFactory.h"
// ============================================================================
/** @file
 *
 *  definition and implementation file for class LoKi::Hybrid::TrackFunctorFactory
 *
 *  This file is a part of LoKi project -
 *    "C++ ToolKit  for Smart and Friendly Physics Analysis"
 *
 *  The package has been designed with the kind help from
 *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
 *  contributions and advices from G.Raven, J.van Tilburg,
 *  A.Golutvin, P.Koppenburg have been used in the design.
 *
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date 2004-06-29
 */
// ============================================================================
namespace LoKi
{
  // ==========================================================================
  namespace Hybrid
  {
    // ========================================================================
    /** @class TrackFunctorFactory
     *
     *  Concrete impelmentation of LoKi::ITrHybridTool interface
     *  This file is a part of LoKi project -
     *    "C++ ToolKit  for Smart and Friendly Physics Analysis"
     *
     *  The package has been designed with the kind help from
     *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
     *  contributions and advices from G.Raven, J.van Tilburg,
     *  A.Golutvin, P.Koppenburg have been used in the design.
     *
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     *  @date   2004-06-29
     */
    class TrackFunctorFactory
    // : public virtual extends2< LoKi::Hybrid::Base             ,
    // LoKi::ITrackFunctorFactory     ,
    // LoKi::ITrackFunctorAntiFactory >
      : public virtual LoKi::Hybrid::Base
      , public virtual LoKi::ITrackFunctorFactory
      , public virtual LoKi::ITrackFunctorAntiFactory
    {
    public:
      // ======================================================================
      /// finalization   of the tool
      StatusCode finalize() override;
      // ======================================================================
    public:
      // ========================================================================
      /** "Factory": get the the object form python code
       *  @param pycode the python pseudo-code of the function
       *  @param cuts the placeholder for the result
       *  @param context the context lines to be executed
       *  @return StatusCode
       */
      StatusCode get
      ( const std::string&   pycode  ,
        LoKi::Types::TrCut&  cuts    ,
        const std::string&   context ) override
      { return _get ( pycode , m_trcuts  , cuts , context ) ; }
      /** "Factory": get the the object form python code
       *  @param pycode the python pseudo-code of the function
       *  @param func the placeholder for the result
       *  @param context the context lines to be executed
       *  @return StatusCode
       */
      StatusCode get
      ( const std::string&   pycode  ,
        LoKi::Types::TrFun&  func    ,
        const std::string&   context ) override
      { return _get ( pycode , m_trfunc  , func , context ) ; }
      // ========================================================================
    public:
      // ========================================================================
      // functional part for LHCb::Track
      // ========================================================================
      /** "Factory": get the the object form python code
       *  @param pycode the python pseudo-code of the function
       *  @param func the placeholder for the result
       *  @param context the context lines to be executed
       *  @return StatusCode
       */
      StatusCode get
      ( const std::string&   pycode  ,
        LoKi::Types::TrMap&  func    ,
        const std::string&   context ) override
      { return _get ( pycode , m_trMaps  , func , context ) ; }
      // ========================================================================
      /** "Factory": get the the object form python code
       *  @param pycode the python pseudo-code of the function
       *  @param func the placeholder for the result
       *  @param context the context lines to be executed
       *  @return StatusCode
       */
      StatusCode get
      ( const std::string&   pycode  ,
        LoKi::Types::TrPipe& func    ,
        const std::string&   context ) override
      { return _get ( pycode , m_trPipes  , func , context ) ; }
      // ========================================================================
      /** "Factory": get the the object form python code
       *  @param pycode the python pseudo-code of the function
       *  @param func the placeholder for the result
       *  @param context the context lines to be executed
       *  @return StatusCode
       */
      StatusCode get
      ( const std::string&     pycode  ,
        LoKi::Types::TrFunVal& func    ,
        const std::string&     context ) override
      { return _get ( pycode , m_trFunVals  , func , context ) ; }
      // ========================================================================
      /** "Factory": get the the object form python code
       *  @param pycode the python pseudo-code of the function
       *  @param func the placeholder for the result
       *  @param context the context lines to be executed
       *  @return StatusCode
       */
      StatusCode get
      ( const std::string&     pycode  ,
        LoKi::Types::TrCutVal& func    ,
        const std::string&     context ) override
      { return _get ( pycode , m_trCutVals  , func , context ) ; }
      // ========================================================================
      /** "Factory": get the the object form python code
       *  @param pycode the python pseudo-code of the function
       *  @param func the placeholder for the result
       *  @param context the context lines to be executed
       *  @return StatusCode
       */
      StatusCode get
      ( const std::string&     pycode  ,
        LoKi::Types::TrSource& func    ,
        const std::string&     context ) override
      { return _get ( pycode , m_trSources  , func , context ) ; }
      // ========================================================================
    public: // "Anti-Factory"
      // ========================================================================
      /// set the C++ predicate for LHCb::Track
      void set ( const LoKi::Types::TrCuts&    cut ) override
      { LoKi::Hybrid::Base::_set ( m_trcuts   , cut ) ; }
      /// set the C++ function for LHCb::Track
      void set ( const LoKi::Types::TrFunc&    fun ) override
      { LoKi::Hybrid::Base::_set ( m_trfunc  , fun ) ; }
      // ======================================================================
    public: // "Anti-factory": functional part for Track
      // ======================================================================
      /// set the C++ "maps"     for Track
      void set ( const LoKi::Types::TrMaps&     fun ) override
      { LoKi::Hybrid::Base::_set ( m_trMaps      , fun ) ; }
      /// set the C++ "pipes"    for Track
      void set ( const LoKi::Types::TrPipes&    fun ) override
      { LoKi::Hybrid::Base::_set ( m_trPipes     , fun ) ; }
      /// set the C++ "funvals"  for Track
      void set ( const LoKi::Types::TrFunVals&  fun ) override
      { LoKi::Hybrid::Base::_set ( m_trFunVals   , fun ) ; }
      /// set the C++ "cutvals"  for Track
      void set ( const LoKi::Types::TrCutVals&  fun ) override
      { LoKi::Hybrid::Base::_set ( m_trCutVals   , fun ) ; }
      /// set the C++ "sources"  for Track
      void set ( const LoKi::Types::TrSources&  fun ) override
      { LoKi::Hybrid::Base::_set ( m_trSources   , fun ) ; }
      // ======================================================================
      /// constructor
      TrackFunctorFactory
      ( const std::string& type   ,
        const std::string& name   ,
        const IInterface*  parent ) ;
      // ======================================================================
    private:
      // ======================================================================
      /// helper method to save many lines:
      template <class TYPE1,class TYPE2>
      inline StatusCode _get
      ( const std::string&                                            pycode  ,
        std::unique_ptr<LoKi::Functor<TYPE1,TYPE2>>&                  local   ,
        typename LoKi::Assignable<LoKi::Functor<TYPE1,TYPE2> >::Type& output  ,
        const std::string&                                            context ) ;
      // ======================================================================
    protected:
      // ======================================================================
      // local holder of cuts
      std::unique_ptr<LoKi::Types::TrCuts>     m_trcuts     ;
      std::unique_ptr<LoKi::Types::TrFunc>     m_trfunc     ;
      // functional parts for LHCb::Track
      std::unique_ptr<LoKi::Types::TrMaps>     m_trMaps     ;
      std::unique_ptr<LoKi::Types::TrPipes>    m_trPipes    ;
      std::unique_ptr<LoKi::Types::TrFunVals>  m_trFunVals  ;
      std::unique_ptr<LoKi::Types::TrCutVals>  m_trCutVals  ;
      std::unique_ptr<LoKi::Types::TrSources>  m_trSources  ;
      //
      typedef std::vector<std::string> Modules ;
      Modules     m_modules ;
      std::string m_actor   =  "LoKi.Hybrid.TrackEngine()" ;
      typedef std::vector<std::string> Lines   ;
      Lines       m_lines   ;
      // ======================================================================
    } ;
    // ========================================================================
  } //                                            end of namespace LoKi::Hybrid
  // ==========================================================================
} //                                                      end of namespace LoKi
// ============================================================================
// helper method to save many lines:
// ============================================================================
template <class TYPE1,class TYPE2>
inline StatusCode LoKi::Hybrid::TrackFunctorFactory::_get
( const std::string&                                            pycode  ,
  std::unique_ptr<LoKi::Functor<TYPE1,TYPE2>>&                  local   ,
  typename LoKi::Assignable<LoKi::Functor<TYPE1,TYPE2> >::Type& output  ,
  const std::string&                                            context )
{
  // ==========================================================================
  // consistency check:
  const LoKi::Functor<TYPE1,TYPE2>* tmp = &output ;
  StatusCode sc = ( 0 != tmp ) ? StatusCode::SUCCESS : StatusCode::FAILURE ;
  // prepare the actual python code
  std::string code =
    makeCode  ( m_modules , m_actor , pycode , m_lines , context ) ;
  // define the scope:  ATTENTION: the scope is locked!!
  LoKi::Hybrid::TrackFactoryLock lock ( this ) ;
  //
  // use the base class method
  sc = LoKi::Hybrid::Base::_get_ ( code , local , output ) ;
  if ( sc.isFailure() )
  { return Error ( "Invalid object for the code '" + pycode + "'"    ) ; } // RETURN
  //
  //
  return StatusCode::SUCCESS ;
  // =========================================================================
}
// ============================================================================
// Standard constructor
// ============================================================================
LoKi::Hybrid::TrackFunctorFactory::TrackFunctorFactory
( const std::string& type   ,
  const std::string& name   ,
  const IInterface*  parent )
// : base_class ( type , name , parent )
  : LoKi::Hybrid::Base( type , name , parent )
///
{
  declareInterface<LoKi::ITrackFunctorFactory>     (this) ;
  declareInterface<LoKi::ITrackFunctorAntiFactory> (this) ;
  //
  m_modules.push_back ( "LoKiTrack.decorators"  ) ;
  m_modules.push_back ( "LoKiCore.math"          ) ;
  m_modules.push_back ( "LoKiCore.functions"     ) ;
  //
  declareProperty
    ( "Modules" ,
      m_modules ,
      "Python modules to be imported"            ) ;
  declareProperty
    ( "Actor"   ,
      m_actor   ,
      "The processing engine"                    ) ;
  declareProperty
    ( "Lines"   ,
      m_lines   ,
      "Additional Python lines to be executed"   ) ;
  // ==========================================================================
  // C++
  // ==========================================================================
  m_cpplines.push_back ( "#include \"LoKi/LoKiTrack.h\"" ) ;
  // ==========================================================================
}
// ============================================================================
// finalization of the tool
// ============================================================================
StatusCode LoKi::Hybrid::TrackFunctorFactory::finalize  ()
{
  //
  m_trcuts.reset();
  m_trfunc.reset();
  //
  m_trMaps.reset();
  m_trPipes.reset();
  m_trFunVals.reset();
  m_trCutVals.reset();
  m_trSources.reset();
  //
  // finalize the base
  return LoKi::Hybrid::Base::finalize() ;
}
// ============================================================================
DECLARE_COMPONENT( LoKi::Hybrid::TrackFunctorFactory )
// ============================================================================

// ============================================================================
// The END
// ============================================================================
