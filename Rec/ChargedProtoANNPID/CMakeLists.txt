################################################################################
# Package: ChargedProtoANNPID
################################################################################
gaudi_subdir(ChargedProtoANNPID v2r15)

gaudi_depends_on_subdirs(Event/MCEvent
                         Event/PhysEvent
                         Event/RecEvent
                         GaudiAlg
                         Rec/RecInterfaces)

#find_package(NeuroBayesExpert)
#if(NEUROBAYESEXPERT_FOUND)
#  add_definitions(-D_ENABLE_NEUROBAYES)
#  include_directories(${NEUROBAYESEXPERT_INCLUDE_DIRS})
#else()
#  message(WARNING "Not using NeuroBayesExpert")
#endif()

find_package(ROOT COMPONENTS TMVA)
find_package(VDT)

find_package(Boost)
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS})

# Test compiling the TMVA MVAs with different optimisation options
#set_property(SOURCE src/TMVAImpFactory-MC15TuneV1.cpp APPEND_STRING PROPERTY COMPILE_FLAGS " -O3 " )
#set_property(SOURCE src/TMVAImpFactory-MC12TuneV4.cpp APPEND_STRING PROPERTY COMPILE_FLAGS " -O3 " )
#set(CMAKE_CXX_FLAGS_RELEASE " -O3 ")

gaudi_add_library(ChargedProtoANNPID-yPIDLib
                  src/yPID/MC15TuneV1/*/*.cpp
                  PUBLIC_HEADERS src/yPID/MC15TuneV1/keras)

gaudi_add_module(ChargedProtoANNPID
                 src/*.cpp
                 INCLUDE_DIRS ROOT VDT Rec/RecInterfaces
                 LINK_LIBRARIES ROOT VDT MCEvent PhysEvent RecEvent GaudiAlgLib ChargedProtoANNPID-yPIDLib)

# Bug in gcc7.1 causes the above to give a warning
# https://gcc.gnu.org/bugzilla/show_bug.cgi?id=80593
# work around below is OK but strictly not as complete a check
set_property(SOURCE src/ChargedProtoANNPIDToolBase.cpp APPEND_STRING PROPERTY COMPILE_FLAGS " -Wno-strict-aliasing " )
set_property(SOURCE src/ChargedProtoANNPIDAlgBase.cpp  APPEND_STRING PROPERTY COMPILE_FLAGS " -Wno-strict-aliasing " )

#if(NEUROBAYESEXPERT_FOUND)
#  target_link_libraries(ChargedProtoANNPID ${NEUROBAYESEXPERT_LIBRARIES})
#endif()

gaudi_install_python_modules()
