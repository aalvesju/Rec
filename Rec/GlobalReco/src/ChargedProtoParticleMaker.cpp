
//-----------------------------------------------------------------------------
/** @file ChargedProtoParticleMaker.cpp
 *
 * Implementation file for algorithm ChargedProtoParticleMaker
 *
 * @author Chris Jones   Christopher.Rob.Jones@cern.ch
 * @date 28/08/2009
 */
//-----------------------------------------------------------------------------

// local
#include "ChargedProtoParticleMaker.h"

//-----------------------------------------------------------------------------

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
ChargedProtoParticleMaker::ChargedProtoParticleMaker( const std::string& name,
                                                      ISvcLocator* pSvcLocator )
  : GaudiAlgorithm ( name , pSvcLocator )
{
  // track selector type
  declareProperty( "TrackSelectorType", m_trSelType );

  // Input data
  declareProperty( "Inputs", m_tracksPath );

  // output data
  declareProperty( "Output", m_protoPath );
}

//=============================================================================
// Initialization
//=============================================================================
StatusCode ChargedProtoParticleMaker::initialize()
{
  const StatusCode sc = GaudiAlgorithm::initialize();
  if ( sc.isFailure() ) return sc;

  if ( context() == "HLT" || context() == "Hlt" )
  {
    if ( m_trSelType.empty() ) { m_trSelType = "TrackSelector"; }
  }
  else
  {
    if ( m_tracksPath.empty() )
    { m_tracksPath.push_back(LHCb::TrackLocation::Default); }
    if ( m_protoPath.empty() )
    { m_protoPath  = LHCb::ProtoParticleLocation::Charged; }
    if ( m_trSelType.empty() )
    { m_trSelType  = "DelegatingTrackSelector"; }
  }


  if ( msgLevel(MSG::VERBOSE) )
  {
    verbose() << "Inputs = " << m_tracksPath << endmsg;
    verbose() << "Output = " << m_protoPath << endmsg;
  }

  // get an instance of the track selector
  m_trSel = tool<ITrackSelector>( m_trSelType, "TrackSelector", this );

  return sc;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode ChargedProtoParticleMaker::execute()
{
  // check if output data already exists
  auto * protos = getIfExists<LHCb::ProtoParticles>(m_protoPath);
  if ( protos )
  {
    Warning( "Existing ProtoParticle container at " + m_protoPath +
             " found -> Will replace.", StatusCode::SUCCESS, 1 ).ignore();
    protos->clear();
  }
  else
  {
    // make new container and give to Gaudi
    protos = new LHCb::ProtoParticles();
    put ( protos, m_protoPath );
  }

  // Loop over tracks container
  setFilterPassed(false);
  for ( const auto & loc : m_tracksPath )
  {
    // Load the Track objects (mandatory - should be there for each event)
    const auto * tracks = getIfExists<LHCb::Tracks>( loc );
    if ( !tracks )
    {
      Warning( "No Tracks at '"+loc+"'", StatusCode::SUCCESS ).ignore();
      continue;
    }

    setFilterPassed(true);
    if ( msgLevel(MSG::DEBUG) )
      debug() << "Successfully loaded " << tracks->size() << " Tracks from " << loc << endmsg;

    int count = 0;
    // Loop over tracks
    for ( const auto * tk : *tracks )
    {

      // Select tracks
      if ( msgLevel(MSG::VERBOSE) )
        verbose() << "Trying Track " << tk->key() << endmsg;
      if ( !m_trSel->accept(*tk) ) continue;
      if ( msgLevel(MSG::VERBOSE) )
      {
        verbose() << " -> Track selected " << tk->key() << endmsg;
        verbose() << " -> Track type " << tk->type() << endmsg;
        verbose() << " -> Track flag " << tk->flag() << endmsg;
        verbose() << " -> Track charge " << tk->charge() << endmsg;
      }

      // Make a proto-particle
      auto * proto = new LHCb::ProtoParticle();

      if ( m_tracksPath.size() == 1 )
      {
        // Insert into container, with same key as Track
        protos->insert( proto, tk->key() );
      }
      else
      {
        // If more than one Track container, cannot use Track key
        protos->insert( proto );
      }

      // Set track reference
      proto->setTrack( tk );

      // Add minimal track info
      proto->addInfo( LHCb::ProtoParticle::additionalInfo::TrackChi2PerDof, tk->chi2PerDoF() );
      proto->addInfo( LHCb::ProtoParticle::additionalInfo::TrackNumDof,     tk->nDoF()       );
      proto->addInfo( LHCb::ProtoParticle::additionalInfo::TrackHistory,    tk->history()    );
      proto->addInfo( LHCb::ProtoParticle::additionalInfo::TrackType,       tk->type()       );
      proto->addInfo( LHCb::ProtoParticle::additionalInfo::TrackP,          tk->p()          );
      proto->addInfo( LHCb::ProtoParticle::additionalInfo::TrackPt,         tk->pt()         );

      if ( msgLevel(MSG::VERBOSE) )
      {
        verbose() << " -> Created ProtoParticle : " << *proto << endmsg;
      }
      ++count;
    }
    counter( loc +" ==> " + m_protoPath ) += count;
  }
  // return
  return StatusCode::SUCCESS;
}

//=============================================================================

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( ChargedProtoParticleMaker )

//=============================================================================
