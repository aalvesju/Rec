
//-----------------------------------------------------------------------------
/** @file ChargedProtoParticleTupleAlg.h
 *
 * Header file for algorithm ChargedProtoParticleTupleAlg
 *
 * @author Chris Jones   Christopher.Rob.Jones@cern.ch
 * @date 2006-11-15
 */
//-----------------------------------------------------------------------------

#ifndef GLOBALRECO_ChargedProtoParticleTupleAlg_H
#define GLOBALRECO_ChargedProtoParticleTupleAlg_H 1

// from Gaudi
#include "GaudiAlg/GaudiTupleAlg.h"

// Event
#include "Event/ProtoParticle.h"
#include "Event/RichPID.h"
#include "Event/MuonPID.h"
#include "Event/MCParticle.h"

// Tool interfaces
#include "MCInterfaces/IRichMCTruthTool.h"

//-----------------------------------------------------------------------------
/** @class ChargedProtoParticleTupleAlg ChargedProtoParticleTupleAlg.h
 *
 *  Simple algorithm to produce an ntuple containing the charged ProtoParticle
 *  PID information. Useful for tuning purposes.
 *
 *  @author Chris Jones
 *  @date   2006-11-15
 */
//-----------------------------------------------------------------------------

class ChargedProtoParticleTupleAlg final : public GaudiTupleAlg
{

public:

  /// Standard constructor
  using GaudiTupleAlg::GaudiTupleAlg;

  StatusCode execute() override;    ///< Algorithm execution

private:

  Gaudi::Property<std::string> m_protoPath { this, "ProtoParticleLocation",
                                             ( context() == "HLT" || context() == "Hlt" ) ? LHCb::ProtoParticleLocation::HltCharged
                                                                                          : LHCb::ProtoParticleLocation::Charged ,
                                             "Location in TES of ProtoParticles" };

  /// Use RICH tool to get MCParticle associations for Tracks (To avoid Linker details)
  ToolHandle<Rich::MC::IMCTruthTool> m_truth { this, "MCTruthTool", "Rich::MC::MCTruthTool/MCTruth" };

};

#endif // GLOBALRECO_ChargedProtoParticleTupleAlg_H
